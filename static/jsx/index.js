// index.js
/**
 * Point d'entrée de l'application React (pour Webpack : entry shiproutes)
 */

import React from "react";
import ReactDOM from "react-dom";
import 'leaflet/dist/leaflet.css';
import 'leaflet/dist/leaflet.js'
import { ShipRoutes } from './ShipRoutes';
import './translate/i18nextInit';   // Détection langue du navigateur (ou préférence utilisateur) et initialisation syst. d'internationalisation
 

// Fix pb. d'image des marqueurs Leaflet avec Webpack :
delete L.Icon.Default.prototype._getIconUrl;

L.Icon.Default.mergeOptions({
  iconRetinaUrl: require('leaflet/dist/images/marker-icon-2x.png'),
  iconUrl: require('leaflet/dist/images/marker-icon.png'),
  shadowUrl: require('leaflet/dist/images/marker-shadow.png'),
});
// Fix fin


// Insertion de l'application React dans la page Web :

// Cherche dans la page la div d'id 'render-react' :
const domContainer = document.querySelector('#render-react');

// Insère la balise de l'application dans la div, en reprenant les paramètres data-xxx de la div du domContainer :
ReactDOM.render(<ShipRoutes {...domContainer.dataset} />, domContainer);
