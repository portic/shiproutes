// LangSelector.jsx
/**
 * Liste déroulante pour choisir la langue d'affichage de l'application
 * 
 * translate prefix : langSelector
 */

import 'bootstrap/dist/css/bootstrap.min.css';
import { Form } from 'react-bootstrap';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { langsAvailable } from './i18nextInit'


export const LangSelector = ({bgColorClass}) => {
    const { t, i18n } = useTranslation();
        // Dans i18n :
        // lng=i18n.resolvedLanguage : lng=langue en cours (en, fr, ...)
        // i18n.changeLanguage(lng) : modifie la langue en cours
    const currentLang=i18n.resolvedLanguage;    // langue actuelle (détectée ou sélectionnée)

    // Gère la sélection d'une langue dans la liste déroulante
    const handleChange=(e) => {
        i18n.changeLanguage(e.target.value);    // modifie la langue en cours
    }

    return (
        <Form.Select name={"lang"} 
            title={t('langSelector.purpose_description', "Select language")} 
            aria-label={t('langSelector.purpose_description', "Select language")} 
            value={currentLang} onChange={handleChange} 
            className={`shadow-none ${bgColorClass} border-0`}
        >
            {
                langsAvailable.map(langString => (
                    <option key={langString} value={langString} >{langString}</option>
                ))
            }
        </Form.Select>
    )
}