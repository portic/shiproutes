// Ship.js
/**
 * Navire, construit à partir des données de l'API /ships : 
 * ship_id (string), occurences_names (tableau d'objets {nom: fréquence}), occurences_homeports, occurences_flags, occurences_class, occurences_tonnageclass, 
 * captain_list (tableau d'objets {captain_id: fréquence dans les sourcedoc}), mindate (yyyy-mm-dd), maxdate, nb_sourcedoc
 * 
 */

import { Captain } from "./Captain";

 export class Ship {
    // Membres de l'instance :
    apiData=null;   // données brutes pour ce navire (reçues de l'API /ships)
    parentList=null;  // Liste parente des capitaines / navires (pour retrouver le capitaine à la demande si pas déjà fait)
    captainFound=null;   // objet Captain mis en cache à la première demande
    captainsFound=null;    // tableau d'objets Captain mis en cache à la première demande
    
    // Membres de la classe (static) :

    // Constructeur
    constructor(apiData, parentList) {
        this.apiData=apiData
        this.parentList=parentList
    }

    // Fonction de comparaison à passer à Array.sort() pour trier les navires par ordre alphabétique du nom
    static compareShips(a, b) {
        const nomA=a.name;
        const nomB=b.name;
        return nomA.localeCompare(nomB)
    }

    // Propriété id : id (chaine) du navire (ship_id)
    get id() {
        return this.apiData.ship_id.trim()
    }

    // Propriété name : première occurence de nom (la plus fréquente) dans le tableau : occurences_names
    get name() {
        return Captain.getFirstOfList(this.apiData.occurences_names)
    }

    // Propriété names : toutes les occurences de nom dans le tableau : occurences_names
    get names() {
        return Captain.getTabNamesOfList(this.apiData.occurences_names)
    }

    // Propriété nameVariantes : variations d'orthographe du nom, pour affichage au survol de la cellule dans la liste des routes
    get nameVariantes() {
        const occurences=this.apiData.occurences_names
        // console.log(occurences)
        return Captain.getVariantesAsTxt(occurences)    // Mise en forme : nom (fréquence), pour affichage au survol (HTML title)
    }

    // Propriété homeport : première occurence du port d'attache (la plus fréquente) dans le tableau : occurences_homeports
    get homeport() {
        return Captain.getFirstOfList(this.apiData.occurences_homeports)
    }

    // Propriété flag : première occurence du pavillon (la plus fréquente) dans le tableau : occurences_flags
    get flag() {
        return Captain.getFirstOfList(this.apiData.occurences_flags)
    }

    // Propriété shipClass : première occurence du type de navire (la plus fréquente) dans le tableau : occurences_class
    get shipClass() {
        return Captain.getFirstOfList(this.apiData.occurences_class)
    }

    // Propriété tonnageClass : première occurence de la classe de tonnage du navire (la plus fréquente) dans le tableau : occurences_tonnageclass
    get tonnageClass() {
        return Captain.getFirstOfList(this.apiData.occurences_tonnageclass)
    }

    // Renvoie l'objet Captain du premier capitaine de la captain_list
    get captain() {
        if(this.captainFound == null) { // chercher et mettre en cache l'objet Captain (capitaine du navire)
            // Trouver l'id du 1er élément de captain_list :
            const idDuCapitaine=Captain.getFirstOfList(this.apiData.captain_list)  // id du capitaine
            if(idDuCapitaine == "") {   // capitaine du navire pas renseigné
                this.captainFound=null  
            } else {    // id à retrouver dans la liste : this.parentList.captainsList
                this.captainFound=this.parentList.captainsList.find(element => element.id == idDuCapitaine)
            }
        }   // sinon on retourne la valeur déjà mise en cache
        return this.captainFound
    }

    // Renvoie un tableau des capitaines de la captain_list
    get captains() {
        if(this.captainsFound == null) { // chercher et mettre en cache le tableau d'objets Captain (capitaines du navire)
            // Extraire le tableau des id de captain_list :
            const tabIdsDeCapitaines=Captain.getTabNamesOfList(this.apiData.captain_list)  // tableau des id de capitaines
            if(tabIdsDeCapitaines.length == 0) {   // capitaines du navire pas renseigné
                this.captainsFound=null  
            } else {    // retrouver les id dans la liste : this.parentList.captainsList
                this.captainsFound=new Array()

                tabIdsDeCapitaines.forEach((idDuCapitaine) => {
                    const captainFound=this.parentList.captainsList.find(element => element.id == idDuCapitaine)
                    if(captainFound != undefined) {    // On a trouvé l'objet Captain correspondant à l'id
                        this.captainsFound.push(captainFound) // l'ajouter au tableau résultat
                    }
                })
                if(this.captainsFound.length == 0) {   // Si aucun capitaine trouvé, on renvoie null
                    this.captainsFound=null
                } else {    // trier les capitaines par ordre alphabétique :
                    this.captainsFound.sort(Captain.compareCaptains)
                }
            }
        }   // sinon on retourne la valeur déjà mise en cache
        return this.captainsFound
    }


    // Date de début du parcours
    get mindate() {
        return this.apiData.mindate.trim()
    }

    // Date de fin du parcours
    get maxdate() {
        return this.apiData.maxdate.trim()
    }

    // Nombre de documents sources dans lesquels cet élément figure
    get nbSourcedoc() {
        return this.apiData.nb_sourcedoc.trim()
    }


    // Affichage des lignes de données contenues, pour debug
    dump() {
        // Affichage des lignes de données : 
        // console.log(this)
        console.log(this.name+": id: "+this.id+" homeport: "+this.homeport+" flag: "+this.flag+" shipClass: "+this.shipClass+" tonnageClass: "+this.tonnageClass+" captain: "+(this.captain ? this.captain.name : ""))
    }

}