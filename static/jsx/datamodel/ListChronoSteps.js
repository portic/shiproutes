// ListChronoSteps.js
/**
 * Contient les étapes d'un parcours (Liste d'objets ChronoStep)
 */

import { ChronoStep } from "./ChronoStep"

export class ListChronoSteps {
    // Membres de l'instance :
    steps=[]    // Tableau d'objets ChronoStep actuellement utilisé, filtré à partir de allSteps
    allSteps=[] // Tous les objets ChronoStep chargés
    differentiateColors=false   // Afficher les parcours avec les mêmes couleurs dépendant de l'incertitude
    filterRedSteps=null    // steps = tableau d'étapes SANS les étapes rouges
    filterOrangeSteps=null // steps = tableau d'étapes SANS les étapes oranges
    dateDomain=null // cache pour les valeurs min / max de date
    latDomain=null  // cache pour les valeurs min / max de latitude


    /**
     * Indique si on doit afficher deux étapes avec des couleurs neutres (true), ou avec les couleurs indiquant l'incertitude (false)
     * Si l'id du capitaine (ou du navire selon byCaptain) est le même pour les deux parcours, renvoie false (garder les couleurs d'incertitude)
     * Si l'id est différent, alors différencier les deux parcours à l'affichage par des couleurs neutres (n'indiquant pas l'incertitude)
     * @param {*} selectedRoutes le tableau des routes sélectionnées par l'utilisateur
     * @param {*} byCaptain indique si routes d'un capitaine (true) ou d'un navire (false)
     */
    static differentiateColors(selectedRoutes, byCaptain) {
        var result=false

        if(selectedRoutes.length > 1) { // plusieurs parcours à afficher
            // Id à considérer :
            const id1=(byCaptain ? selectedRoutes[0].captainId : selectedRoutes[0].shipId)
            const id2=(byCaptain ? selectedRoutes[1].captainId : selectedRoutes[1].shipId)
            if(id1 != id2) {    // Id différents : utiliser des couleurs neutres pour différencier les deux parcours
                result=true
            }
        }
        return result
    }
    
    // Constructeur
    constructor(tabApiData, differentiateColors, lang, byCaptain, filterRedSteps, filterOrangeSteps) {
        this.differentiateColors=differentiateColors
        // Lire les parcours reçus de l'API :
        tabApiData.forEach((apiData, routeIndex) => {   // Pour chaque parcours (1 ou 2)
            this.addToListOfSteps(routeIndex+1, tabApiData[routeIndex], lang, byCaptain) // Ajouter les étapes du parcours au tableau allSteps (instanciation de tous les objets ChronoStep)
        })

        // Tri :
        const nbParcours=tabApiData.length
        if(nbParcours == 1) {   // 1 seul parcours : trier par rank
            this.allSteps.sort(ChronoStep.compareChronoStepsByRank)
        } else {    // plus d'un parcours
            if(this.differentiateColors) {  // id de capitaine/navire différents : dérouler les étapes indépendamment (trier par N° de parcours, puis N° d'étape)
                this.allSteps.sort(ChronoStep.compareChronoStepsByRank)
            } else {    // même capitaine/navire pour les deux parcours : dérouler les parcours/étapes dans l'ordre chronologique (trier par dates)
                this.allSteps.sort(ChronoStep.compareChronoStepsByDate)
            }
        }
        

        // Filtrage des étapes qui ne doivent pas être affichées :
        this.setFilter(filterRedSteps, filterOrangeSteps)   // Remplir le tableau steps (étapes à afficher selon le filtrage - initial - demandé)
        // this.dump()
        // this.dumpNoParcoursEtRank()
    }

    /**
     * Ajoute au tableau allSteps[], les objets ChronoStep correspondant aux étapes du parcours, à partir des données du parcours reçues de l'API /travels
     * @param {*} routeIndex N° du parcours (1 ou 2)
     * @param {*} routeApiData Données reçues de l'API pour ce parcours
     * @param {*} lang Langue actuelle (fr ou en)
     * @param {*} byCaptain Indique si le parcours est celui d'un capitaine (true) ou d'un navire (false)
     */
    addToListOfSteps(routeIndex, routeApiData, lang, byCaptain) {
        // Création des éléments :
        routeApiData.forEach(segment => {
            const step=new ChronoStep(routeIndex, this.differentiateColors, segment, lang, byCaptain)  // Créer l'objet segment
            this.allSteps.push(step)   // Ajouter le segment au tableau
        })
        
    }

    // Filtrage des étapes rouges ou oranges
    setFilter(filterRedSteps, filterOrangeSteps) {
        var filtersChanged=(filterRedSteps != this.filterRedSteps) || (filterOrangeSteps != this.filterOrangeSteps)

        if(filtersChanged) {    // Un des filtres a changé
            // mémo. des nouvelles valeurs de filtres :
            this.filterRedSteps=filterRedSteps
            this.filterOrangeSteps=filterOrangeSteps
            
            // Faire le filtrage :
            this.steps=this.allSteps.filter((step) => {
                const stepColor=step.apiColor

                // Détecter si raison de filtrer l'élément
                const toBeFiltered=((this.filterRedSteps && stepColor=="red") || (this.filterOrangeSteps && stepColor=="orange"))
                // Renvoyer le contraire : (true => garder l'élément, false => ne pas le garder)
                return !toBeFiltered
            })
        }
    }

    // Renvoie l'étape du parcours (ChronoStep) dont on passe l'indice dans le tableau steps
    getStepByIndex(index) {
        var result=null // par défaut, si index invalide
        const indexMax=this.steps.length - 1

        if((index >=0) && (index <= indexMax)) {    // indice valide
            result=this.steps[index]    // élément à retourner
        }
        return result
    }

    // Renvoie un tableau de dates correspondant aux étapes du parcours, pour affichage sur l'axe des x du chronogramme
    // https://formidable.com/open-source/victory/docs/victory-axis/#tickvalues 
    getTickDateValues(listeComplete) {
        const listDatesUniques=new Array()
        var result=null
        const SEUIL_ECART_JOURS=15

        // Parcours de la liste des étapes :
        // console.log("--- deb ---")
        for(const step of this.steps) {
            // Date de départ
            const dateDepart=step.departureDateChrono
            // console.log(dateDepart)
            const dateDepartStr=dateDepart.toLocaleDateString()
            if(listDatesUniques.find(date => date.toLocaleDateString() == dateDepartStr) == undefined) {  // Ajouter la date si elle n'est pas déjà dans la liste
                listDatesUniques.push(dateDepart)
            }
            // Date d'arrivée
            const dateArrivee=step.destinationDateChrono
            // console.log(dateArrivee)
            const dateArriveeStr=dateArrivee.toLocaleDateString()
            if(listDatesUniques.find(date => date.toLocaleDateString() == dateArriveeStr) == undefined) {  // Ajouter la date si elle n'est pas déjà dans la liste
                listDatesUniques.push(dateArrivee)
            }
        }
        // console.log("--- fin ---")

        // Trier par dates croissantes :
        listDatesUniques.sort(function(a,b) {
            return a-b
        })
        // console.log("Dates uniques : ")
        // console.log(listDatesUniques)
        
        // Valeurs à renvoyer selon option :
        // console.log("Différences avec la date préc. : ")
        if(listeComplete) { // Renvoyer toutes les dates uniques trouvées
            result=listDatesUniques
        } else {    // Ne garder que les dates éloignées de la précédente de plus d'un certain nb. de jours :
            result=new Array()  // Liste vide initialement
            var dateGardeePrec=null
            for(const newDate of listDatesUniques) {    // Parcours des dates uniques des étapes
                var diffDatesJours=0
                if(dateGardeePrec) {    // La différence est calculable : newDate - dateGardeePrec
                    diffDatesJours=ChronoStep.diffDatesJours(dateGardeePrec, newDate)
                }
                // console.log(diffDatesJours)
                if(!dateGardeePrec || (diffDatesJours > SEUIL_ECART_JOURS)) {  // date à garder
                    // console.log("gardée")
                    result.push(newDate)    // garder cette date
                    dateGardeePrec=newDate    // MAJ dernière date gardée
                }
            }
        }

        // console.log("Dates sur l'axe des x : ")
        // console.log(result)
        return result
    }

    // Renvoie les dates de début / fin des étapes, au format de Domain du chronogramme : { x: [dateDébut, dateFin]}
    getDateDomain() {
        var result=null

        if(this.dateDomain) {   // le domaine est en cache
            result=this.dateDomain
        } else {    // Il faut calculer le domaine
            const listeDates=this.getTickDateValues(true)   // Liste complète des dates sur l'axe des x
            var dateDeb=listeDates[0]   // 1er élément
            var dateFin=listeDates[listeDates.length - 1]   // dernier élément

            result={ x: [dateDeb, dateFin] }
            // Mettre le résultat en cache :
            this.dateDomain=result
        }
        return result
    }

    // Renvoie le min / max des latitudes, au format Domain du chronogramme : {y: [latMin, latMax]}
    getLatDomain() {
        var latMin=90
        var latMax=-90
        var lat=-90
        var result=null

        if(this.latDomain) {    // le domaine est en cache
            result=this.latDomain
        } else {    // calculer le domaine
            this.steps.forEach(step => {
                if(!isNaN(step.departureLat)) { // la latitude de départ est non-nulle
                    lat=step.departureLat
                    // MAJ min :
                    if(lat < latMin) {
                        latMin=lat
                    }
                    // MAJ max :
                    if(lat > latMax) {
                        latMax=lat
                    }
                }
                if(!isNaN(step.destinationLat)) { // la latitude de destination est non-nulle
                    lat=step.destinationLat
                    // MAJ min :
                    if(lat < latMin) {
                        latMin=lat
                    }
                    // MAJ max :
                    if(lat > latMax) {
                        latMax=lat
                    }
                }
            })
            result={ y: [latMin, latMax] }
            // mettre la valeur trouvée en cache :
            this.latDomain=result
        }
        return result
    }
    

    // Affichage des étapes du parcours, pour debug
    dump() {
        this.steps.forEach(step => {step.dump()})
    }

    // Affichage des dates sur l'axe des x, pour debug
    dumpTickDateValues(listeComplete) {
        const tickDateValues=this.getTickDateValues(listeComplete)
        tickDateValues.forEach(date => {
            console.log(date.toLocaleDateString())
        })
    }

    // Affichage de routeIndex et de rank pour chaque étape se trouvant dans la liste
    dumpNoParcoursEtRank() {
        this.steps.forEach(etape => {
            console.log("parcours: "+etape.routeIndex+" rank: "+etape.rank)
        })
    }

}