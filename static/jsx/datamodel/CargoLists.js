// CargoLists.js
/**
 * Listes des produits, au départ et à l'arrivée d'une étape (d'un parcours)
 */

import { CargoElement } from "./CargoElement";


export class CargoLists {
    // Membres de l'instance :

    emptyAll=true   // Cargaison vide (non-précisée)
    emptyAtDeparture=true   // vide au départ
    emptyAtDestination=true // vide à l'arrivée
    listAtDeparture=[]  // liste de CargoElement (cargaison au départ de l'étape)
    listAtDestination=[]    // liste de CargoElement (cargaison à l'arrivée de l'étape)


    // Membres de la classe :


    // Constructeur
    constructor(productsList, allCargos, destinationAllCargos) {
        /*
        console.log(" ")
        console.log(allCargos)
        console.log(destinationAllCargos)
        */

        // Teste si les tableaux de l'API sont nulls ou vides :
        this.emptyAtDeparture=((allCargos == null) || ((allCargos != null) && (allCargos.length == 0)))
        this.emptyAtDestination=((destinationAllCargos == null) || ((destinationAllCargos != null) && (destinationAllCargos.length == 0)))
        this.emptyAll=(this.emptyAtDeparture && this.emptyAtDestination)

        if(!this.emptyAll) {   // Des infos sur la cargaison sont dispo => les lire
            // Cargaison au départ :
            if(!this.emptyAtDeparture) {   // infos dispo sur cargaison au départ
                this.listAtDeparture=this.readCargo(allCargos, productsList)
            }

            // Cargaison à l'arrivée :
            if(!this.emptyAtDestination) {   // infos dispo sur cargaison à l'arrivée
                this.listAtDestination=this.readCargo(destinationAllCargos, productsList)
            }
        }
        // this.dump()
    }

    // Lit le tableau de l'API, et retourne un tableau d'objets CargoElement
    readCargo(tabAllCargos, productsList) {
        const result=new Array()
        var cargoElement=null

        // Lecture des produits de l'API et ajout au tableau :
        tabAllCargos.forEach(cargoElementApiData => {  // Parcours du tableau des données de l'API
            cargoElement=new CargoElement(cargoElementApiData, productsList) // lecture des champs dans cargoElementApiData (données de l'API)
            result.push(cargoElement)   // ajout de l'objet remplis à la liste résultat
        })
        // Tri du tableau dans l'ordre alphabétique des noms de produits :
        result.sort(CargoElement.compareCargoElementsByProductName)   
        return result
    }

    // Renvoie le plus grand nombre d'éléments des deux listes (liste au départ, liste à l'arrivée)
    get maxSizeOfLists() {
        return Math.max(this.listAtDeparture.length, this.listAtDestination.length)
    }

    // Pour l'affichage, renvoie un tableau [ [1er elt liste départ, 1er elt liste d'arrivée], [2ème elt liste départ, 2ème elt liste d'arrivée], ... ]
    get linesOfBothLists() {
        const result=new Array()    // tableau des lignes de données à afficher
        const departureSize=this.listAtDeparture.length // taille liste au départ
        const destinationSize=this.listAtDestination.length // taille liste à l'arrivée
        var innerTabToAdd=null  // tableau représentant une ligne de données à afficher

        for(var noLigne=0; noLigne<this.maxSizeOfLists; noLigne++) {    // Parcours des deux listes en parallèle
            innerTabToAdd=[
                (noLigne < departureSize) ? this.listAtDeparture[noLigne].displayableString : " ",
                (noLigne < destinationSize) ? this.listAtDestination[noLigne].displayableString : " "
            ]   // prendre un élément de chaque liste et en faire un tableau
            result.push(innerTabToAdd)  // ajout du tableau au tableau résultat
        }
        return result
    }

    // Affichage dans la console, pour debug
    dump() {
        console.log(" ")
        if(this.emptyAll) { // Aucune info
            console.log("Pas d'infos sur la cargaison")
        } else {
            console.log("Cargaison :")
            // Départ :
            console.log(" ")
            if(this.emptyAtDeparture) { // Pas d'info au départ
                console.log("--- Pas d'info au départ ---")
            } else {    // Afficher les éléments de la liste au départ
                console.log("--- Au départ : "+this.listAtDeparture.length+" ---")
                this.listAtDeparture.forEach(cargoElt => {
                    cargoElt.dump()
                })
            }

            // Arrivée :
            console.log(" ")
            if(this.emptyAtDestination) { // Pas d'info à l'arrivée
                console.log("--- Pas d'info à l'arrivée ---")
            } else {    // Afficher les éléments de la liste au départ
                console.log("--- A l'arrivée : "+this.listAtDestination.length+" ---")
                this.listAtDestination.forEach(cargoElt => {
                    cargoElt.dump()
                })
            }
        }
    }
}