// Captain.js
/**
 * Capitaine de navire, construit à partir des données de l'API /captains : 
 * captain_id (string), occurences_names (tableau d'objets {nom: fréquence}), occurences_birthplaces, occurences_citizenships, 
 * ship_list (tableau d'objets {ship_id: fréquence dans les sourcedoc}), mindate (yyyy-mm-dd), maxdate, nb_sourcedoc
 * 
 */

import { Ship } from "./Ship";

export class Captain {
    // Membres de l'instance :
    apiData=null;   // données brutes pour ce capitaine (reçues de l'API /captains)
    parentList=null;  // Liste parente des capitaines / navires (pour retrouver le navire du capitaine à la demande si pas déjà fait)
    shipFound=null;   // objet Ship mis en cache à la première demande
    shipsFound=null;    // tableau d'objets Ship mis en cache à la première demande
    
    // Membres de la classe (static) :

    // Constructeur
    constructor(apiData, parentList) {
        this.apiData=apiData
        this.parentList=parentList
    }

    // Fonction de comparaison à passer à Array.sort() pour trier les capitaines par ordre alphabétique du nom
    static compareCaptains(a, b) {
        const nomA=a.name;
        const nomB=b.name;
        return nomA.localeCompare(nomB)
    }

    // Extrait les variations d'orthographe du nom, pour affichage au survol de la cellule dans la liste des routes
    static getVariantesAsTxt(occurences) {
        var result=""

        // Préparer un chaîne avec les différentes orthographes
        occurences.forEach((variante, varianteIndex) => {
            // extraire clé (orthographe) et valeur (fréquence) de la 1ère propriété de la variante :
            const nom=Object.getOwnPropertyNames(variante)[0].trim()    // extraire le nom (=nom de la 1ère propriété de l'objet)
            const frequence=variante[nom]   // Ne marche pas si la version trimmée est différente de celle dans l'objet => renvoie : undefined
            // Mise en forme :
            if(varianteIndex > 0) { // Passer à la ligne
                result+="\n"
            }
            result+=nom+" ("+frequence+")"
        })
        return result
    }

    // Extrait le nom du premier élément {nom: fréquence} de la liste passée
    static getFirstOfList(list) {
        var nom=""
        if(list != null) {    // il y a au moins un élément dans la liste passée
            const frequence=list[0]   // prendre le premier
            nom=Object.getOwnPropertyNames(frequence)[0].trim()    // extraire le nom (=nom de la 1ère propriété de l'objet)
        }
        return nom
    }

    // Extrait tous les noms des éléments {nom: fréquence} de la liste passée
    static getTabNamesOfList(list) {
        var result=new Array()
        var nom=""

        if(list != null) {  // La liste est remplie
            for(const elt of list) {    // Parcours des éléments de la liste
                nom=Object.getOwnPropertyNames(elt)[0].trim()    // extraire le nom (=nom de la 1ère propriété de l'objet)
                result.push(nom)    // Ajouter le nom au tableau résultat
            }
        }
        return result
    }

    // Propriété id : id (chaine) du capitaine (captain_id)
    get id() {
        return this.apiData.captain_id.trim()
    }

    // Propriété name : première occurence de nom (la plus fréquente) dans le tableau : occurences_names
    get name() {
        return Captain.getFirstOfList(this.apiData.occurences_names)
    }

    // Propriété names : toutes les occurences de nom dans le tableau : occurences_names
    get names() {
        return Captain.getTabNamesOfList(this.apiData.occurences_names)
    }

    // Propriété nameVariantes : variations d'orthographe du nom, pour affichage au survol de la cellule dans la liste des routes
    get nameVariantes() {
        const occurences=this.apiData.occurences_names
        // console.log(occurences)
        return Captain.getVariantesAsTxt(occurences)    // Mise en forme : nom (fréquence), pour affichage au survol (HTML title)
    }

    // Propriété birthplace : première occurence de lieu de naissance (la plus fréquente) dans le tableau : occurences_birthplaces
    get birthplace() {
        return Captain.getFirstOfList(this.apiData.occurences_birthplaces)
    }

    // Propriété citizenship : première occurence de nationalité (la plus fréquente) dans le tableau : occurences_citizenships
    get citizenship() {
        return Captain.getFirstOfList(this.apiData.occurences_citizenships)
    }

    // Renvoie l'objet Ship du premier navire de la ship_list
    get ship() {
        if(this.shipFound == null) { // chercher et mettre en cache l'objet Ship (navire du capitaine)
            // Trouver l'id du 1er élément de ship_list :
            const idDuNavire=Captain.getFirstOfList(this.apiData.ship_list)  // id du navire
            if(idDuNavire == "") {   // navire du capitaine pas renseigné
                this.shipFound=null  
            } else {    // id à retrouver dans la liste : this.parentList.shipsList
                this.shipFound=this.parentList.shipsList.find(element => element.id == idDuNavire)
            }
        }   // sinon on retourne la valeur déjà mise en cache
        return this.shipFound
    }

    // Renvoie un tableau des navires de la ship_list
    get ships() {
        if(this.shipsFound == null) { // chercher et mettre en cache le tableau d'objets Ship (navires du capitaine)
            // Extraire le tableau des id de ship_list :
            const tabIdsDeNavires=Captain.getTabNamesOfList(this.apiData.ship_list)  // tableau des id de navires
            if(tabIdsDeNavires.length == 0) {   // navires du capitaine pas renseigné
                this.shipsFound=null  
            } else {    // retrouver les id dans la liste : this.parentList.shipsList
                this.shipsFound=new Array()

                tabIdsDeNavires.forEach((idDuNavire) => {
                    const shipFound=this.parentList.shipsList.find(element => element.id == idDuNavire)
                    if(shipFound != undefined) {    // On a trouvé l'objet Ship correspondant à l'id
                        this.shipsFound.push(shipFound) // l'ajouter au tableau résultat
                    }
                })
                if(this.shipsFound.length == 0) {   // Si aucun navire trouvé, on renvoie null
                    this.shipsFound=null
                } else {    // trier les navires par ordre alphabétique :
                    this.shipsFound.sort(Ship.compareShips)
                }
            }
        }   // sinon on retourne la valeur déjà mise en cache
        return this.shipsFound
    }

    // Date de début du parcours
    get mindate() {
        return this.apiData.mindate.trim()
    }

    // Date de fin du parcours
    get maxdate() {
        return this.apiData.maxdate.trim()
    }

    // Nombre de documents sources dans lesquels cet élément figure
    get nbSourcedoc() {
        return this.apiData.nb_sourcedoc.trim()
    }

    // Affichage des lignes de données contenues, pour debug
    dump() {
        // Affichage des lignes de données :
        // console.log(this);
        console.log(this.name+": id: "+this.id+" birthplace: "+this.birthplace+" citizenship: "+this.citizenship+" ship: "+(this.ship ? this.ship.name : ""))
    }

}