// ApiDataReader.js
/**
 * Classe d'interrogation de l'API : effectue une seule requête (pas de chaînage)
 * - nécessite une variable "loadStatus" dans le state de l'appelant (permet aussi de faire les mises à jour de l'affichage de l'appelant ...)
 * - Passer l'URL de la requête lors de l'instanciation de la classe + la fonction de mise à jour de "loadStatus" : setLoadStatus()
 * - appeler chargerDonnees(), l'appeler à nouveau si la langue de l'application (lang) change, ou s'il faut recharger les données
 * - si loadStatus.loading, le chargement est en cours
 * - si loadStatus.error, l'explication courte de l'erreur est dans loadStatus.apiMessage . 
 *     -> Explication détaillée dans loadStatus.apiInfo, 
 *     -> loadStatus.apiInfoWrap = true indique si on peut forcer le passage à la ligne dans la mise en page du message d'erreur
 *     -> loadStatus.apiInfoWrap = false indique qu'il faut respecter la présentation originale (ajouter ascenseur horizontal si besoin)
 * - si : ni loadStatus.loading, ni loadStatus.error, alors le chargement est terminé avec succès, les données sont dans le champ apiData de l''objet.
 */

/* Utiliser le composant MsgLoad.jsx pour faire l'affichage :

    // Rendu du composant :

    // Chargement en cours
    if(loadStatus.loading) {
        return (<MsgLoad erreur={false} message={t('mainComponent.load_in_progress', 'Data loading ...')} />)
    }

    // Erreur de chargement
    if(loadStatus.error) {
        // Dev :
        return (<MsgLoad erreur={true} message={<>{t('mainComponent.load_error', 'Error while loading data : ')}<br />{loadStatus.apiMessage}</>} info={loadStatus.apiInfo} infoWrap={loadStatus.apiInfoWrap} />)
        // Prod :
        // return (<MsgLoad erreur={true} message={<>{t('mainComponent.load_error', 'Error while loading data : ')}<br />{loadStatus.apiMessage}</>} />)
    }

    // Données chargées sans erreur
    if(!loadStatus.loading && !loadStatus.error) {

        *** Faire le rendu du composant ici ***
        return (<>
            
        </>)
    }
    
*/

import React from 'react'
import axios from 'axios';


export class ApiDataReader {
    // Membres de l'instance :
    apiData=null    // les données au format JSON, ou null si pas chargées
    requete=""
    updateLoadStatus=null;  // Fonction pour rafraîchir le statut du chargement, dans l'appelant

    // Données de classe :
    static initialStatus={
        apiMessage: "", // message reçu pendant le chargement des données si erreur
        apiInfo: null,    // information complémentaire (debug)
        apiInfoWrap: false, // true : Ne pas afficher d'ascenseur horizontal, mais passer à la ligne (pour erreurs autres que 500)
        loading: true,  // true pendant le chargement, false quand c'est fini
        error: false // true si une erreur s'est produite pendant le chargement, false sinon
    }

    // Constructeur
    constructor(requete, updateLoadStatus) {
        this.requete=requete
        this.updateLoadStatus=updateLoadStatus
    }

    // Gérer une erreur renvoyée lors de la communication avec l'API
    gererErreurApi(error) {
        // Erreur : dans le navigateur (error) ou sur le serveur (error.response)
        // error.response avec Flask/jsonify() : {data (JSON - simple chaine), status (404), statusText (NOT FOUND), headers, config, request}
        // console.log(error.response)
        var apiMsg = "";
        var apiInfo = null;
        var apiInfoWrap=false;
        if((typeof error.response) == 'undefined') {    // Pas de réponse du serveur
            apiMsg = error.toString();
            // apiInfo = error.response.data;
            apiInfo=""
            apiInfoWrap = false;
        } else {    // On a une réponse du serveur
            // Valeurs par défaut :
            apiMsg = <>{error.toString()} - {error.response.statusText}</>;
            apiInfo = error.response.data;
            apiInfoWrap=true;   // Passer à la ligne, pas d'ascenseur horizontal
            // Personnalisation des infos de déboguage :
            switch(error.response.status) {
                case 404 :  // not found
                    apiInfo = error.response.request.responseURL+'\n\n'+error.response.data;  // Ajouter l'URL demandée
                    break;
                case 405 :  // method not allowed
                    // Ajouter la méthode utilisée :
                    apiMsg = <>{error.toString()} - {error.response.config.method.toString().toUpperCase()} {error.response.statusText}</>;
                    break;
                case 500 :  // internal server error
                    // ascenseur horizontal (pas de retour à la ligne forcé pour préserver la lisibilité des messages de Pandas SQL) :
                    apiInfoWrap=false;  
                    break;
                default :
                            
            }
        }
        // Mémoriser le message d'erreur et le status du chargement :
        this.updateLoadStatus({ apiMessage: apiMsg, apiInfo: apiInfo, apiInfoWrap: apiInfoWrap, loading: false, error: true })
    }

    // Charger les données depuis l'API
    chargerDonnees() {
        // mettre le loadStatus à : chargement en cours
        this.updateLoadStatus({ apiMessage: '', apiInfo: null, apiInfoWrap: false, loading: true, error: false })

        // Interroger l'endpoint de l'API :
        axios.get(this.requete)
            .then(res => {  // Succès :
                // stocker les données reçues :
                this.apiData=res.data

                // mettre à jour le status du chargement :
                this.updateLoadStatus({ ...this.loadStatus, loading: false, error: false })  // chargement terminé sans erreur
            })
            .catch(error => { 
                // console.log(error)
                this.gererErreurApi(error) // Gestion d'erreur avec l'API
            });
    }

    dump() {    // Pour debuggage
        console.log(this.apiData)
    }

 }