// ChronoStep.js
/**
 * Représente une étape (d'un parcours), à partir des données lues dans l'API /travels.
 * 
 * Comporte aussi des fonctions de gestion des dates (différence en jours, formattage selon la locale)
 */

import { latLng } from 'leaflet';
import { CargoLists } from './CargoLists'
import 'leaflet/dist/leaflet.css';
import 'leaflet/dist/leaflet.js' 

export class ChronoStep {
    // Membres de l'instance :

    routeIndex=-1   // N° du parcours (1 ou 2) auquel appartient cette étape
    differentiateColors=false   // Indique si on doit afficher des parcours avec des couleurs différentes, indépendantes de l'incertitude
    stepApiData=null    // Données reçues de l'API
    lang="" // fr ou en
    byCaptain=false // indique si c'est un parcours sélectionné par capitaine (true) ou par navire (false)


    // Membres de la classe :

    // Liste des champs à lire dans l'API (pour requête) :
    static paramsRead="shipcaptain_travel_rank,style_dashed,uncertainty_color,\
departure,departure_uhgs_id,departure_latitude,departure_longitude,outdate_fixed,outvizdate_fixed,departure_out_date,departure_nb_conges_1787_inputdone,departure_nb_conges_1787_cr,departure_nb_conges_1789_inputdone,departure_nb_conges_1789_cr,\
destination,destination_uhgs_id,destination_latitude,destination_longitude,indate_fixed,invizdate_fixed,destination_in_date,destination_nb_conges_1787_inputdone,destination_nb_conges_1787_cr,destination_nb_conges_1789_inputdone,destination_nb_conges_1789_cr,\
geom4326,duration,distance_dep_dest_miles,tonnage,captain_name,homeport,ship_name,ship_flag,all_cargos,destination_all_cargos\
"

    // Renvoie la date au format de la locale détectée / choisie, en utilisant la fonction / la locale JavaScript
    static formatDateForLang(date, lang) {
        return date.toLocaleDateString(lang == "fr" ? "fr-FR" : "en-US")
    }

    // Différence entre deux dates. Résultat entier en nombre de jours, arrondis.
    static diffDatesJours(d1, d2) {
        d1 = d1.getTime() / 86400000;
        d2 = d2.getTime() / 86400000;
        const strDiffArrondie=new Number(d2 - d1).toFixed(0)    // toFixed -> valeur arrondie convertie en chaine
        const result=Number.parseInt(strDiffArrondie)    // parseInt -> chaine convertie en entier
        return result
    }

    // Fonction de comparaison à passer à Array.sort() pour trier les étapes dans l'ordre des dates (jours) de départ (departureDate)
    static compareChronoStepsByDate(a, b) {
        // Tri secondaire par N° de parcours, puis par Rank (N° d'étape dans le parcours)
        const rankA=a.rank+((a.routeIndex-1)*10000)
        const rankB=b.rank+((b.routeIndex-1)*10000)
        
        // Tri primaire par date de départ (outdate_fixed), et si égalité tri secondaire
        const diffJours=ChronoStep.diffDatesJours(b.departureDate, a.departureDate)
        var result=0
        if(diffJours == 0) {    // même jour
            result=(rankA - rankB)  // tri par N° d'ordre du segment dans le parcours
        } else {
            result=diffJours
        }
        return result
    }

    // Fonction de comparaison à passer à Array.sort() pour trier les étapes dans l'ordre des N° de parcours, puis des N° d'étape
    static compareChronoStepsByRank(a, b) {
        // Tri par N° de route (routeIndex), puis par N° d'étape (rank)
        const rankA=a.rank+((a.routeIndex-1)*10000)
        const rankB=b.rank+((b.routeIndex-1)*10000)
        return (rankA - rankB)
    }


    // Constructeur
    constructor(routeIndex, differentiateColors, stepApiData, lang, byCaptain) {
        this.routeIndex=routeIndex
        this.differentiateColors=differentiateColors
        this.stepApiData=stepApiData
        this.lang=lang
        this.byCaptain=byCaptain
    }
    
    // Renvoie l'entier : N° de segment du parcours (pour navire ou capitaine)
    get rank() {
        // console.log(this.stepApiData.shipcaptain_travel_rank)
        return Number.parseInt(this.stepApiData.shipcaptain_travel_rank)
    }

    // Renvoie un booléen : segment en pointillés (true) ou entier (false)
    get isDashed() {
        return this.stepApiData.style_dashed
    }

    // Renvoie le nom de la couleur du segment (chaîne de car.) envoyée par l'API (brute)
    get apiColor() {
        return this.stepApiData.uncertainty_color
    }

    // Renvoie le nom de la couleur du segment (chaîne de car.) à afficher (chronogramme et carte)
    get color() {
        const apiColor=this.stepApiData.uncertainty_color   // couleur renvoyée par l'API
        var result=apiColor // par défaut, couleur non modifiée

        // Modification des couleurs de l'API :
        if(this.differentiateColors) {  // Seulement si id différents
            switch(apiColor) {
                case "grey" :   // gris=déclaré -> couleur claire
                    result=(this.routeIndex == 1 ? "lightskyblue" : "lightgreen")
                    break
                case "green" :  // vert=confirmé -> couleur plus foncée
                    result=(this.routeIndex == 1 ? "blue" : "green")
                    break
            }
        }
        return result
    }

    // Renvoie le nom du port de départ
    get departureName() {
        return this.stepApiData.departure
    }

    // Renvoie l'id du port de départ
    get departureUhgsId() {
        return this.stepApiData.departure_uhgs_id
    }

    // Renvoie la latitude du port de départ
    get departureLat() {
        return Number.parseFloat(this.stepApiData.departure_latitude)
    }
    
    // Renvoie la longitude du port de départ
    get departureLng() {
        return Number.parseFloat(this.stepApiData.departure_longitude)
    }

    // Renvoie le nb. max de congés de la ville de départ
    get departureMaxConges() {
        const conges1787=(this.stepApiData.departure_nb_conges_1787_inputdone ? this.stepApiData.departure_nb_conges_1787_inputdone : (this.stepApiData.departure_nb_conges_1787_cr ? this.stepApiData.departure_nb_conges_1787_cr : "0"))
        const conges1789=(this.stepApiData.departure_nb_conges_1789_inputdone ? this.stepApiData.departure_nb_conges_1789_inputdone : (this.stepApiData.departure_nb_conges_1789_cr ? this.stepApiData.departure_nb_conges_1789_cr : "0"))

        const maxConges=Math.max(Number.parseInt(conges1787), Number.parseInt(conges1789))
        return maxConges
    }

    // Renvoie la date du départ (type date)
    get departureDate() {
        return new Date(this.stepApiData.outdate_fixed)
    }

    // Renvoie la date du départ pour le chronogramme (type date)
    get departureDateChrono() {
        return new Date(this.stepApiData.outvizdate_fixed)
    }

    /**
     * Date de départ des documents de l'époque
     * Si date manquante (null), on lui substitue : departureDate, mise en forme
     * @returns Date mise en forme (type string)
     */
    get departureDateSource() {
        var result=""

        // date de départ (telle qu'écrite dans les documents), ou null si manquante
        const departureOutDate=this.stepApiData.departure_out_date

        if(departureOutDate == null) {  // Pas de date => utiliser une date alternative
            // Date alternative fournie par l'API, à formater :
            const departDate=this.departureDate // date (de substitution) fournie par l'API, toujours présente
            const departureOutDateAlt=ChronoStep.formatDateForLang(departDate, this.lang).replaceAll('/', '>')  // Remplacer '/' par '>'
            result=departureOutDateAlt
        } else {    // Date OK => la garder
            result=departureOutDate
        }
        return result
    }

    // Renvoie le nom du port d'arrivée
    get destinationName() {
        return this.stepApiData.destination
    }

    // Renvoie l'id du port d'arrivée
    get destinationUhgsId() {
        return this.stepApiData.destination_uhgs_id
    }

    // Renvoie la latitude du port de destination
    get destinationLat() {
        return Number.parseFloat(this.stepApiData.destination_latitude)
    }

    // Renvoie la longitude du port de destination
    get destinationLng() {
        return Number.parseFloat(this.stepApiData.destination_longitude)
    }

    // Renvoie le nb. max de congés de la ville d'arrivée
    get destinationMaxConges() {
        const conges1787=(this.stepApiData.destination_nb_conges_1787_inputdone ? this.stepApiData.destination_nb_conges_1787_inputdone : (this.stepApiData.destination_nb_conges_1787_cr ? this.stepApiData.destination_nb_conges_1787_cr : "0"))
        const conges1789=(this.stepApiData.destination_nb_conges_1789_inputdone ? this.stepApiData.destination_nb_conges_1789_inputdone : (this.stepApiData.destination_nb_conges_1789_cr ? this.stepApiData.destination_nb_conges_1789_cr : "0"))

        const maxConges=Math.max(Number.parseInt(conges1787), Number.parseInt(conges1789))
        return maxConges
    }
    
    // Renvoie la date d'arrivée (type date)
    get destinationDate() {
        return new Date(this.stepApiData.indate_fixed)
    }

    // Renvoie la date d'arrivée pour le chronogramme (type date)
    get destinationDateChrono() {
        return new Date(this.stepApiData.invizdate_fixed)
    }

    /**
     * Date d'arrivée des documents de l'époque
     * Si date manquante (null), on lui substitue : destinationDate, mise en forme
     * @returns Date mise en forme (type string)
     */
    get destinationDateSource() {
        var result=""

        // date d'arrivée (telle qu'écrite dans les documents), ou null si manquante
        const destinationInDate=this.stepApiData.destination_in_date    

        if(destinationInDate == null) { // Pas de date dans les documents : prendre une date alternative
            // Date alternative fournie par l'API, à formater :
            const destDate=this.destinationDate // date (de substitution) fournie par l'API, toujours présente
            // const regex = /\//ig;    // Recherche du caractère spécial '/' (dans la date, une fois formatée par JavaScript dans la locale), flag g obligatoire pour remplacement global par String.prototype.replaceAll()
            // const destinationInDateAlt=ChronoStep.formatDateForLang(destDate, this.lang).replaceAll(regex, '<')    // Dans la date d'arrivée formatée en JavaScript, remplacer le '/' par '<'
            const destinationInDateAlt=ChronoStep.formatDateForLang(destDate, this.lang).replaceAll('/', '<')    // Dans la date d'arrivée formatée en JavaScript, remplacer le '/' par '<'
            result=destinationInDateAlt
        } else {    // Date OK => la garder
            result=destinationInDate
        }
        return result
    }

    // Durée de navigation de l'étape, en jours (entier)
    get duration() {
        return Number.parseInt(this.stepApiData.duration)
    }

    // Renvoie un brushDomain : { x: [dateDepart, dateArrivee] } pour sélectionner l'étape dans le chronogramme
    get brushDomain() {
        const dateDeb=this.departureDateChrono
        const dateFin=this.destinationDateChrono
        return {x: [dateDeb, dateFin]}
    }

    /**
     * Renvoyer le tableau des coordonnées des points du parcours (voir aussi this.latLngs ci-dessous)
     * Les coordonnées sont sous forme [strLat, strLng] : sous forme de chaines de car. pour permettre les tests d'égalités (pas de float)
     * @returns [ [strLat, strLng], [strLat, strLng], ... ] Tableau de coordonnées des points du parcours (chaines de car.). [] tableau vide si geom4326 est nul.
     */
    getTabStrLatLngs() {
        var tabPoints=[]    // Résultat : tableau vide par défaut si geom4326 vaut null
        // Points du parcours de l'étape, en évitant la terre ferme :
        const geom4326=this.stepApiData.geom4326    // LINESTRING(0.2208235 49.4356154,0.1217811 49.4587009,...) de la forme : LINESTRING(lng lat,lng lat,...)

        if(geom4326) {  // non-null
            // Décoder les données geom4326 pour leaflet-spline :
            const strListPoints=geom4326.substring(11, geom4326.length - 1) // enlever LINESTRING()
            // console.log(strListPoints)
            const tabStrPoints=strListPoints.split(',') // séparer les différents points (séparateur : virgule)
            // console.log(tabStrPoints)
            tabPoints=tabStrPoints.map(strPoint => {  // récupérer lng, lat, et renvoyer un tableau de tableaux [lat, lng]
                const tabLngLat=strPoint.split(' ') // 'lng lat' => [lng, lat]
                const result=[tabLngLat[1], tabLngLat[0]]   // str et inversion => [lat, lng]
                return result
            })
        }
        // console.log(tabPoints)        
        return tabPoints
    }

    /**
     * Renvoie un tableau (de tableaux) de LatLng, représentant les points de parcours de l'étape, pour affichage par leaflet-spline
     * Appelle getTabStrLatLngs()
     * @returns [ [lat, lng], [lat, lng], ... ] Tableau de coordonnées des points du parcours (flottants pour Leaflet). [] tableau vide si geom4326 est nul.
     */
    get latLngs() {
        const tabStrLatLngs=this.getTabStrLatLngs() // décodage du LINESTRING(...) dans geom4326
        var result=[]   // Résultat : tableau vide par défaut si geom4326 vaut null

        if(tabStrLatLngs.length > 0) {  // geom4326 n'est pas nul
            result=tabStrLatLngs.map(tabStrLatLng => {
                return [Number.parseFloat(tabStrLatLng[0]), Number.parseFloat(tabStrLatLng[1])]
            })
        }
        // console.log(result)
        return result
    }

    // Distance entre port de départ et d'arrivée, en milles (miles)
    get distance() {
        return this.stepApiData.distance_dep_dest_miles
    }

    // Renvoie une chaine avec le tonnage transporté
    get tonnage() {
        return this.stepApiData.tonnage
    }


    // OBSOLETE (Portic_v7) : Renvoie un tableau, avec le contenu de la cargaison (lignes de texte, directement affichables)
    get tabCargo() {
        const result=new Array()    // Tableau de chaînes décrivant les produits et leurs quantités (avec unités)
        const produits=[ // Mise en tableau d'objets de tous les champs dispo dans l'API
            { name_in_lang: this.stepApiData.commodity1, name_alt: this.stepApiData.commodity_purpose, qty: this.stepApiData.quantity, unit: this.stepApiData.quantity_u }, 
            { name_in_lang: this.stepApiData.commodity2, name_alt: this.stepApiData.commodity_purpose2, qty: this.stepApiData.quantity2, unit: this.stepApiData.quantity_u2 }, 
            { name_in_lang: this.stepApiData.commodity3, name_alt: this.stepApiData.commodity_purpose3, qty: this.stepApiData.quantity3, unit: this.stepApiData.quantity_u3 }, 
            { name_in_lang: this.stepApiData.commodity4, name_alt: this.stepApiData.commodity_purpose4, qty: this.stepApiData.quantity4, unit: this.stepApiData.quantity_u4 }
        ]
        var strProduit=""   // chaîne décrivant un produit de la cargaison
        var strQty=""   // pour arrondis de la quantité

        // parcours du tableau des 4 produits :
        for(const produit of produits) {   
            if(produit.name_in_lang || produit.name_alt) { // nom de produit pas null => il y a un produit
                // Formattage du produit : name_in_lang (sinon name_alt) : quantity quantity_u (ou rien si pas de quantity)
                strProduit=(produit.name_in_lang ? produit.name_in_lang : "- ("+produit.name_alt+")")
                if(produit.qty) { // quantité dispo
                    strQty=Math.round(Number(produit.qty)).toString()   // Arrondis de la quantité
                    strProduit+=" : "+strQty
                    if(produit.unit) {  // unité dispo
                        strProduit+=" "+produit.unit
                    }
                }
                // Ajouter au résultat :
                result.push(strProduit)  
            }
        }
        return result
    }

    // Cargaison (Portic_v8) : renvoie un objet CargoLists = listes de mouvements de marchandises au départ et à l'arrivée de l'étape (pour StepDataView)
    getCargoLists(productsList) {
        const allCargos=this.stepApiData.all_cargos // tableau des mouvements de marchandises au départ de l'étape
        const destinationAllCargos=this.stepApiData.destination_all_cargos  // tableau des mouvements de marchandises à l'arrivée de l'étape

        // Instanciation de l'objet contenant les listes :
        const cargoLists=new CargoLists(productsList, allCargos, destinationAllCargos)
        return cargoLists
    }


    get captainName() {
        return this.stepApiData.captain_name
    }

    // Nom du navire
    get shipName() {
        return this.stepApiData.ship_name
    }

    // Pavillon
    get shipFlag() {
        // return ( (this.lang == 'fr') ? this.stepApiData.ship_flag_standardized_fr : this.stepApiData.ship_flag_standardized_en)
        return this.stepApiData.ship_flag
    }

    // Port d'attache
    get shipHomeport() {
        return this.stepApiData.homeport
    }

    // Vérifie si les points de départ et d'arrivée de l'étape ont des coordonnées [lat, lng] non-nulles
    hasCoordinates() {
        const result=((this.stepApiData.departure_longitude != null) && (this.stepApiData.departure_latitude != null) && (this.stepApiData.destination_longitude != null) && (this.stepApiData.destination_latitude != null))
        return result
    }

    /**
     * Pour le segment [ville de départ, ville d'arrivée], renvoie un tableau de max. deux (tableaux [lat, lng] de) coordonnées des deux villes (si les coordonnées sont disponibles)
     * @returns Tableau de coordonnées [lat, lng] des villes de départ et / ou d'arrivée (si disponibles, tableau vide [] ou avec une seule coordonnée sinon)
     */
    getDepartureDestinationCoordinates() {
        const result=new Array()

        if((this.stepApiData.departure_longitude != null) && (this.stepApiData.departure_latitude != null)) {   // Coordonnées ville de départ disponibles
            result.push([this.stepApiData.departure_latitude, this.stepApiData.departure_longitude])
        }
        if((this.stepApiData.destination_longitude != null) && (this.stepApiData.destination_latitude != null)) {   // Coordonnées ville de destination disponibles
            result.push([this.stepApiData.destination_latitude, this.stepApiData.destination_longitude])
        }
        return result
    }

    // Détecte dans geom4326 les anomalies rendant l'étape non-affichable sur une carte
    isDisplayableOnMap() {
        var result=true // à priori
        const valLatLngs=this.getTabStrLatLngs()  // Lire le tableau (en string) des points de la trajectoire entre le port de départ et le port d'arrivée
        const nbPoints=valLatLngs.length
        
        // Détecter l'absence de trajectoire :
        if(nbPoints < 2) {  // On veut 2 points (une droite) au minimum !
            result=false
        }
        // Détecter les trajectoires n'ayant que 2 points identiques :
        if(result && (nbPoints == 2)) {    // Seulement 2 points dans la trajectoire !
            if((valLatLngs[0][0] == valLatLngs[1][0]) && (valLatLngs[0][1] == valLatLngs[1][1])) {    // Si 2 points identiques : non-traçable, non-zoomable !
                result=false
            }
        }
        
        return result
    }

    // Supprime du tableau this.latLngs deux points [lat, lng] successifs identiques
    supprimerPointsSuccessifsIdentiques() {
        const tabLatLngs=this.getTabStrLatLngs()   // tableau à traiter, en strings
        const result=new Array()    // Résultat : tableau vide par défaut
        var strLatLngPrec=null

        if(tabLatLngs.length > 0) { // le tableau des points n'est pas vide
            // Parcours du tableau de [lat, lng]
            for(const strTabLatLng of tabLatLngs) {
                if(!strLatLngPrec) {    // Premier élément
                    result.push([Number.parseFloat(strTabLatLng[0]), Number.parseFloat(strTabLatLng[1])])   // Conversion en tableau de float
                } else {
                    // Comparer l'élément précédent avec le courant :
                    if((strTabLatLng[0] != strLatLngPrec[0]) || (strTabLatLng[1] != strLatLngPrec[1])) {    // différents
                        result.push([Number.parseFloat(strTabLatLng[0]), Number.parseFloat(strTabLatLng[1])])   // Conversion en tableau de float
                    } else {
                        // identiques : ne pas inclure dans le résultat
                    }
                }
                // mémo. nouvel élément précédent :
                strLatLngPrec=strTabLatLng
            }
        }
        return result
    }

    // Affichage dans la console, pour debug
    dump() {
        
        console.log("("+this.routeIndex+") "+this.rank+": "+(this.isDashed ? "- - -" : "_____")+" "+this.color+" "+this.departureName+" -> "+this.destinationName+"\n"+
            "("+this.departureDate.toLocaleDateString()+", "+this.departureLat+") -> ("+this.destinationDate.toLocaleDateString()+", "+this.destinationLat+")\n"+
            "xChrono("+this.departureDateChrono.toLocaleDateString()+" -> "+this.destinationDateChrono.toLocaleDateString()+")")
        
        /*
        console.log("("+this.routeIndex+") "+this.rank+": ")
        console.log(this.stepApiData)
        */
    }

}