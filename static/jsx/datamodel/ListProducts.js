// ListProducts.js
/**
 * Contient le dictionnaire des produits, construit à partir des données de l'API /cargo_categories
 */

import { Product } from "./Product"

export class ListProducts {
    // Membres de l'instance :

    products=[]    // Tableau (initialement vide) d'objets Product
    

    // Constructeur
    constructor(productsData, lang) {
        this.addToListOfProducts(productsData, lang) // Charge les données de l'API vers le tableau products[]
        // this.products.sort(Product.compareProductsByCategoryAndName) // Tri par catégorie, puis par nom de produit, pour affichage (debug)
        this.products.sort(Product.compareProductsById) // Tri par id (recordId), pour retrouver plus vite un produit par son id
        // this.dump()
    }

    // Chargement des données de l'API
    addToListOfProducts(productsData, lang) {
        // Création des éléments Product :
        productsData.forEach(element => {
            const product=new Product(element, lang)  // Créer l'objet produit
            this.products.push(product)   // Ajouter le produit au tableau
        })
        
    }

    // Renvoie le produit (Product) dont on passe l'id (string)
    getProductByRecordId(recordId) {
        var result=this.products.find(product => product.recordId == recordId)  // Rechercher dans la liste le produit ayant l'id demandé
        if(result == undefined) {   // aucun produit avec cet id n'est trouvé dans la liste
            result=null // Retourner null
        }
        return result
    }

    // Affichage des étapes du parcours, pour debug
    dump() {
        this.products.forEach(product => {product.dump()})
    }


}