// ListCaptains.js
/**
 * Liste des capitaines / navires, chargées depuis les api /captains et /ships
 * 
 * translate prefix : mainComponent
 */

 import React from 'react'
import { Captain } from "./Captain"
import { RouteInfo } from "./RouteInfo"
import { Ship } from "./Ship"
import { SEARCH_BY_NAME, SEARCH_BY_NAME_ALT, SEARCH_BY_ID } from '../user_interface/FormSearchRoutes'

import { t } from "i18next";

export class ListCaptainsShips {
    // membres de l'instance :
    apiData=[]  // les données brutes reçues de l'API /captains et /ships
    // Listes d'objets (listes intermédiaires), classées dans l'ordre alphabétique
    captainsList=[]
    shipsList=[]
    // Listes des noms uniques (ordre alphabétique). Tableau d'objets { value, label } pour la liste déroulante avec champ de recherche react-select
    captainNames=[]
    shipNames=[]
    // Liste des id (ordre alphabétique). Tableau d'objets { value, label } pour la liste déroulante avec champ de recherche react-select
    captainIds=[]
    shipIds=[]
    // Liste des routes pour un nom de capitaine (ou navire) sélectionné : objets RouteInfo
    routesFound=[]
    // Pour filtrage des routes (tableaux d'objets { value, label } pour listes déroulantes)
    filterComplNames=[] // Navires ou capitaines (élément complémentaire du mode de sélection) dans les routes
    filterId=[] // Id du capitaine ou du navire
    filterCitizenship=[]    // Identification "nationale" du capitaine
    filterBirthplace=[]    // Identification locale du capitaine
    filterFlag=[]   // Pavillon
    filterHomeport=[]   // Port d'attache
    filterTonnageClass=[]   // Classe de tonnage
    filterClass=[]   // Type de navire

    // Constructeur
    constructor(apiData) {
        this.apiData=apiData    // mémoriser les données de l'API
        this.buildCaptainsShipsList()    // Construit les listes (intermédiaires) d'objets : Captain, et Ship
        this.prepareCaptainShipNamesAndIds()    // Liste des noms uniques (les id) de capitaines et navires - Préparer les données pour la liste déroulante avec champ de recherche
    }

    // Emet un message d'erreur
    signalError(errorMsg) {
        // Signaler l'erreur dans la console :
        console.error(errorMsg)
    }

    // Renvoie la borne inférieure d'une classe de tonnage (ex.: [21-50] -> 21)
    static getBorneInf(classeDeTonnage) {
        // Regex : voir https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/RegExp/test et https://developer.mozilla.org/fr/docs/Web/JavaScript/Guide/Regular_Expressions 
        // Retrouver le tonnage (borne inférieure) de la classe de tonnage :
        const regexDecodeBornes = /^\[(?<strBorneInf>[\d]+)(-| et plus)(?<strBorneSup>[\d]+)?\]$/    // groupe nommé à capturer : (?<nomGroupe>motif)
        const groupsFound=classeDeTonnage.match(regexDecodeBornes) // exécute la regex pour extraire les groupes nommés (strBorneInf, strBorneSup)
        const borneInf=Number.parseInt(groupsFound["groups"].strBorneInf)    // conversion en entier
        return borneInf
    }

    // Fonction de comparaison à passer à Array.sort() pour trier les classes de tonnage (des navires) dans l'ordre croissant du tonnage
    static compareTonnageClasses(a, b) {
        const borneInfA=ListCaptainsShips.getBorneInf(a)
        const borneInfB=ListCaptainsShips.getBorneInf(b)
        return borneInfA - borneInfB
    }

    // Fonction de comparaison à passer à Array.sort() pour trier les listes déroulantes (pour le composant react-select) dans l'ordre croissant de la valeur à retourner (value), et non affichée (label)
    static compareReactSelectElementsByValue(a, b) {
        const valueA=a.value
        const valueB=b.value
        return valueA.localeCompare(valueB) // tri alphabétique croissant
    }

    // Construit les listes (intermédiaires) d'objets : Captain, et Ship
    buildCaptainsShipsList() {
        // Construction liste des capitaines :
        this.apiData.captainsData.forEach(item => {
            const objCaptain=new Captain(item, this)  // Créer le capitaine correspondant à l'item reçu
            this.captainsList.push(objCaptain)  // Ajouter l'objet Captain à la liste
        })
        // Tri :
        const fctPourTriCaptains=Captain.compareCaptains
        this.captainsList.sort(fctPourTriCaptains)

        // Construction liste des navires :
        this.apiData.shipsData.forEach(item => {
            const objShip=new Ship(item, this)  // Créer le navire correspondant à l'item reçu
            this.shipsList.push(objShip)  // Ajouter l'objet Ship à la liste
        })
        // Tri :
        const fctPourTriShips=Ship.compareShips
        this.shipsList.sort(fctPourTriShips)

        // Debug : affichage liste des capitaines
        /*
        console.log("\n\n\n"+this.captainsList.length+" capitaines :")
        this.captainsList.forEach(capitaine => {
            // Affichage des lignes de données d'un élément :
            capitaine.dump();
            // Séparation des éléments :
            console.log("-----")
        })
        */

        // Debug : affichage liste des navires
        /*
        console.log("\n\n\n"+this.shipsList.length+" navires :")
        this.shipsList.forEach(navire => {
            // Affichage des lignes de données d'un élément :
            navire.dump();
            // Séparation des éléments :
            console.log("-----")
        })
        */
    }

    // Prépare les données pour la liste déroulante avec champ de recherche, à partir de captainsList et shipsList
    prepareCaptainShipNamesAndIds() {
        var namePrec="" // pour sauter les noms identiques (homonymes)
        var idStr=""
        var name=""
        var id=""
        var indice=0
        var nameObj=null
        var idObj=null
        const indiceMax=-1   // Debug : Mettre 0 pour tout afficher, -1 pour désactiver, ou le nombre à afficher
        const showCounts=false   // Debug : affichage du nombre d'éléments dans les listes

        // Parcours de la liste (triée) des capitaines
        namePrec=""
        idStr=""
        name=""
        id=""
        indice=0
        if(indiceMax != -1) {   // Affichage pour debug :
            console.log("Capitaines:")
            console.log(" ")
        }
        this.captainsList.forEach(captain => {
            name=captain.name
            id=captain.id

            // Ajouter directement l'id à la liste :
            idObj={ // Format des entrées de la liste des "options" pour le composant react-select
                value: id, // valeur retournée par la liste
                label: id+" "+name   // valeur affichée
            }
            this.captainIds.push(idObj)

            // Gérer les homonymes :
            if(name != namePrec) {  // Nouveau nom
                if(namePrec != "") {    // pas de nom précédent à stocker au début
                    // Ajouter le nom précédent, avec sa liste d'id, à la liste :
                    nameObj={ // Format des entrées de la liste des "options" pour le composant react-select
                        value: namePrec, 
                        // label: <>{namePrec} (id: {idStr})</>
                        label: namePrec+" (id: "+idStr+")"
                    }    
                    this.captainNames.push(nameObj)    // Ajouter à la liste
                }
                // Pour le nouveau nom :
                namePrec=name
                idStr=id

                // Affichage pour debug :
                if( (indiceMax != -1) && ((indiceMax == 0) || (indice < indiceMax)) ) {
                    console.log(name)
                }
                indice++
            } else {    // Toujours le même nom : ajouter id à idStr
                idStr+=", "+id
            }
        })
        // Ajouter le dernier nom, avec sa liste d'id, à la liste :
        nameObj={ // Format des entrées de la liste des "options" pour le composant react-select
            value: namePrec, 
            label: namePrec+" (id: "+idStr+")"
        }    
        this.captainNames.push(nameObj)    // Ajouter à la liste

        // Tri de la liste des id :
        this.captainIds.sort(ListCaptainsShips.compareReactSelectElementsByValue)

        // Affichage pour debug :
        if((indiceMax != -1) || showCounts) {
            console.log("captainsList: "+this.captainsList.length+" captainNames: "+this.captainNames.length+" captainIds: "+this.captainIds.length)
            console.log(" ")
        }
        

        // Parcours de la liste (triée) des navires
        namePrec=""
        idStr=""
        name=""
        id=""
        indice=0
        if(indiceMax != -1) {   // Affichage pour debug :
            console.log("Navires:")
            console.log(" ")
        }
        this.shipsList.forEach(ship => {
            name=ship.name
            id=ship.id

            // Ajouter directement l'id à la liste :
            idObj={ // Format des entrées de la liste des "options" pour le composant react-select
                value: id, // valeur retournée par la liste
                label: id+" "+name   // valeur affichée
            }
            this.shipIds.push(idObj)

            // Gérer les homonymes :
            if(name != namePrec) {  // Nouveau nom
                if(namePrec != "") {    // pas de nom précédent à stocker au début
                    // Ajouter le nom précédent, avec sa liste d'id, à la liste :
                    nameObj={ // Format des entrées de la liste des "options" pour le composant react-select
                        value: namePrec, 
                        label: namePrec+" (id: "+idStr+")"
                    }    
                    this.shipNames.push(nameObj)    // Ajouter à la liste
                }
                // Pour le nouveau nom :
                namePrec=name
                idStr=id

                // Affichage pour debug :
                if( (indiceMax != -1) && ((indiceMax == 0) || (indice < indiceMax)) ) {
                    console.log(name)
                }
                indice++
            } else {    // Toujours le même nom : ajouter id à idStr
                idStr+=", "+id
            }
        })
        // Ajouter le dernier nom, avec sa liste d'id, à la liste :
        nameObj={ // Format des entrées de la liste des "options" pour le composant react-select
            value: namePrec, 
            label: namePrec+" (id: "+idStr+")"
        }    
        this.shipNames.push(nameObj)    // Ajouter à la liste

        // Tri de la liste des id :
        this.shipIds.sort(ListCaptainsShips.compareReactSelectElementsByValue)

        // Affichage pour debug :
        if((indiceMax != -1) || showCounts) {
            console.log("shipsList: "+this.shipsList.length+" shipNames: "+this.shipNames.length+" shipIds: "+this.shipIds.length)
            console.log(" ")
        }
    }

    // Restreint les dates de 'element' à la période des dates de 'complElt', et renvoie un objet {minDate: xxx, maxDate: xxx}
    restrictDatesMinMax(element, complElt) {
        var minDate=new Date(element.mindate)   // Date JavaScript
        var minDateStr=element.mindate  // date en chaîne de car.
        var maxDate=new Date(element.maxdate)
        var maxDateStr=element.maxdate
        const complMinDate=new Date(complElt.mindate)
        const complMinDateStr=complElt.mindate
        const complMaxDate=new Date(complElt.maxdate)
        const complMaxDateStr=complElt.maxdate
        
        // Comparaison des dates JavaScript :
        if((complMinDate - minDate) > 0) {    // si complMinDate > minDate
            minDateStr=complMinDateStr
        }
        if((complMaxDate - maxDate) < 0) {    // si complMaxDate < maxDate
            maxDateStr=complMaxDateStr
        }
        const result={minDate: minDateStr, maxDate: maxDateStr} // Renvoyer les dates en chaînes de car.
        return result
    }

    /**
     * Construit un Object (routeData) à partir de l'objet Ship ou Captain passé, et de son élément complémentaire (Captain ou Ship) choisi
     * @param {Captain or Ship} element L'objet à partir duquel construire le routeData
     * @param {boolean} byCaptain Indique si element est un Captain ou un Ship
     * @param {Ship or Captain} forCompElt L'élément complémentaire de  element (ex.: si element est un Captain, alors forCompElt est captain.ship, ou un élément de captain.ships !)
     * @param {boolean} restrictDates Doit-on restreindre l'intervalle des dates du parcours (donc du capitaine) au dates de son navire ?
     * @returns Object routeData, contenant les infos d'un parcours pour React-Table
     */
    getRouteDataFromElement(element, byCaptain, forCompElt, restrictDates) {
        var routeData=null
        var restrictedDates=null

        // Restreindre les dates de element, à l'intervalle des dates de forComplElt, si besoin :
        if(restrictDates) {
            restrictedDates=this.restrictDatesMinMax(element, forCompElt)
        }
        // Assembler les données de routeData :
        if(byCaptain) { // route sélectionnée par capitaine
            const ship=forCompElt
            routeData={   // Créer un objet contenant les données affichables
                captainId: element.id,
                captainName: element.name,
                shipId: (ship ? ship.id : ""),
                shipName: (ship ? ship.name : ""),
                captainCitizenship: element.citizenship,
                captainBirthplace: element.birthplace,
                shipFlag: (ship ? ship.flag : ""),
                shipHomeport: (ship ? ship.homeport : ""),
                shipTonnageClass: (ship ? ship.tonnageClass : ""),
                shipClass: (ship ? ship.shipClas : ""),
                mindate: (restrictDates ? restrictedDates.minDate : element.mindate),   // Utiliser les dates restreintes si besoin
                maxdate: (restrictDates ? restrictedDates.maxDate : element.maxdate),
            }
        } else {    // route sélectionnée par navire
            const captain=forCompElt
            routeData={   // Créer un objet contenant les données affichables
                shipId: element.id,
                shipName: element.name,
                captainId: (captain ? captain.id : ""),
                captainName: (captain ? captain.name : ""),
                shipFlag: element.flag,
                shipHomeport: element.homeport,
                shipTonnageClass: element.tonnageClass,
                shipClass: element.shipClass,
                captainCitizenship: (captain ? captain.citizenship : ""),
                captainBirthplace: (captain ? captain.birthplace : ""),
                mindate: (restrictDates ? restrictedDates.minDate : element.mindate),   // Utiliser les dates restreintes si besoin
                maxdate: (restrictDates ? restrictedDates.maxDate : element.maxdate),
            }
        }
        return routeData
    }

    /**
     * Construit une liste (Array) de RouteInfo, à partir de la liste de capitaines (ou de bateaux) du nom choisi, 
     * en ne gardant que le 1er élément complémentaire => la liste fournie est une liste d'homonymes du nom choisi,
     * et dans le résultat il n'y aura qu'un seul parcours par id source (ex. si navire, on propose la route avec le 1er capitaine)
     * @param {boolean} byCaptain indique si liste de capitaines ou de navires
     * @param {Array} filteredList la liste de capitaines (ou de navires) utilisée pour construire la liste de parcours
     */
     buildListOfRoutesWithFirstCompEltOnly(byCaptain, filteredList) {
        this.routesFound=new Array()    // Initialement, liste de parcours vide

        // Ajouter tous les champs à afficher :
        filteredList.forEach(element => {
            var routeData=null

            if(byCaptain) { // route sélectionnée par capitaine
                const ship=element.ship // élément complémentaire
                routeData=this.getRouteDataFromElement(element, byCaptain, ship, false)    // Créer un objet contenant les données affichables
            } else {    // route sélectionnée par navire
                const captain=element.captain   // élément complémentaire
                routeData=this.getRouteDataFromElement(element, byCaptain, captain, false)  // Créer un objet contenant les données affichables
            }
            const routeInfo=new RouteInfo(byCaptain, routeData)    // instanciation objet RouteInfo
            this.routesFound.push(routeInfo)    // ajout à la liste des routes
        })
    }

    /**
     * Construit une liste (Array) de RouteInfo, à partir de la liste de capitaines (ou de bateaux) du nom choisi, 
     * en gardant TOUS les éléments complémentaires => la liste fournie est une liste de paires (navire, capitaine),
     * et dans le résultat il peut y avoir plusieurs parcours par id source (ex. si navire, on propose la route avec le 1er capitaine, et la route avec le 2ème, ...)
     * @param {boolean} byCaptain indique si liste de capitaines ou de navires
     * @param {Array} filteredList la liste de capitaines (ou de navires) utilisée pour construire la liste de parcours
     */
     buildListOfRoutesWithAllCompElts(byCaptain, filteredList) {
        this.routesFound=new Array()    // Initialement, liste de parcours vide

        // Traiter chaque homonyme (se trouvant dans la liste filteredList) :
        filteredList.forEach((element) => {
            var tabComplElt=null
            var routeData=null
            var routeInfo=null

            // tableau d'éléments complémentaires :
            tabComplElt=( byCaptain ? element.ships : element.captains )
            // parcours du tableau pour ajouter chaque paire (navire, capitaine) à la liste des parcours :
            if(tabComplElt) {   // La liste est remplie,
                const restrictDates=(tabComplElt.length > 1)    // Si deux parcours au moins à afficher, restreindre les dates à celles de l'élément complémentaire
                tabComplElt.forEach((complElt) => { // la parcourir :
                    routeData=this.getRouteDataFromElement(element, byCaptain, complElt, restrictDates)    // Créer un objet contenant les données affichables
                    routeInfo=new RouteInfo(byCaptain, routeData)    // instanciation objet RouteInfo
                    this.routesFound.push(routeInfo)    // ajout à la liste des routes
                })
            }
        })
    }

    /**
     * Prépare les données (routesFound = parcours) à afficher dans la table de résultats de la boîte de dialogue de sélection de parcours
     * C'est un tableau d'objets Ship ou Captain
     * Prépare aussi les listes d'options des listes déroulantes de filtrage
     */
    buildRoutesFound(byCaptain, selection, selectionType) {
        var filteredList=null

        this.routesFound=null   // c'est la liste qu'on veut construire

        // Ne retenir que les capitaines / navires portant le nom ou les id sélectionnés :
        const listToFilter=(byCaptain ? this.captainsList : this.shipsList)
        switch(selectionType) {
            case SEARCH_BY_NAME :   // Par nom
                // Filtrage : 
                filteredList=listToFilter.filter( element => ((element.name == selection)) )
                break;
            case SEARCH_BY_ID :   // Par id
                // Filtrage :
                filteredList=listToFilter.filter( element => (selection.includes(element.id)) )   // Cherche si chacun des éléments possède l'un des id recherchés (selection = tableau d'id recherchés)
                // Tri de la liste dans l'ordre des noms :
                const fctPourTri=(byCaptain ? Captain.compareCaptains : Ship.compareShips)
                filteredList.sort(fctPourTri)
                break;
        }

        // Construire la liste des parcours correspondant aux éléments trouvés (navires ou capitaines qui portent le nom choisi) :
        // this.buildListOfRoutesWithFirstCompEltOnly(byCaptain, filteredList)    // Ne garder que le 1er capitaine d'un navire (un seul parcours par navire homonyme)
        this.buildListOfRoutesWithAllCompElts(byCaptain, filteredList)  // Proposer tous les couples (navire, capitaine) d'un navire

        // Debug : afficher la liste des routes du capitaine / navire :
        /*
        console.log(" ")
        console.log(" ")
        console.log(this.routesFound.length+" trouvées: ")
        console.log(" ")
        this.routesFound.forEach(element => {
            element.dump()
            console.log("------")
        })
        */

        // Préparer aussi les listes déroulantes de filtrage :
        // 1) listes intermédiaires (non conservées)
        const filterComplNameValues=new Array() // Liste intermédiaire ne contenant que des chaines de car.
        const filterIdValues=new Array() // Liste intermédiaire
        const filterCitizenshipValues=new Array() // Liste intermédiaire
        const filterBirthplaceValues=new Array() // Liste intermédiaire
        const filterFlagValues=new Array() // Liste intermédiaire
        const filterHomeportValues=new Array() // Liste intermédiaire
        const filterTonnageClassValues=new Array() // Liste intermédiaire
        const filterClassValues=new Array() // Liste intermédiaire
        // 2) collecter les différentes valeurs
        this.routesFound.forEach(route => {
            // Elément complémentaire (navire ou capitaine) :
            const complName=route.complElt    // nom de l'élément complémentaire, "" s'il n'y en a pas
            if( (complName != "") && (!filterComplNameValues.includes(complName)) )  // Si pas vide, et pas encore dans la liste
                filterComplNameValues.push(complName)   // l'ajouter
            // Id (navire ou capitaine) :
            const id=(byCaptain ? route.routeData.captainId : route.routeData.shipId)   // Id de l'élément à sélectionner
            if( (id != "") && (!filterIdValues.includes(id)) )  // Si pas vide, et pas encore dans la liste
                filterIdValues.push(id)   // l'ajouter
            // Nationalité du capitaine
            const citizenship=route.routeData.captainCitizenship
            if( (citizenship != "") && (!filterCitizenshipValues.includes(citizenship)) )  // Si pas vide, et pas encore dans la liste
                filterCitizenshipValues.push(citizenship)   // l'ajouter
            // Ville d'origine du capitaine
            const birthplace=route.routeData.captainBirthplace
            if( (birthplace != "") && (!filterBirthplaceValues.includes(birthplace)) )  // Si pas vide, et pas encore dans la liste
                filterBirthplaceValues.push(birthplace)   // l'ajouter
            // Pavillon du navire
            const flag=route.routeData.shipFlag
            if( (flag != "") && (!filterFlagValues.includes(flag)) )  // Si pas vide, et pas encore dans la liste
                filterFlagValues.push(flag)   // l'ajouter
            // Port d'attache du navire
            const homeport=route.routeData.shipHomeport
            if( (homeport != "") && (!filterHomeportValues.includes(homeport)) )  // Si pas vide, et pas encore dans la liste
                filterHomeportValues.push(homeport)   // l'ajouter
            // Classe de tonnage du navire
            const tonnageClass=route.routeData.shipTonnageClass
            if( (tonnageClass != "") && (!filterTonnageClassValues.includes(tonnageClass)) )  // Si pas vide, et pas encore dans la liste
                filterTonnageClassValues.push(tonnageClass)   // l'ajouter
            // Type de navire
            const shipClass=route.routeData.shipClass
            if( (shipClass != "") && (!filterClassValues.includes(shipClass)) )  // Si pas vide, et pas encore dans la liste
                filterClassValues.push(shipClass)   // l'ajouter
        })
        // 3) les trier
        filterComplNameValues.sort()    // tri alphabétique de la liste de noms
        filterIdValues.sort()
        filterCitizenshipValues.sort()
        filterBirthplaceValues.sort()
        filterFlagValues.sort()
        filterHomeportValues.sort()
        filterTonnageClassValues.sort(ListCaptainsShips.compareTonnageClasses)  // Tri sur l'entier borne inférieure de l'intervalle (ex.: [21-50] -> 21)
        filterClassValues.sort()
        // 4) construire les listes des options des listes déroulantes (conservées)
        this.filterComplNames=filterComplNameValues.map(someName => ({ value: someName, label: someName })) // mémoriser le tableau d'objets pour la liste de sélection
        this.filterId=filterIdValues.map(someName => ({ value: someName, label: someName }))
        this.filterCitizenship=filterCitizenshipValues.map(someName => ({ value: someName, label: someName }))
        this.filterBirthplace=filterBirthplaceValues.map(someName => ({ value: someName, label: someName }))
        this.filterFlag=filterFlagValues.map(someName => ({ value: someName, label: someName }))
        this.filterHomeport=filterHomeportValues.map(someName => ({ value: someName, label: someName }))
        this.filterTonnageClass=filterTonnageClassValues.map(someName => ({ value: someName, label: someName }))
        this.filterClass=filterClassValues.map(someName => ({ value: someName, label: someName }))
    }

    /**
     * Prépare les données (routesFound = parcours) à visualiser directement (issues de l'URL), sans passer par les boîtes de dialogue de sélection
     * @param {boolean} byCaptain Indique quel est l'élément principal (capitaine ou navire)
     * @param {*} tabIdCouples tableau de couples [captainId, shipId] à transformer directement en routeData / RouteInfo pour stockage dans le tableau routesFound
     * @returns boolean success Indiquant si on a pu trouver les parcours à afficher
     */
    buildRoutesFoundFromSelectedIds(byCaptain, tabIdCouples) {
        var success=false
        var elementId="", complEltId="" // id des éléments à retrouver : élément principal (capitaine si byCaptain = true) et l'autre élément (complémentaire) du couple (capitaine, navire)
        var elements=null, complElts=null   // tableaux résultats de filtrage (recherche)
        var element=null, complElt=null // éléments trouvés
        var routeData=null  // objet routeData construit
        var routeInfo=null  // objet routeInfo correspondant à routeData
        var errorMsg="" // message d'erreur signalant un problème (avec les données provenant de l'URL)

        this.routesFound=new Array()    // Initialement, liste de parcours vide

        tabIdCouples.forEach(coupleOfIds => {
            if(byCaptain) { // élément principal = capitaine
                // ids à utiliser :
                elementId=coupleOfIds[0]    // captainId
                complEltId=coupleOfIds[1]    // shipId
                // Recherche du capitaine :
                elements=this.captainsList.filter(captain => (captain.id == elementId))  // recherche du capitaine
                if(elements.length == 1) {   // On a trouvé le capitaine
                    element=elements[0] // capitaine trouvé
                    // Recherche du navire :
                    complElts=this.shipsList.filter(ship => (ship.id == complEltId))  // recherche du navire
                    if(complElts.length == 1) { // On a trouvé le navire
                        complElt=complElts[0]   // navire trouvé
                        // Validation du couple (capitaine, navire) :
                        if(element.ships.includes(complElt)) {  // Le capitaine a bien utilisé ce navire
                            routeData=this.getRouteDataFromElement(element, byCaptain, complElt, false)    // Créer un objet contenant les données affichables
                            routeInfo=new RouteInfo(byCaptain, routeData)    // instanciation objet RouteInfo
                            this.routesFound.push(routeInfo)    // ajout à la liste des routes
                            success=true
                        } else {    // Parcours introuvable
                            errorMsg=t('mainComponent.unknown_route_error', "Error in URL : the route with captain id '{{ captain_id }}' and ship id '{{ ship_id }}' can't be found", {
                                captain_id: elementId,
                                ship_id: complEltId
                            })
                            this.signalError(errorMsg)
                        }
                    } else {    // Navire introuvable
                        errorMsg=t('mainComponent.unknown_ship_error', "Error in URL : ship with id '{{ id }}' not found", {id: complEltId})
                        this.signalError(errorMsg)
                    }
                } else {    // Capitaine introuvable
                    errorMsg=t('mainComponent.unknown_captain_error', "Error in URL : captain with id '{{ id }}' not found", {id: elementId})
                    this.signalError(errorMsg)
                }
            } else {    // élément principal = navire
                // ids à utiliser :
                elementId=coupleOfIds[1]    // shipId
                complEltId=coupleOfIds[0]    // captainId
                // Recherche du navire :
                elements=this.shipsList.filter(ship => (ship.id == elementId))  // recherche du navire
                if(elements.length == 1) {   // On a trouvé le navire
                    element=elements[0] // navire trouvé
                    // Recherche du capitaine :
                    complElts=this.captainsList.filter(captain => (captain.id == complEltId))  // recherche du capitaine
                    if(complElts.length == 1) { // On a trouvé le capitaine
                        complElt=complElts[0]   // capitaine trouvé
                        // Validation du couple (navire, capitaine) :
                        if(element.captains.includes(complElt)) {  // Le navire a bien été utilisé par ce capitaine
                            routeData=this.getRouteDataFromElement(element, byCaptain, complElt, false)    // Créer un objet contenant les données affichables
                            routeInfo=new RouteInfo(byCaptain, routeData)    // instanciation objet RouteInfo
                            this.routesFound.push(routeInfo)    // ajout à la liste des routes
                            success=true
                        } else {    // Parcours introuvable
                            errorMsg=t('mainComponent.unknown_route_error', "Error in URL : the route with captain id '{{ captain_id }}' and ship id '{{ ship_id }}' can't be found", {
                                captain_id: complEltId,
                                ship_id: elementId
                            })
                            this.signalError(errorMsg)
                        }
                    } else {    // Capitaine introuvable
                        errorMsg=t('mainComponent.unknown_captain_error', "Error in URL : captain with id '{{ id }}' not found", {id: complEltId})
                        this.signalError(errorMsg)
                    }
                } else {    // Navire introuvable
                    errorMsg=t('mainComponent.unknown_ship_error', "Error in URL : ship with id '{{ id }}' not found", {id: elementId})
                    this.signalError(errorMsg)
                }
            }
        })
        return success
    }

    /**
     * Teste chaque variante du tableau tabVariantes, avec une expression régulière regex, renvoie celles qui ont matché
     * Appelée par searchAllWordsFromString()
     * @param {Array} tabVariantes Variantes d'un même nom (majoritaire en tête)
     * @param {RegExp} regex Expression régulière pour tester si chaque variante matche
     * @param {string} id id du capitaine ou du navire
     * @returns objet { optGroupName, results } avec results=un tableau contenant un objet { value: id du capitaine ou du navire, label: id et nom trouvé} pour chaque variante qui a matché, tableau vide sinon
     */
    searchVariantesForRegex(tabVariantes, regex, id) {
        const result=new Array()
        var nomMajoritaire=""
        var matchFound=null
        var idTrouve=""
        var nomTrouve=""

        tabVariantes.forEach( (nom, index) => {    // parcours des noms (variantes) du capitaine ou du navire
            if(index == 0) {
                nomMajoritaire=nom  // mémo. spelling majoritaire
            }
            matchFound=nom.match(regex) // appliquer l'expression régulière au nom pour voir s'il correspond
            if(matchFound !== null) {    // ça matche
                idTrouve=id
                nomTrouve="id:"+idTrouve+" "+nom+(index > 0 ? " ("+nomMajoritaire+")" : "")  // id: nom majoritaire ou variante (nom majoritaire si variante)
                // console.log(nomTrouve)
                result.push({ value: idTrouve, label: nomTrouve})  // Renvoyer un objet avec l'id et le texte à afficher
            }
        })   
        return { optGroupName: "id:"+id+" "+nomMajoritaire, results: result}
    }

    /**
     * Recherche un nom de capitaine ou de navire, contenant tous les mots de la chaine à rechercher.
     * La recherche est effectuée aussi dans chaque variante de nom d'un capitaine ou d'un navire
     * La fonction searchVariantesForRegex() est utilisée
     * @param {string} wordsToSearch L'expression tapée dans le champ de recherche, contenant les mots à rechercher
     * @param {boolean} byCaptain true si recherche de capitaine, false si recherche de navire
     * @returns un tableau de tableaux d'objets résultat de recherche { optGroupName, results }, ou un tableau vide si rien trouvé
     */
    searchAllWordsFromString(wordsToSearch, byCaptain) {
        const result=new Array()    // Tableau (vide par défaut) de : tableaux de noms ou variantes trouvés (pour pouvoir faire des <optgroup> si plusieurs spellings pour un même majoritaire)
        var variantes=null
        var searchResultVariantes=null
        
        // Construire une expression régulière pour trouver tous les mots d'au moins 2 lettres figurant dans wordsToSearch :

        // Isoler les mots à rechercher dans la chaine wordsToSearch :
        const allWords=wordsToSearch.split(' ')  // obtient un tableau des mots (séparateur : espace)
        // Ne garder que les mots de 2 lettres au moins :
        const words=allWords.filter( word => ((word.length >= 2)) )
        if(words.length > 0) {  // une recherche est possible : il y a des (bribes de) mots à rechercher
            // Construire la regex permettant de trouver tous ces mots dans une chaine :
            const tabSubExpr=words.map( word => ("(?=.*"+word+")") ) // renvoie (?=.*mot) pour chaque mot 
                // (lookahead à partir du début de la chaine pour assurer de trouver les mots dans n'importe quel ordre) 
                // voir https://www.developpez.net/forums/d1480099/javascript/general-javascript/recherche-plusieurs-mots-via-regex/
            const strRegex=tabSubExpr.join('')  // combine tous les éléments du tableau en une seule chaine, sans séparateur
            const regex=new RegExp(strRegex, 'i')   // transfo. en expression régulière, pour une recherche insensible à la casse (i)


            // Rechercher tous les mots de wordsToSearch dans toutes les variantes de chaque nom :
            // console.log(" ")

            const listObj=(byCaptain ? this.captainsList : this.shipsList)  // Liste d'objets dans laquelle rechercher des matchs : capitaines ou navires
            listObj.forEach( obj => { // Parcours de la liste
                variantes=obj.names   // tableau de toutes les variantes du nom, variante majoritaire en premier (classé par ordre du nb. d'occurences)
                searchResultVariantes=this.searchVariantesForRegex(variantes, regex, obj.id)
                if(searchResultVariantes.results.length > 0) {  // des variantes ont matché
                    result.push(searchResultVariantes)  // Ajouter le résultat contenant un tableau des variantes qui ont matché au résultat (pour pouvoir faire des <optgroup>)
                }
            })
        }
        return result
    }

    // Compte le nombre total d'éléments dans les sous-tableaux des résultats de recherche
    countSearchResults(tabOfResults) {
        var result=0

        tabOfResults.forEach(objResSearch => {
            result+=objResSearch.results.length
        })
        return result
    }

    // Construit un tableau des id (uniques) sélectionnés (tableau de chaines de car.)
    getIdTableFromSelectedOptions(selectedOptions) {
        const result=new Array()
        const tabFromIterable=Array.from(selectedOptions)   // Transforme HTMLCollection (iterable) en Array

        tabFromIterable.forEach(option => {
            const id=option.value

            if(!result.includes(id)) {  // Cet id n'est pas déjà dans la liste résultat
                result.push(id) // l'ajouter
            }
        })
        return result
    }

}