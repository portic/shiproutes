// Product.js
/**
 * Représente un produit (nom traduit, avec catégorie), à partir des données lues dans l'API /cargo_categories
 */


export class Product {
    // Membres de l'instance :

    cargoApiData=null    // Données reçues de l'API
    lang="" // fr ou en


    // Membres de la classe :

    // Fonction de comparaison à passer à Array.sort() pour trier les étapes dans l'ordre des catégories, puis des noms de produits
    static compareProductsByCategoryAndName(a, b) {
        var result=0

        // Tri par nom de catégorie, puis par nom de produit
        const catA=a.categoryPortic
        const catB=b.categoryPortic
        result=catA.localeCompare(catB)
        if(result == 0) {
            const nameA=a.commodityStandardized
            const nameB=b.commodityStandardized
            result=nameA.localeCompare(nameB)
        }
        return result
    }

    // Fonction de comparaison à passer à Array.sort() pour trier les étapes dans l'ordre des catégories, puis des noms de produits
    static compareProductsById(a, b) {
        var result=0

        // Tri par recordId
        const idA=a.recordId
        const idB=b.recordId
        result=idA.localeCompare(idB)
        return result
    }


    // Constructeur
    constructor(cargoApiData, lang) {
        this.cargoApiData=cargoApiData
        this.lang=lang
    }
    
    // Renvoie l'id du produit (servant à la jointure avec : all_cargos / destination_all_cargos)
    get recordId() {
        return this.cargoApiData.record_id
    }

    // Renvoie le nom du produit (dans la langue fr, sinon en)
    get commodityStandardized() {
        return (this.lang == "fr" ? this.cargoApiData.commodity_standardized_fr : this.cargoApiData.commodity_standardized_en)
    }

    // Renvoie la catégorie du produit (dans la langue fr, sinon en)
    get categoryPortic() {
        return (this.lang == "fr" ? this.cargoApiData.category_portic_fr : this.cargoApiData.category_portic_en)
    }

    // Affichage dans la console, pour debug
    dump() {
        console.log(this.recordId+" : ["+this.categoryPortic+"] "+this.commodityStandardized)
    }

}