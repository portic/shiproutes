// CargoElement.js
/**
 * Une marchandise de la cargaison (au départ ou à l'arrivée d'une étape)
 */

import { Product } from './Product'
import { ListProducts } from './ListProducts'


export class CargoElement {
    // Membres de l'instance : tous privés !!! (utiliser les getters)

    product=null    // produit retrouvé dans productsList à partir de commodityId
    nameAlt=""  // Nom à mettre si produit introuvable dans le dico
    act=""   // Action (chargement ou déchargement) : In, out, Transit, ...
    qty=null   // Quantité (nombre à arrondir)
    unt=null  // Unité de la quantité (string)


    // Membres de la classe :

    // Fonction de comparaison à passer à Array.sort() pour trier les marchandises dans l'ordre alphabétique du produit (productName)
    static compareCargoElementsByProductName(a, b) {
        // Tri par produit (productName) :
        const productA=a.productName
        const productB=b.productName
        return productA.localeCompare(productB) // comparaison entre chaînes de caractères (en fonction de la locale du navigateur)
    }


    // Constructeur
    constructor(cargoElementApiData, productsList) {
        // Lecture des éléments nécessaires à partir des données de l'API :
        const commodityId=cargoElementApiData.commodity_id
        const commodityPurpose=cargoElementApiData.commodity_purpose
        const cargoItemAction=cargoElementApiData.cargo_item_action
        const quantity=cargoElementApiData.quantity
        const quantityU=cargoElementApiData.quantity_u
        
        // Traitement des données lues :
        this.product=productsList.getProductByRecordId(commodityId) // Retrouve les infos sur le produit si trouvables dans le dictionnaire des produits
        this.nameAlt=commodityPurpose   // Nom alternatif (en français seulement) si produit introuvable dans le dico
        this.act=cargoItemAction     // Chargement, déchargement, ...
        this.qty=quantity  // Quantité (nombre)
        this.unt=quantityU    // Unité (de la quantité)
    }

    // Renvoie le nom du produit (traduit ou non)
    get productName() {
        var result=""

        if(this.product) {  // traduction trouvée
            result=this.product.commodityStandardized   // nom du produit, dans la langue en cours
        } else {    // Pas de traduction du nom du produit dans le dico
            result="- ("+this.nameAlt+")" // Nom alternatif (tout le temps en français)
        }
        return result
    }

    // Mouvement du produit dans le port : chargement, déchargement, ...
    get action() {
        return this.act
    }

    // Renvoie la quantité du produit (null si non-précisée)
    get quantity() {
        var result=null

        if(this.qty) {
            result=Math.round(Number(this.qty)).toString()   // Arrondis de la quantité + conversion en string
        }
        return result
    }

    // Unité (de la quantité, si quantité. null si non-précisée)
    get unit() {
        return this.unt
    }

    // Chaîne représentant le produit (action - productName : quantity unit)
    get displayableString() {
        return this.action+" - "+this.productName+(this.quantity ? " : "+this.quantity+(this.unit ? " "+this.unit : "") : "")
    }

    // Affichage dans la console, pour debug
    dump() {
        console.log(this.displayableString)
    }
}