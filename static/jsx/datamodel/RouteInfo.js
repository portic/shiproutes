// RouteInfo.js
/**
 * Parcours d'un navire ou capitaine
 * Ligne de données pour affichage dans React Table
 * 
 * new Date(), ou Date(1910, 9, 11) (january = 0) https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Date
 */

 export class RouteInfo {
    // Membres de l'instance : que des chaînes de car., dans un {objet}
    routeData={}
    /*
    captainId=""
    captainName=""
    captainCitizenship=""
    captainBirthplace=""
    shipId=""
    shipName=""
    shipFlag=""
    shipHomeport=""
    shipTonnageClass=""
    shipClass=""
    minDate=""
    maxDate=""
    */
    byCaptain=true  // Pour Debug : affichage avec origine capitaine ou navire

    // Membres de la classe (static) :

    
    // Constructeur avec routeData déjà prêt
    constructor(byCaptain, routeData) {
        this.byCaptain=byCaptain
        this.routeData=routeData
    }

    // Renvoie la valeur de nom de l'élément complémentaire du mode de sélection actuel (par capitaine ou navire)
    get complElt() {
        var result=""

        if(this.byCaptain) {    // sélection par capitaine => renvoyer le nom du navire
            result=this.routeData.shipName
        } else {    // sélection par navire : renvoyer le nom du capitaine
            result=this.routeData.captainName
        }
        return result
    }

    // Affichage des données pour debug
    dump() {
        if(this.byCaptain) { // route de capitaine
            console.log("captainId: "+this.routeData.captainId+" captainName: "+this.routeData.captainName+" shipId: "+this.routeData.shipId+" shipName: "+this.routeData.shipName)
        } else {    // route de navire
            console.log("shipId: "+this.routeData.shipId+" shipName: "+this.routeData.shipName+" captainId: "+this.routeData.captainId+" captainName: "+this.routeData.captainName)
        }
    }

}