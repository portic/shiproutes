// LegendChronogram.jsx
/**
 * Légende du Chronogramme (affichée en-dehors du graphique).
 * Remplace le composant <VictoryLegend /> du Chronogramme.
 * 
 * translate prefix : chronogram.legend_
 */

import 'bootstrap/dist/css/bootstrap.min.css';
import { Table } from 'react-bootstrap';
import React from 'react';
import { useTranslation } from 'react-i18next';
import '/static/css/shiproutes.css'

export const LegendChronogram = ({differentiateColors}) => {
    const { t, i18n } = useTranslation();
    const lang=i18n.resolvedLanguage;   // langue actuelle (détectée ou sélectionnée)

    // Rendu du composant :
    return (
        <div className={"d-inline-flex mb-0 px-3 border border-color-axis-chronogram border-2 rounded-2"}>
            <table className="" size="sm">
                <tbody>
                    <tr>
                        <td colSpan={3}>
                            <div className={"mb-2"}>{t("chronogram.legend_title", "Destinations' Uncertainty")}</div>
                        </td>
                    </tr>
                    <tr>
                        <td className={"text-start pe-3"}>
                            <span className={(differentiateColors ? "text-info" : "text-secondary")}><span style={{letterSpacing: "0.075em"}}>{"\u2505"}</span>{"\u2505"}</span>
                            { differentiateColors && <>
                                &nbsp;<span className={"text-lightgreen"}><span style={{letterSpacing: "0.075em"}}>{"\u2505"}</span>{"\u2505"}</span>
                            </>}
                            &nbsp;{t("chronogram.legend_dashed", "inferred")}
                        </td>
                        <td className={"text-start"}>
                            <span className={(differentiateColors ? "text-info" : "text-secondary")+" fw-bold"}><span style={{letterSpacing: '-0.075em'}}>{"\u2014"}</span>{"\u2014"}</span>
                            { differentiateColors && <>
                                &nbsp;<span className={"text-lightgreen fw-bold"}><span style={{letterSpacing: '-0.075em'}}>{"\u2014"}</span>{"\u2014"}</span>
                            </>}
                            &nbsp;{t("chronogram.legend_grey", "declared")}
                        </td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td className={"text-start pe-3"}>
                            <span className={(differentiateColors ? "text-primary" : "text-success")+" fw-bold"}><span style={{letterSpacing: '-0.075em'}}>{"\u2014"}</span>{"\u2014"}</span>
                            { differentiateColors && <>
                                &nbsp;<span className={"text-success fw-bold"}><span style={{letterSpacing: '-0.075em'}}>{"\u2014"}</span>{"\u2014"}</span>
                            </>}
                            &nbsp;{t("chronogram.legend_green", "confirmed")}
                        </td>
                        <td className={"text-start pe-3"}>
                            <span className={"text-warning fw-bold"}><span style={{letterSpacing: '-0.075em'}}>{"\u2014"}</span>{"\u2014"}</span>
                            &nbsp;{t("chronogram.legend_orange", "controversial")}
                        </td>
                        <td className={"text-start"}>
                            <span className={"text-danger fw-bold"}><span style={{letterSpacing: '-0.075em'}}>{"\u2014"}</span>{"\u2014"}</span>
                            &nbsp;{t("chronogram.legend_red", "invalidated")}
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>
    )
}