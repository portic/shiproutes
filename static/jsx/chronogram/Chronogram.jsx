// Chronogram.jsx
/**
 * Graphique temporel des déplacements d'un capitaine / navire
 * Utilise la librairie de graphiques : Victory ( https://formidable.com/open-source/victory/ https://blog.theodo.com/2019/01/data-visualisation-victory-chart/ )
 * 
 * translate prefix : chronogram
 */

import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Row, Col, ButtonGroup, ToggleButton, Form } from 'react-bootstrap';
import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { VictoryChart, VictoryTheme, VictoryAxis, VictoryLine, VictoryLabel, Curve, VictoryZoomContainer, VictoryBrushContainer, createContainer, VictoryLegend, LineSegment } from 'victory'
import { ApiDataReader } from '../datamodel/ApiDataReader';
import { MsgLoad } from '../user_interface/MsgLoad'
import { ListChronoSteps } from '../datamodel/ListChronoSteps'
import { ChronoStep } from '../datamodel/ChronoStep'
import { ChronoPlayer } from './ChronoPlayer'
import { LegendChronogram } from './LegendChronogram'


/**
 * Pour dessiner une flèche placée en bout d'une ligne du graphique
 * props du composant React : 
 * arrowId = N° unique pour la définition (defs) de la flèche au sein du graphique SVG produit par Victory / d3
 * fill (string) = couleur de la flèche
 */
class Arrow extends React.Component {
    render() {
        return (
            <g>
                <defs>
                    <marker
                        id={"arrow"+this.props.arrowId}
                        markerWidth="10"
                        markerHeight="10"
                        refX="9"
                        refY="3"
                        orient="auto"
                        // markerUnits="strokeWidth"
                        markerUnits="userSpaceOnUse"    // Pour garder la flèche fine en bout d'un segment qui serait épais (strokeWidth > 1)
                    >
                        <path d="M0,0 L0,6 L9,3 z" fill={this.props.fill} fillOpacity={this.props.fillOpacity} />
                    </marker>
                </defs>
                <Curve {...this.props} pathComponent={<path markerEnd={"url(#arrow"+this.props.arrowId+")"} />} 
                />
            </g>
        );
    }
}

// Ligne pour la légende du graphique
const MyLine = ({x, y, size, style, ...defProps}) => {
    return (<LineSegment 
        x1={x}
        y1={y}
        x2={x+size}
        y2={y}
        style={style.style} 
        {...defProps}
    />)
}

// Modes d'interraction avec le graphique :
export const MODE_ZOOM=0
export const MODE_PLAYER=1
// valeurs pour la variable d'état : graphMode, dans le composant jsx Chronogram
// valeurs aussi utilisées dans MapOfFrenchPorts.jsx et ses sous-composants

export const Chronogram = ({routes, apiurl, byCaptain, differentiateColors, listSteps, setListSteps, highlightedStepIndex, setHighlightedStepIndex, filterRedSteps, setFilterRedSteps, filterOrangeSteps, setFilterOrangeSteps, graphMode, setGraphMode, stopPlayer, setStopPlayer}) => {
    const { t, i18n } = useTranslation();
    const lang=i18n.resolvedLanguage;   // langue actuelle (détectée ou sélectionnée)

    // Constantes de Look (styles SVG) pour les lignes du graphique :
    const lineStyle_strokeWidth=1   // épaisseur de trait
    const lineStyle_strokeWidth_highlighted=2   // épaisseur de trait en gras
    const lineStyle_opacity=0.8     // transparence
    const lineStyle_opacity_highlighted=0.8     // transparence en gras
    const lineStyle_dashed="3,3"    // pointillés
    const lineStyle={   // style de ligne continue (il faut ajouter la couleur)
        strokeWidth: lineStyle_strokeWidth, 
        strokeOpacity: lineStyle_opacity, 
    }
    const dashedLineStyle={...lineStyle, strokeDasharray: lineStyle_dashed} // style de ligne en pointillés
    const legendLineSize=15 // longueur du trait tracé dans la légende du graphique
    const axisColor="#90a4ae"   // couleur des axes dans le thème "material"

    const nbParcours=routes.length  // Nombre de parcours à afficher

    // Dans le state :
    // * apiDataReader1 : objet pour lire les données de la requête depuis l'API, et les stocker
    const [apiDataReader1, setApiDataReader1] = useState(null)
    // * loadStatus1 : statut du chargement des données de l'API pour le parcours 1
    const [loadStatus1, setLoadStatus1] = useState(ApiDataReader.initialStatus)   // Initialement : données en cours de chargement
    // * apiDataReader2 : objet pour lire les données de la requête depuis l'API, et les stocker
    const [apiDataReader2, setApiDataReader2] = useState(null)
    // * loadStatus2 : statut du chargement des données de l'API pour le parcours 2
    const [loadStatus2, setLoadStatus2] = useState({...ApiDataReader.initialStatus, loading: (nbParcours == 2)})   // Initialement : données chargées sans erreur (si un seul parcours à charger), sinon données en cours de chargement
    // * zoomDomain : zone du zoom sur le graphique
    const [zoomDomain, setZoomDomain] = useState(null)
    // * brushDomain : zone de sélection persistante en fond gris sur le graphique
    const [brushDomain, setBrushDomain] = useState(null)   
    

    // Prépare la requête à l'endpoint /travels de l'API, pour recevoir la liste des segments du parcours 1 ou 2
    const composeRequetePourParcours = (noParcours) => {
        var result=`${apiurl}/travels/?format=json&lang=${lang}&params=${ChronoStep.paramsRead}`

        const routeToLoad=routes[noParcours-1] // Route dont on veut charger les segments
        if(routeToLoad.captainId != "") {   // Il y a un captainId
            result+=`&captain_id=${routeToLoad.captainId}`
        }
        if(routeToLoad.shipId != "") {   // Il y a un shipId
            result+=`&ship_id=${routeToLoad.shipId}`
        }
        return result
    }

    // Déclencher le chargement des données de l'API, pour le parcours dont on passe le N°
    const lanceChargementDonneesAPIpourParcours = (noParcours) => {
        const requete=composeRequetePourParcours(noParcours)  // nouvelle requête
        // console.log(requete)
        const newApiDataReader=new ApiDataReader(requete, (noParcours == 1 ? setLoadStatus1 : setLoadStatus2))   // Construire objet de requêtage
        newApiDataReader.chargerDonnees()   // Lancer le chargement (asynchrone) des données
        // mémo. ApiDataReader dans le state, pour garder les données reçues (qui seront dans cet objet) :
        switch(noParcours) {
            case 1 : 
                setApiDataReader1(newApiDataReader)
                break;
            case 2 :
                setApiDataReader2(newApiDataReader)
                break;
        }
    }

    // Charger les données de l'API pour les parcours
    useEffect(() => {
        // 1er parcours :
        lanceChargementDonneesAPIpourParcours(1)    // Chargement des données
        // 2ème parcours :
        if(nbParcours == 1) {   // un seul parcours à charger
            setApiDataReader2(null) // pas de 2ème chargement
        } else {    // il faut charger un 2ème parcours
            lanceChargementDonneesAPIpourParcours(2)    // Chargement des données
        }
    }, [routes]); // Effet exécuté initialement + ré-exécuté seulement si les [variables] changent

    // Quand les données de l'API sont chargées, construire la liste des étapes du parcours
    useEffect(() => {
        if((!loadStatus1.loading && !loadStatus1.error) && (!loadStatus2.loading && !loadStatus2.error)) {  // toutes les données sont chargées sans erreur
            // Crée la liste des étapes du parcours, à partir des données chargées de l'API :
            // construire le tableau des données chargées :
            const tabApiData=new Array()
            tabApiData.push(apiDataReader1.apiData)
            if(nbParcours == 2) {
                tabApiData.push(apiDataReader2.apiData)
            }
            // construire la liste des étapes à partir du tableau :
            const listSteps=new ListChronoSteps(tabApiData, differentiateColors, lang, byCaptain, filterRedSteps, filterOrangeSteps)
            // listSteps.dump()
            setListSteps(listSteps) // stockage dans le state
        }
    }, [loadStatus1, loadStatus2]); // Effet exécuté initialement + ré-exécuté seulement si les [variables] changent

    // Quand la liste des étapes est construite, initialiser le graphique
    useEffect(() => {
        if(listSteps) {
            changeGraphMode(MODE_ZOOM)    // Fixe le mode par défaut du graphique
        }
    }, [listSteps]) // Effet exécuté initialement + ré-exécuté seulement si les [variables] changent

    // Indique si la date se trouve dans la zone de zoom (true) ou pas (false)
    const isDateInZoomDomain = (date) => {
        var result=true

        if(zoomDomain) {    // zoomDomain n'est pas nul
            const dateDeb=zoomDomain.x[0]
            const dateFin=zoomDomain.x[1]
            if(!((date >= dateDeb)&&(date <= dateFin))) {
                result=false
            }
        }
        return result
    }

    // Indique si on est en train de zoomer sur le graphique (true), ou si on affiche le graphique en entier (false)
    const isChartZoomed = () => {
        var result=false    // par défaut (initialement), le graphique est affiché sans zoomer

        if(listSteps && zoomDomain ) {    // on affiche un graphique (listSteps n'est pas nul), et zoomDomain n'est pas nul (on a déjà zoomé/ on est en train de zoomer)
            const stepsDateDomain=listSteps.getDateDomain() // Cherche la plus petite et la plus grande date des étapes du graphique
            if((zoomDomain.x[0] >= stepsDateDomain.x[0]) || (zoomDomain.x[1] <= stepsDateDomain.x[1])) {    // Le zoom est (en partie ?) à l'intérieur des données du graphique
                result=true
            }
        }
        return result
    }

    // Si la latitude n'est pas précisée, renvoie une sorte de valeur médiane de latitude du graphique (renvoie quelque chose de représentable)
    const getLatitudeValide = (valeur) => {
        const latDomain=listSteps.getLatDomain()
        const latMin=latDomain.y[0]
        const latMax=latDomain.y[1]
        var ecartLat=(latMax - latMin)
        if(ecartLat == 0) {    // Pour que la valeur se distingue
            ecartLat=3.0
        }
        const latMediane=latMin + ecartLat/2.0
        var result=latMediane

        if(!isNaN(valeur)) {    // la valeur est un nombre
            result=valeur
        }
        return result
    }

    // Renvoie la mise en valeur (brushStyle=couleur de fond) de l'étape en cours pour le VictoryBrushContainer
    const getBrushStyle = () => {
        var result={stroke: "transparent", fill: "black", fillOpacity: 0.1} // gris par défaut

        if(nbParcours > 1) {    // ne colorer la mise en valeur que si plus d'un parcours
            if(listSteps && (highlightedStepIndex != -1)) { // Liste des étapes chargée, et étape sélectionnée => on peut obtenir l'étape en cours
                const currentStep=listSteps.getStepByIndex(highlightedStepIndex)
                if(currentStep) {   // étape en cours trouvée
                    const routeIndex=currentStep.routeIndex // lire le N° de parcours
                    switch(routeIndex) {
                        case 1 :    // parcours 1
                            result={...result, fill: "cornflowerblue"} // bleu
                            break;
                        case 2 :    // parcours 2
                            result={...result, fill: "green"}   // vert
                            break;
                    }
                }
            }
        }        
        return result
    }

    // Renvoie les lignes (segments de droite) des étapes du parcours (sous-partie de la construction du graphique ci-dessous)
    const getChartLines = () => {
        return listSteps.steps.map((step, stepIndex) => {    // Parcourir la liste des étapes
            // Données d'une étape : les deux points du segment (departure, destination) pour cette étape
            const data=[
                {   // departure
                    x: step.departureDateChrono, // abscisse
                    y: getLatitudeValide(step.departureLat), // ordonnée
                    portName: step.departureName // étiquette du nom (du port) de l'étape
                },
                {   // destination
                    x: step.destinationDateChrono, 
                    y: getLatitudeValide(step.destinationLat),
                    portName: step.destinationName
                }
            ]
            // Style du trait : couleur et pointillés selon données d'incertitude de l'étape
            var dataStyle=(step.isDashed ? dashedLineStyle : lineStyle) // style de base de la ligne : en pointillés ou continue
            dataStyle={...dataStyle, stroke: step.color}    // Ajouter la couleur de la ligne
            if(stepIndex == highlightedStepIndex) {  // Mettre ce segment en gras
                dataStyle={...dataStyle, strokeWidth: lineStyle_strokeWidth_highlighted, strokeOpacity: lineStyle_opacity_highlighted}
            }
            return (<VictoryLine 
                key={step.rank+((step.routeIndex-1)*10000)}
                data={data} 
                labels={({ datum }) => (isDateInZoomDomain(datum.x) ? datum.portName : "")} // cause bug des labels affichés hors graphique lors du zoom
                labelComponent={<VictoryLabel renderInPortal dy={-9} verticalAnchor={"end"} />} 
                // Interpolation :  "basis", "bundle", "cardinal", "catmullRom", "linear", "monotoneX", "monotoneY", "natural", "step", "stepAfter", "stepBefore"
                interpolation={"linear"}
                style={{
                    data: dataStyle,
                    // labels: { fontSize: ({ text }) => text.length > 10 ? 8 : 12 },
                    // parent: { border: "1px solid #ccc" }
                }} 
                dataComponent={<Arrow 
                    arrowId={step.rank+((step.routeIndex-1)*10000)} 
                    fill={step.color} 
                    fillOpacity={(stepIndex == highlightedStepIndex) ? lineStyle_opacity_highlighted : lineStyle_opacity} 
                />} // Flèche de direction en bout de segment
            >
            </VictoryLine>)
        })
    }

    // Effectue le dessin du chronogramme
    const getChart = () => {
        return (<VictoryChart  
            width={500} // Taille en pixels de l'image SVG rendue pour le graphique (l'image sera étirée à la taille max. du container)
            height={400} 
            padding={{top: 10, bottom: 60, left: 60, right: 20}} // place à laisser autour du graphique (pour les axes et les débordements des labels)
            theme={VictoryTheme.material} 
            domainPadding={{x: [60, 60], y: [0, 22]}} // marge autour des données dessinées [en début, en fin] de chaque axe, pour : débordements des labels, légende, flèche, ...
            containerComponent={    // Composant englobant, pour pouvoir zoomer horizontalement sur une étape, avec la souris ou avec le player
                (graphMode == MODE_ZOOM) ? <VictoryZoomContainer
                    zoomDimension="x"   // zoom horizontal (seulement)
                    zoomDomain={zoomDomain}
                    onZoomDomainChange={setZoomDomain} // mémo. de la zone du graphique qui est affichée (suite au zoom) 
                    
                /> : <VictoryBrushContainer
                    brushDimension="x"
                    brushDomain={brushDomain}
                    brushStyle={getBrushStyle()}
                />}
            title={(graphMode == MODE_ZOOM) ? t("chronogram.zoom_invite", "Zoom with mouse wheel - Pan by click-n-drag.") : undefined} // Aide au survol du graphique, pour savoir comment zoomer sur une étape
        >
            <VictoryAxis    // axe des x
                label={t("chronogram.axis_time", "Time")} // nom de l'axe
                tickFormat={(datetime) => { // Formattage des valeurs sur l'axe
                    return ChronoStep.formatDateForLang(new Date(datetime), lang)
                } }
                tickLabelComponent={<VictoryLabel angle={-30} textAnchor="end" dy={10} />} // composant d'affichage des valeurs : valeurs inclinées + décalage vers le bas
                // Valeurs de dates sur l'axe :
                tickValues={listSteps.getTickDateValues(isChartZoomed())} // true : garder toutes les valeurs (même si superpositions, pour pouvoir zoomer)  
            />
            <VictoryAxis
                dependentAxis // axe des y
                label={t("chronogram.axis_latitude", "Latitude")} // nom de l'axe
                tickLabelComponent={<VictoryLabel dx={-10} />} // composant d'affichage des valeurs sur l'axe vertical : simple décalage vers la gauche
            />
            {/*<VictoryLegend x={70} y={15} // point d'ancrage : en haut à gauche du rectangle de la légende - Taille en fonction du contenu
                orientation={"vertical"} // les éléments de légende s'empilent verticalement
                itemsPerRow={2} // max. de valeurs empilées; ensuite on se décale vers la droite pour empiler à nouveau
                gutter={20}
                title={t("chronogram.legend_title", "Destinations' Uncertainty")}
                centerTitle
                style={{
                    border: { stroke: axisColor }, // bordure du rectangle
                    // title: { fontWeight: "bold" }, // titre en gras
                }} 
                data={[
                    { name: t("chronogram.legend_dashed", "inferred"), symbol: {size: legendLineSize, style: {...dashedLineStyle, stroke: "grey"}} }, // pointillé
                    { name: t("chronogram.legend_green", "confirmed"), symbol: {size: legendLineSize, style: {...lineStyle, stroke: "green"}} }, 
                    // { name: t("chronogram.legend_blue", "deduced"), symbol: {size: legendLineSize, style: {...lineStyle, stroke: "blue"}} }, 
                    { name: t("chronogram.legend_grey", "declared"), symbol: {size: legendLineSize, style: {...lineStyle, stroke: "grey"}} }, 
                    { name: t("chronogram.legend_orange", "controversial"), symbol: {size: legendLineSize, style: {...lineStyle, stroke: "orange"}} }, 
                    { name: t("chronogram.legend_red", "invalidated"), symbol: {size: legendLineSize, style: {...lineStyle, stroke: "red"}} }
                ]}
                dataComponent={<MyLine />} // Composant pour dessiner l'élément de la légende : segment de droite
            />*/} 
            {/* Composants <VictoryLine /> pour les étapes du parcours : */}
            {getChartLines()}   
        </VictoryChart>)
    }

    // Pour le mode Player, sélectionne sur fond gris l'étape dont on passe le N° (de : zéro, à : listSteps.steps.length - 1)
    const selectStep = (index) => {
        const newStep=listSteps.getStepByIndex(index)

        if(newStep) {
            setHighlightedStepIndex(index) // segment en gras
            const newBrushDomain=newStep.brushDomain   // objet permettant de sélectionner la période de temps de l'étape
            setBrushDomain(newBrushDomain)  // sélection sur fond gris de l'étape sur le graphique
        }
    }

    // Applique un mode (zoom, player, ...) au graphique
    const changeGraphMode = (newMode) => {
        // Pas de segment mis en évidence initialement :
        setHighlightedStepIndex(-1)
        // Faire le changement de mode :
        switch(newMode) {
            case MODE_ZOOM :    // Passage en mode Zoom :
                setZoomDomain(null) // Afficher tout le graphe
                break;
            case MODE_PLAYER :  // Passage en mode Player :
                setZoomDomain(null) // Tout afficher initialement (pas de zoom)
                setBrushDomain(null)    // Afficher tout le graphe
                selectStep(0)   // sélectionner la 1ère étape
                break;
        }
        // Mémoriser le nouveau mode du graphique :
        setGraphMode(newMode)
    }

    // Appui sur un des boutons de changement de mode (zoom, player, ...) du graphique
    const onBtnGrpGraphModeChange = (e) => {
        const newMode=Number.parseInt(e.currentTarget.value) // Lecture du nom du bouton cliqué

        changeGraphMode(newMode)    // Applique le nouveau mode du graphique
    }

    // Coche / décoche une case de filtrage (par type d'étape)
    const onFilterCheckboxesChange = (e) => {
        const newValue=e.target.checked

        switch(e.target.name) {
            case "filterRedSteps" : // Etapes en rouge
                setFilterRedSteps(newValue) // mémo. nouveau mode de filtrage
                // console.log("filterRedSteps : "+newValue);
                // Appliquer le filtrage :
                listSteps.setFilter(newValue, filterOrangeSteps)    // Filtrage liste : seul le filtrage des étapes en ROUGE change
                break;
            case "filterOrangeSteps" :  // Etapes en orange
                setFilterOrangeSteps(newValue)  // mémo. nouveau mode de filtrage
                // console.log("filterOrangeSteps : "+newValue);
                // Appliquer le filtrage :
                listSteps.setFilter(filterRedSteps, newValue)   // Filtrage liste : seul le filtrage des étapes en ORANGE change
                break;
        }

        // Rafraîchir affichage : Pas de segment mis en évidence initialement (pour forcer le rafraîchissement)
        setHighlightedStepIndex(-1)
    }



    // Rendu du composant :
    
    // Chargement en cours
    if((loadStatus1.loading) || (loadStatus2.loading)) {
        return (<MsgLoad erreur={false} message={t('mainComponent.load_in_progress', 'Data loading ...')} />)
    }

    // Erreur de chargement
    if(loadStatus1.error || loadStatus2.error) {
        var errorResult=null

        if(loadStatus1.error) { // Renvoyer seulement la 1ère erreur
            // Dev :
            errorResult=<MsgLoad erreur={true} message={<>{t('mainComponent.load_error', 'Error while loading data : ')}<br />{loadStatus1.apiMessage}</>} info={loadStatus1.apiInfo} infoWrap={loadStatus1.apiInfoWrap} />
            // Prod :
            // errorResult=<MsgLoad erreur={true} message={<>{t('mainComponent.load_error', 'Error while loading data : ')}<br />{loadStatus1.apiMessage}</>} />

        } else {    // Renvoyer la 2ème erreur
            // Dev :
            errorResult=<MsgLoad erreur={true} message={<>{t('mainComponent.load_error', 'Error while loading data : ')}<br />{loadStatus2.apiMessage}</>} info={loadStatus2.apiInfo} infoWrap={loadStatus2.apiInfoWrap} />
            // Prod :
            // errorResult=<MsgLoad erreur={true} message={<>{t('mainComponent.load_error', 'Error while loading data : ')}<br />{loadStatus2.apiMessage}</>} />
        }
        return (errorResult)
    }

    // Données chargées sans erreur
    if((!loadStatus1.loading && !loadStatus1.error) && (!loadStatus2.loading && !loadStatus2.error)) {
        // apiDataReader.apiData.forEach(segment => {console.log(segment)})
        if(!listSteps) {
            return null
        } else {
            return (<>
                <Row className={"pt-2"}>
                    <Col className={"text-center"}>
                        <LegendChronogram 
                            differentiateColors={differentiateColors}
                        />
                    </Col>
                </Row>
                <Row className={"pb-2"}>
                    <Col>
                        {getChart()}
                    </Col>
                </Row>
                <Row className={"py-0"}>
                    <Col className={"d-flex align-items-center"}>
                        <span>{t("chronogram.fct_hide_steps", "Hide steps")} : </span>&nbsp;
                        <Form.Check inline type='checkbox' name="filterRedSteps" id="checkbox-filterRedSteps" label={<><span className="text-danger" style={{letterSpacing: '-1px'}}>&mdash;&mdash;</span> {t("chronogram.legend_red", "invalidated")}</>} defaultChecked={filterRedSteps} onChange={onFilterCheckboxesChange} />
                        <Form.Check inline type='checkbox' name="filterOrangeSteps" id="checkbox-filterOrangeSteps" label={<><span className="text-warning" style={{letterSpacing: '-1px'}}>&mdash;&mdash;</span> {t("chronogram.legend_orange", "controversial")}</>} defaultChecked={filterOrangeSteps} onChange={onFilterCheckboxesChange} />
                    </Col>
                </Row>
                <Row className={"py-2"}>
                    <Col>
                        <ButtonGroup>
                            <ToggleButton id={"chronogramBtnModeZoom"} type={"radio"} name={"radio"} variant={"outline-secondary"}
                                value={MODE_ZOOM} 
                                checked={graphMode == MODE_ZOOM}
                                onChange={onBtnGrpGraphModeChange} 
                                title={t("chronogram.btn_zoom_expl", "Magnify / Pan a portion of the graph with the mouse")}
                            >
                                {t("chronogram.btn_zoom", "Zoom")}
                            </ToggleButton>
                            <ToggleButton id={"chronogramBtnModePlayer"} type={"radio"} name={"radio"} variant={"outline-secondary"}
                                value={MODE_PLAYER} 
                                checked={graphMode == MODE_PLAYER}
                                onChange={onBtnGrpGraphModeChange} 
                                title={t("chronogram.btn_player_expl", "Cycle through the steps of the route")}
                            >
                                {t("chronogram.btn_player", "Player")}
                            </ToggleButton>
                        </ButtonGroup>
                        { (graphMode == MODE_PLAYER) && <ChronoPlayer 
                            steps={listSteps.steps} 
                            selectStep={selectStep} 
                            stopPlayer={stopPlayer} 
                            setStopPlayer={setStopPlayer}
                        />}
                    </Col>
                </Row>
            </>)
        }
    }
}