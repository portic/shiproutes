// ChronoPlayer.jsx
/**
 * Player associé au chronogramme des étapes d'un parcours de capitaine / navire
 * Permet de faire défiler les étapes (steps) du chronogramme
 * Rq.: Si la liste des étapes change, il faut sélectionner l'étape 0 (car un reset a automatiquement lieu sur cette étape, si elle est valide)
 * 
 * translate prefix : chronogram
 */

import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Row, Col, ButtonGroup, ToggleButton } from 'react-bootstrap';
import React, { useState, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { solid, regular, brands } from '@fortawesome/fontawesome-svg-core/import.macro' // <-- import styles to be used


export const ChronoPlayer = ({steps, selectStep, stopPlayer, setStopPlayer}) => {
    const { t, i18n } = useTranslation()

    const maxStepIndex=steps.length - 1

    // Dans le state :
    // * currentStepIndex : indice de l'étape courante
    const [currentStepIndex, setCurrentStepIndex] = useState(0)
    // * displayDuration : durée (en secondes) d'affichage d'une étape du parcours
    const [displayDuration, setDisplayDuration] = useState(3)
    // * playing : true -> mode play (timer auto), false -> mode pause (manuel)
    const [playing, setPlaying] = useState(false)   // en pause initialement

    // Timer pour faire avancer le player automatiquement si playing
    useEffect(() => {
        if(playing) {   // démarrage auto.
            // Effet :
            const timeoutId=setTimeout(() => {
                nextStep()  // Avancer d'une étape
            }, displayDuration*1000)
            // Nettoyage de l'effet :
            return () => {
                clearTimeout(timeoutId)
            }
        }
    }, [currentStepIndex, displayDuration, playing, steps])   // Exécuté à chaque affichage ! (optimisé avec les [variables vraiment indispensables])

    // Reset du player si la liste des étapes change
    useEffect(() => {
        if(steps) {
            setStep(0)  // Etape 0 sélectionnée si elle existe
        }
    }, [steps])

    // Arrête le player si une demande est reçue (stopPlayer == true), et RAZ demande
    useEffect(() => {
        if(stopPlayer) {    // On a demandé d'arrêter le player
            if(playing) {   // si Player en mode play (avance auto)
                setPlaying(false)   // Player en mode pause (avance manuelle)
            }
            setStopPlayer(false)    // RAZ demande
        }
    }, [stopPlayer])

    // Indique si la valeur est dans la plage possible
    const isIndexValid = (index) => {
        return ((index >= 0) && (index <= maxStepIndex))
    }

    // Change l'étape en cours
    const setStep = (index) => {
        var result=false    // par défaut, opération pas faite

        if(isIndexValid(index)) {
            selectStep(index)   // Change l'étape sur fond gris dans le chronogramme parent
            setCurrentStepIndex(index)  // mémo. index de l'étape courante
            result=true // opération faite
        }
        return result
    }

    // Renvoie l'index de l'étape suivante (si c'est possible). Reboucle si on était à la dernière étape. Renvoie -1 si impossible
    const getNextStepIndex = () => {
        var indexOk=false
        var nextIndex=currentStepIndex+1

        if(isIndexValid(nextIndex)) {
            indexOk=true
        } else {    // Index hors plage : voir à reboucler
            nextIndex=0 // reboucler
            if(nextIndex != currentStepIndex) { // On ne fait pas du sur-place
                indexOk=true
            }
        }
        return (indexOk ? nextIndex : -1)
    }

    // Passe à l'étape suivante
    const nextStep = () => {
        var result=false    // par défaut, opération pas faite

        const nextIndex=getNextStepIndex()
        // console.log("nextIndex: "+nextIndex)
        if(nextIndex != -1) {   // opération possible
            result=setStep(nextIndex)
        }
        return result
    }

    // Renvoie l'index de l'étape précédente (si c'est possible). Reboucle si on était à la première étape. Renvoie -1 si impossible
    const getPrecStepIndex = () => {
        var indexOk=false
        var precIndex=currentStepIndex-1

        if(isIndexValid(precIndex)) {
            indexOk=true
        } else {    // Index hors plage : voir à reboucler
            precIndex=maxStepIndex // reboucler
            if(precIndex != currentStepIndex) { // On ne fait pas du sur-place
                indexOk=true
            }
        }
        return (indexOk ? precIndex : -1)
    }

    // Passe à l'étape précédente
    const precStep = () => {
        var result=false    // Par défaut, opération pas effectuée

        const precIndex=getPrecStepIndex()
        if(precIndex != -1) {   // opération possible
            result=setStep(precIndex)
        }
        return result
    }

    




    // Affichage :
    const etapeNo=currentStepIndex + 1
    const nbTotalEtapes=maxStepIndex + 1

    return (<div className="d-inline-flex align-items-center">
        <div className={"ps-2"} >
            <button className="btn btn-sm btn-light" onClick={() => setStep(0)} disabled={currentStepIndex == 0}>
                <FontAwesomeIcon icon={solid('fast-backward')} size="lg" fixedWidth title={t("chronogram.player_first", "First step")} />
            </button>{' '}
        </div>
        <div className={"ps-2"} >
            <button className="btn btn-sm btn-light" onClick={() => precStep()} disabled={getPrecStepIndex() == -1}>
                <FontAwesomeIcon icon={solid('backward')} size="lg" fixedWidth title={t("chronogram.player_prec", "Former step")} />
            </button>{' '}
        </div>
        <div className={"ps-2"} >
            <span title={t("chronogram.player_current", "Current step")}>
                <strong>{`${etapeNo} / ${nbTotalEtapes}`}</strong>{' '}
            </span>
        </div>
        <div className={"ps-2"} >
            <button className="btn btn-sm btn-light" onClick={() => nextStep()} disabled={getNextStepIndex() == -1}>
                <FontAwesomeIcon icon={solid('forward')} size="lg" fixedWidth title={t("chronogram.player_next", "Next step")} />
            </button>{' '}
        </div>
        <div className={"ps-2"} >
            <button className="btn btn-sm btn-light" onClick={() => setStep(maxStepIndex)} disabled={currentStepIndex == maxStepIndex}>
                <FontAwesomeIcon icon={solid('fast-forward')} size="lg" fixedWidth title={t("chronogram.player_last", "Last step")} />
            </button>{' '}
        </div>
        {/*<div className="d-inline-flex align-items-center">
            <label className="form-label mb-0" htmlFor="stepNumberInput">{t("chronogram.player_goto", "Go to step")}&nbsp;</label>
            <input className="form-control" type="number" id="stepNumberInput" defaultValue={currentStepIndex+1} onChange={e => {
                const stepIndex = e.target.value ? Number(e.target.value) - 1 : -1
                setStep(stepIndex)
            }} style={{ width: '75px' }} />{' '}
        </div>*/}
        <div className={"ps-2"} >
            <select className="custom-select" value={displayDuration} onChange={e => setDisplayDuration(Number(e.target.value))} title={t("chronogram.player_duration_expl", "Step display duration")} >
                {[3, 5, 7, 10].map(displayDuration => (
                    <option key={displayDuration} value={displayDuration}>{t("chronogram.player_duration", "{{ displayDuration }} s. / step", {
                        displayDuration: displayDuration
                    })}</option>
                ))}
            </select>
        </div>
        <div className={"ps-2"} >
            <button className="btn btn-sm btn-light" onClick={() => setPlaying(!playing)} >
                <FontAwesomeIcon icon={playing ? solid('pause') : solid('play')} size="lg" fixedWidth 
                    title={playing ? t("chronogram.player_pause", "Manual advance") : t("chronogram.player_play", "Automatic advance")} 
                />
            </button>{' '}
        </div>
    </div>)
}