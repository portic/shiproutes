// UrlButton.jsx
/**
 * Bouton permettant d'obtenir l'URL correspondant au(x) parcour(s) actuellement affiché(s), avec tous les paramètres conservés à l'identique
 * 
 * translate prefix : urlButton
 */
import 'bootstrap/dist/css/bootstrap.min.css';
import { Col, Button } from 'react-bootstrap';
import React, { useState } from 'react';
import { Trans, useTranslation } from 'react-i18next';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { solid, regular, brands } from '@fortawesome/fontawesome-svg-core/import.macro' // <-- import styles to be used

export const UrlButton = ({byCaptain, captainId1, shipId1, captainId2, shipId2, filterRedSteps, filterOrangeSteps}) => {
    const { t } = useTranslation();

    // Dans le state :
    // * showingCopyDone : true => le message confirmant l'action est visible
    const [showingCopyDone, setShowingCopyDone] = useState(false);
    
    // Analyse de window.location (debug)
    const showLoc = () => {
        var oLocation = location
        var aLog = ["Property (Typeof): Value", "location (" + (typeof oLocation) + "): " + oLocation ]

        for (var sProp in oLocation) {
            aLog.push(sProp + " (" + (typeof oLocation[sProp]) + "): " + (oLocation[sProp] || "n/a"));
        }
        console.log(aLog.join("\n"));
    }  

    // Traitement du clic sur le bouton
    const copyUrl = () => {
        // * Composer l'URL :
        var url=`${window.location.origin}/index.html?by_captain=${byCaptain}&captain_id_1=${captainId1}&ship_id_1=${shipId1}`  // 1er parcours
        if((captainId2 !== null) && (shipId2 !== null)) {   // affichage d'un deuxième parcours
            url=url+`&captain_id_2=${captainId2}&ship_id_2=${shipId2}`
        }
        url=url+`&filter_red_steps=${filterRedSteps}&filter_orange_steps=${filterOrangeSteps}`
        // console.log(url)
        // showLoc()

        // * Copier l'url dans le presse-papier :
        // Essayer d'abord avec l'API Clipboard :
        // - l'API Clipboard est disponible uniquement pour les contextes sécurisés
        // - Pour les scripts de page, la permission "clipboard-write" doit être demandée via l'API navigator.permissions
        var clipboardApiOk=false;
        if (window.isSecureContext) {   // Contexte sécurisé (localhost ou https)
            navigator.permissions.query({name: "clipboard-write"})  // Demander la permission d'écrire dans le presse-papier
                .then((result) => {   // objet Permissions reçu 
                    if (result.state == "granted" || result.state == "prompt") {    // On peut alors écrire dans le presse-papier
                        navigator.clipboard.writeText(url).then(() => { // Succès écriture
                            clipboardApiOk=true;   
                        }, (error) => {  // échec écriture
                            console.error(t("urlButton.clipboard_writeText_failure", "navigator.clipboard.writeText() failed to write URL to clipboard : ")+error)
                          })
                    }
                }, (error) => { // erreur en demandant la permission 'clipboard-write'
                    console.error(t("urlButton.clipboard-write_query_error", "Error while querying 'clipboard-write' permission : ")+error)
                })
        } else {    // erreur : contexte non-sécurisé
            console.error(t("urlButton.not_a_secure_context", "The application is not running in a secure context"))
        }
        
        // Essayer ensuite avec execCommand("copy") :
        var execCommandCopyOk=false
        if(!clipboardApiOk) {   // La méthode précédente a échoué
            var textArea = document.createElement("textarea");
            textArea.value = url;
  
            // Avoid scrolling to bottom
            textArea.style.top = "0";
            textArea.style.left = "0";
            textArea.style.position = "fixed";

            document.body.appendChild(textArea);
            textArea.focus();
            textArea.select();

            try {
                execCommandCopyOk = document.execCommand('copy');
                if (!execCommandCopyOk) {
                    console.error(t("urlButton.execCommand_copy_failure", "document.execCommand('copy') failed to write URL to clipboard"))
                }
            } catch (error) {
                execCommandCopyOk=false;
                console.error(t("urlButton.execCommand_copy_failure", "document.execCommand('copy') failed to write URL to clipboard")+" : "+error)
            }

            document.body.removeChild(textArea)
        }

        // * Au final :
        // Si échec total, copier l'URL dans la barre d'adresse (redirection) :
        if (!clipboardApiOk && !execCommandCopyOk) {    // Echec
            // Signaler le problème à l'utilisateur dans une boîte de dialogue :
            const warningMsg=t("urlButton.no_access_to_clipboard_redirect", "Unable to access the clipboard.\nThe URL will be pasted to the address bar : you may copy it from there.")
            window.alert(warningMsg)  // Boîte de dialogue d'affichage pour l'utilisateur
            // Rediriger le navigateur vers l'URL :
            window.location=url
        } else {    // Réussite
            // Montrer que l'URL a été copiée :
            setShowingCopyDone(true)
            // Lancer une tempo pour annuler l'affichage au bout de 3 secondes :
            setTimeout(() => {setShowingCopyDone(false)}, 3000)
        }
    }

    // Affichage du composant : bouton avec icône + message de confirmation si (showingCopyDone == true)
    return (
        <Col xs={"auto"} className={"ps-0"}>
            <Button 
                variant={"outline-secondary"}
                className={showingCopyDone ? "" : "border border-1 shadow-none"} 
                title={t("urlButton.description", "Copy URL with current settings")} 
                onClick={() => {copyUrl()}}
            >
                <FontAwesomeIcon icon={solid('link')} size="lg" />
            </Button>
            { showingCopyDone && <span className={"fa-fade"}>
                <Trans i18nKey="urlButton.copy_done"> done</Trans>
            </span> }
        </Col>
    )
}