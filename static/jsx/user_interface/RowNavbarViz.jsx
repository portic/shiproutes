// RowNavbarViz.jsx
/**
 * Barre de navigation sous forme de Row (ligne) pour l'application React VizSources
 * 
 * translate prefix : navbar
 */

import 'bootstrap/dist/css/bootstrap.min.css';
import { Row, Col, Image } from 'react-bootstrap';
import React from 'react';
import '../../css/shiproutes.css'
import { Trans, useTranslation } from 'react-i18next';
import { LangSelector } from '../translate/LangSelector'
// Assets
import PorticLogoTxt from '../../images/portic_txt_fond_gris.jpg'

export const RowNavbarViz = ({bgColorClass,txtClass}) => {
    const { t, i18n } = useTranslation();
    const lang=i18n.resolvedLanguage;   // langue actuelle (détectée ou sélectionnée)

    return (
        <Row className={`mt-2 py-2 px-0 mx-0 justify-content-between align-items-center ${bgColorClass} ${txtClass}`}>
            <Col xs={"auto"} className={"ps-2 pe-0"} >
                <a href={"https://anr.portic.fr/acces-visualisations/"} title={t('navbar.back_link_description', 'Return to visualizations list')}>
                    <div className={"d-flex align-items-center"} >
                        <Image className={"d-inline-block"} src={PorticLogoTxt} />&nbsp;<span className={""} >
                            <Trans i18nKey="navbar.back_link_txt">Visualizations</Trans>
                        </span>
                    </div>
                </a>
            </Col>
            <Col xs={"auto"} className={"px-0"} >
                <Trans i18nKey="navbar.app_title">Routes of Captains and Ships</Trans>
            </Col>
            <Col xs={"auto"} className={"ps-0 pe-2"} >
                <div className={"d-flex align-items-center"} >
                    <LangSelector bgColorClass={bgColorClass} />&nbsp;
                    <a className={"mx-0"} href={`static/manual/${lang}/index.html`} title={t('navbar.link_to_manual_description', 'User Manual')} >
                        <Trans i18nKey="navbar.link_to_manual_txt">Help</Trans>
                    </a>
                </div>
            </Col>
        </Row>
    )
}