// RowControlsViz.jsx
/**
 * Eléments d'interface utilisateur
 * 
 * Sélection de parcours par capitaine, ou par navire (choix par boutons) + affichage des choix faits
 * 
 * translate prefix : controlsViz
 */

import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Row, Col, Form, Button, Modal } from 'react-bootstrap';
import React, { useState, useEffect } from 'react';
import { Trans, useTranslation } from 'react-i18next';
import '../../css/shiproutes.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { solid, regular, brands } from '@fortawesome/fontawesome-svg-core/import.macro' // <-- import styles to be used
import { ListCaptainsShips } from '../datamodel/ListCaptainsShips'
import { FormSearchRoutes } from './FormSearchRoutes'


export const RowControlsViz = ({apiData, byCaptain, setRoutesSelected, urlButton, captainToDisplay1, shipToDisplay1, captainToDisplay2, shipToDisplay2, differentiateColors, highlightRouteNb}) => {
    const { t } = useTranslation();

    // Dans le state :
    // Rq.: tout ce qui commence par "modal" est une sélection temporaire, prise en compte uniquement si on valide (si on annule, c'est perdu)
    // * modalByCaptain : mode d'affichage de la boîte de dialogue de sélection de parcours (true = par capitaine, false = par navire)
    const [modalByCaptain, setModalByCaptain] = useState(false);
    // * showModal : indique si la boîte de dialogue modale de sélection de parcours est visible
    const [showModal, setShowModal] = useState(false);
    // * listCaptains : liste des capitaines (obtenue par l'API)
    const [listCaptainsShips, setListCaptainsShips] = useState(new ListCaptainsShips(apiData));
    // * selectedRoutes : tableau d'objets routeData (cf. RouteInfo.js), des routes sélectionnées dans la boîte modale
    const [modalSelectedRoutes, setModalSelectedRoutes] = useState([])

    // Clic sur le bouton "par capitaine"
    const handleClickBtnCaptain = () => {
        setModalByCaptain(true)  // La boîte de dialogue doit proposer la recherche par nom du capitaine
        setModalSelectedRoutes([])  // pas encore de route sélectionnée
        setShowModal(true)  // Afficher la boîte de dialogue de sélection de parcours
    }

    // Clic sur le bouton "par navire"
    const handleClickBtnShip = () => {
        setModalByCaptain(false)  // La boîte de dialogue doit proposer la recherche par nom de navire
        setModalSelectedRoutes([])  // pas encore de route sélectionnée
        setShowModal(true)  // Afficher la boîte de dialogue de sélection de parcours
    }

    // Fermeture de la boîte de dialogue
    const onHideModal = (cancel=true) => {
        // console.log("cancel: "+cancel)
        setShowModal(false) // masque la boîte de dialogue modale
        if(!cancel) {   // Une donnée a été choisie / validée dans la boîte de dialogue
            // Mémoriser les choix dans le composant parent :
            setRoutesSelected(modalByCaptain, modalSelectedRoutes)
        } else {    // Réinitialiser l'état de la modale
            setModalSelectedRoutes([])
        }
    }


    
    return (<>
        <Form as={Row} className={"px-2"} >
            <Container className={""} >
                <Form.Group as={Row} className={"pb-1"} controlId={"formLabelSearchRoutes"} >
                    <Form.Label column >
                        <Trans i18nKey="controlsViz.routesSearch">Search for routes</Trans>&nbsp;:
                    </Form.Label>
                </Form.Group>
                <Row>
                    <Form.Label column xs={"auto"} className={"pt-0 pb-1"} >
                        <Button id={"btn-search-byCaptain"}
                            variant={"outline-secondary"}
                            className={""} 
                            title={t("controlsViz.search_by_captain", "by captain")} 
                            onClick={handleClickBtnCaptain}
                        >
                            { !byCaptain && <FontAwesomeIcon icon={solid('person')} size="lg" fixedWidth transform="grow-5" /> }
                            { byCaptain && <span className="fa-layers fa-fw fa-lg">
                                <FontAwesomeIcon icon={solid('person')} size="lg" transform="left-6" />
                                <FontAwesomeIcon icon={solid('check')} transform="shrink-5 right-8" />
                            </span> }
                        </Button>
                    </Form.Label>
                    <Col sm={5} className={"pb-1"} >
                        <Form.Control id={"input-captainToDisplay1"}
                            type="text" 
                            className={(highlightRouteNb == 1 ? "bg-primary bg-opacity-10" : "bg-transparent")+(differentiateColors ? " text-primary" : " text-secondary")} 
                            value={captainToDisplay1} 
                            readOnly 
                            onClick={handleClickBtnCaptain} 
                        />
                    </Col>
                    { (captainToDisplay2 != "") && <Col sm={5} className={"pb-1"} >
                        <Form.Control id={"input-captainToDisplay2"}
                            type="text" 
                            className={(highlightRouteNb == 2 ? "bg-success bg-opacity-10" : "bg-transparent")+(differentiateColors ? " text-success" : " text-secondary")} 
                            value={captainToDisplay2} 
                            readOnly 
                            onClick={handleClickBtnCaptain} 
                        />
                    </Col>}
                    <Col  xs={"auto"} className={"pb-1"} >
                        &nbsp;
                    </Col>
                </Row>
                <Row>
                    <Form.Label column xs={"auto"} className={"pt-0 pb-1"} >
                        <Button  id={"btn-search-byShip"}
                            variant={"outline-secondary"}
                            className={""} 
                            title={t("controlsViz.search_by_ship", "by ship")}  
                            onClick={handleClickBtnShip}
                        >
                            { byCaptain && <FontAwesomeIcon icon={solid('ship')} size="lg" fixedWidth transform="grow-3" /> }
                            { !byCaptain && <span className="fa-layers fa-fw fa-lg">
                                <FontAwesomeIcon icon={solid('ship')} transform="left-6" />
                                <FontAwesomeIcon icon={solid('check')} transform="shrink-5 right-10" />
                            </span> }
                        </Button>
                    </Form.Label>
                    <Col sm={5} className={"pb-1"} >
                        <Form.Control id={"input-shipToDisplay1"}
                            type="text" 
                            className={(highlightRouteNb == 1 ? "bg-primary bg-opacity-10" : "bg-transparent")+(differentiateColors ? " text-primary" : " text-secondary")} 
                            value={shipToDisplay1} 
                            readOnly 
                            onClick={handleClickBtnShip} 
                        />
                    </Col>
                    { (shipToDisplay2 != "") && <Col sm={5} className={"pb-1"} >
                        <Form.Control id={"input-shipToDisplay2"}
                            type="text" 
                            className={(highlightRouteNb == 2 ? "bg-success bg-opacity-10" : "bg-transparent")+(differentiateColors ? " text-success" : " text-secondary")} 
                            value={shipToDisplay2} 
                            readOnly 
                            onClick={handleClickBtnShip} 
                        />
                    </Col>}
                    <Col  xs={"auto"} className={"pb-1"} >
                        {urlButton}
                    </Col>
                </Row>
            </Container>
        </Form>

        <Modal
            size="xl" 
            scrollable={false} 
            show={showModal}
            onHide={() => {onHideModal(true)}}
            aria-labelledby="modal-dialog-route-search"
        >
            <Modal.Header closeButton closeLabel={t("controlsViz.btn_cancel", "Cancel")} >
                <Modal.Title id="modal-dialog-route-search" >
                    {t('controlsViz.modal_title', "Search for routes {{ bySearchElement }}", {
                        bySearchElement: (modalByCaptain ? t("controlsViz.search_by_captain", "by captain") : t("controlsViz.search_by_ship", "by ship") ),
                    })}
                </Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <FormSearchRoutes 
                    byCaptain={modalByCaptain} 
                    dataList={listCaptainsShips}
                    setSelectedRoutes={setModalSelectedRoutes} 
                />
            </Modal.Body>
            <Modal.Footer>
                <span>{t("controlsViz.nb_routes_selected", "{{count}} route selected", {count: modalSelectedRoutes.length})}</span>&nbsp;
                { ((modalSelectedRoutes.length == 1) || (modalSelectedRoutes.length == 2)) && <Button variant="primary" onClick={() => {onHideModal(false)}}>
                    <Trans i18nKey="controlsViz.btn_show">Show</Trans>
                </Button> }
                <Button variant="secondary" onClick={() => {onHideModal(true)}}>
                    <Trans i18nKey="controlsViz.btn_cancel">Cancel</Trans>
                </Button>
            </Modal.Footer>
        </Modal>
    </>)
}