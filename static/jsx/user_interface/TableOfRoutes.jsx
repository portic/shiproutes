// TableOfRoutes.jsx
/**
 * Table des parcours trouvés lors de la recherche par capitaine / navire
 * Utilisé dans : FormSearchRoutes.jsx
 * 
 * translate prefix : formSearchRoutes
 */

import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Row, Col, Form, Button, Modal } from 'react-bootstrap';
import React, { useState, useEffect, useMemo } from 'react';
import { Trans, useTranslation } from 'react-i18next';
import '../../css/shiproutes.css'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { solid, regular, brands } from '@fortawesome/fontawesome-svg-core/import.macro' // <-- import styles to be used
import { useTable, useFilters, useSortBy, usePagination, useRowSelect } from 'react-table'
import Select from 'react-select'
import { SEARCH_BY_NAME, SEARCH_BY_NAME_ALT, SEARCH_BY_ID } from './FormSearchRoutes'


// Composant fonctionnel nécessaire pour la sélection de ligne par case à cocher avec React-Table (non-exporté, utilisation locale au fichier)
const IndeterminateCheckbox = React.forwardRef(
    ({ indeterminate, ...rest }, ref) => {
      const defaultRef = React.useRef()
      const resolvedRef = ref || defaultRef
  
      React.useEffect(() => {
        resolvedRef.current.indeterminate = indeterminate
      }, [resolvedRef, indeterminate])
  
      return (
        <>
          <input type="checkbox" ref={resolvedRef} {...rest} />
        </>
      )
    }
  )


// Formulaire d'affichage en tableau / recherche de parcours
export const TableOfRoutes = ({selection, selectionType, byCaptain, dataList, setSelectedRoutes}) => {
    const { t, i18n } = useTranslation();
    const lang=i18n.resolvedLanguage;   // langue actuelle (détectée ou sélectionnée)

    // * allColumns : indique si on affiche toutes les données, ou seulement le nom des capitaines / navires
    const [allColumns, setAllColumns] = useState(false)
    // * isFiltering : indique si les champs de filtrage sont affichés / en action
    const [isFiltering, setIsFiltering] = useState(false)
    
    // Prépare la liste des colonnes à afficher dans la table
    const prepareColumns = () => {
        const columns=new Array()
        const captainColumns=new Array()
        const shipColumns=new Array()
        const afficherDetailsComplementaires=false  // ex.: lors d'une recherche par navire, afficher les colonnes supplémentaire du capitaine associé

        // Colonnes concernant le capitaine :
        if(allColumns) {
            captainColumns.push({ Header: "Id", accessor: "captainId" })
        }
        captainColumns.push({ Header: t("formSearchRoutes.name", "Name"), accessor: "captainName" })
        if(allColumns && (byCaptain || afficherDetailsComplementaires) ) {
            captainColumns.push({ Header: t("formSearchRoutes.citizenship", "Citizenship"), accessor: "captainCitizenship" })
            captainColumns.push({ Header: t("formSearchRoutes.birthplace", "Birthplace"), accessor: "captainBirthplace" })
        }
        // Colonnes concernant son navire :
        if(allColumns) {
            shipColumns.push({ Header: "Id", accessor: "shipId" })
        }
        shipColumns.push({ Header: t("formSearchRoutes.name", "Name"), accessor: "shipName" })
        if(allColumns && (!byCaptain || afficherDetailsComplementaires) ) {
            shipColumns.push({ Header: t("formSearchRoutes.flag", "Flag"), accessor: "shipFlag" })
            shipColumns.push({ Header: t("formSearchRoutes.homeport", "Homeport"), accessor: "shipHomeport" })
            shipColumns.push({ Header: t("formSearchRoutes.tonnage_class", "Tonnage class"), accessor: "shipTonnageClass" })
            shipColumns.push({ Header: t("formSearchRoutes.class", "Class"), accessor: "shipClass" })
        }
        // Insertion dans l'ordre de la recherche (par capitaine ou par navire) :
        if(byCaptain) { // par capitaine
            columns.push({Header: t("formSearchRoutes.captain", "Captain"), columns: captainColumns})
            columns.push({Header: t("formSearchRoutes.ship", "Ship"), columns: shipColumns})
        } else {    // par navire
            columns.push({Header: t("formSearchRoutes.ship", "Ship"), columns: shipColumns})
            columns.push({Header: t("formSearchRoutes.captain", "Captain"), columns: captainColumns})
        }
        // Ajout des propriétés du parcours
        columns.push( {
            Header: t("formSearchRoutes.route", "Route"),
            columns: [
                {Header: t("formSearchRoutes.min_date", "Beginning"), accessor: "mindate"},
                {Header: t("formSearchRoutes.max_date", "End"), accessor: "maxdate"}
            ]
        })
        return columns
    }

    // Met les données à afficher au format de React Table (tableau d'objets représentant les lignes de données)
    const prepareData = () => {
        const result=new Array()
        
        // Construire la liste des parcours :
        dataList.buildRoutesFound(byCaptain, selection, selectionType)
        // Extraire les objets routeData de chaque RouteInfo du tableau routesFound[] :
        dataList.routesFound.forEach(route => {
            result.push(route.routeData)
        })
        return result
    }

    // Filtrage : change le filtre de la colonne avec la valeur de l'option sélectionnée dans la liste déroulante
    const setColumnFilter = (option, nomColonne) => {
        // Cherche l'objet Column de React-Table dont on passe le nom (nom de la colonne de données)
        const cols=headerGroups[1].headers // tableau des objets column (2ème ligne du tableau)
        const column=cols.find((col) => col.id == nomColonne)   // colonne du nom demandé
        // Change le filtre de la colonne avec la valeur de l'option :
        const newValue=option ? option.value : null
        column.setFilter(newValue)
    }

    // Désactiver le filtrage (RAZ des filtres)
    const clearFilters = () => {
        setAllFilters(filtersDefaultInitialValue);  // vide les filtres de colonnes
    }

    // Quand la sélection principale change : désactiver le filtrage
    useEffect(() => {
        // Désactiver le filtrage :
        setIsFiltering(false)   // Plus de filtrage dans l'interface utilisateur
        clearFilters()  // effacer les filtres React-Table
    }, [selection]); // Effet exécuté initialement + ré-exécuté seulement si selection change

    // Clic sur le bouton d'affichage de toutes les colonnes
    const handleClickBtnDetails = () => {
        setAllColumns(!allColumns)  // toggle
    }

    // Clic sur le bouton de filtrage des données
    const handleClickBtnFilter = () => {
        if(isFiltering) {  // valeur précédente = true => On vient de désactiver le filtrage
            clearFilters()  // Effacer les filtres
        }
        // mémo. nouvelle valeur de l'état :
        setIsFiltering(!isFiltering)  // toggle
    }

    // On a choisi, pour le filtrage, un capitaine (ou navire) dans la liste déroulante
    const onFilterComplSelected = (option) => {
        setColumnFilter(option, byCaptain ? "shipName" : "captainName")   // filtrage de la colonne avec l'option choisie
    }

    // On a choisi, pour le filtrage, un Id de capitaine (ou navire) dans la liste déroulante
    const onFilterIdSelected = (option) => {
        setColumnFilter(option, byCaptain ? "captainId" : "shipId")   // filtrage de la colonne avec l'option choisie
    }

    // On a choisi, pour le filtrage, une valeur de "captainCitizenship"
    const onFilterCitizenshipSelected = (option) => {
        setColumnFilter(option, "captainCitizenship")   // filtrage de la colonne avec l'option choisie
    }

    // On a choisi, pour le filtrage, une valeur de "captainBirthplace"
    const onFilterBirthplaceSelected = (option) => {
        setColumnFilter(option, "captainBirthplace")   // filtrage de la colonne avec l'option choisie
    }

    // On a choisi, pour le filtrage, une valeur de "shipFlag"
    const onFilterFlagSelected = (option) => {
        setColumnFilter(option, "shipFlag")   // filtrage de la colonne avec l'option choisie
    }

    // On a choisi, pour le filtrage, une valeur de "shipHomeport"
    const onFilterHomeportSelected = (option) => {
        setColumnFilter(option, "shipHomeport")   // filtrage de la colonne avec l'option choisie
    }

    // On a choisi, pour le filtrage, une valeur de "shipTonnageClass"
    const onFilterTonnageClassSelected = (option) => {
        setColumnFilter(option, "shipTonnageClass")   // filtrage de la colonne avec l'option choisie
    }

    // On a choisi, pour le filtrage, une valeur de "shipClass"
    const onFilterClassSelected = (option) => {
        setColumnFilter(option, "shipClass")   // filtrage de la colonne avec l'option choisie
    }

    // Préparer les données pour React Table :
    const columns=useMemo(() => prepareColumns(), [byCaptain, allColumns])
    const data=useMemo(() => prepareData(), [selection, selectionType, byCaptain])

    // Intialement, par défaut, les filtres de colonnes sont vides (value: '')
    const filtersDefaultInitialValue = useMemo(() => [ 
        { id:'shipName', value:''},
        { id:'captainName', value:''},
    ], []);

    // Initialement, par défaut, tri ascendant par les noms de navire ou capitaine recherchés, puis par élément complémentaire (ex.: les navires d'un capitaine)
    const sortByDefaultInitialValue = useMemo(() => {
        var result=[]

        if(byCaptain) { // trier par nom de capitaine, puis de navire
            result=[
                { id:'captainName', desc: false}, 
                { id:'shipName', desc: false}
            ]
        } else {    // trier par nom de navire, puis de capitaine
            result=[
                { id:'shipName', desc: false}, 
                { id:'captainName', desc: false}
            ]
        }
        return result
    }, []);

    // Chargement valeur initiale de l'état React-table : initialState. Valeur par défaut
    const initialState= { 
        pageIndex: 0, 
        pageSize: 5,
        filters: filtersDefaultInitialValue,
        // globalFilter: globalFilterDefaultInitialValue,
        sortBy: sortByDefaultInitialValue, 
        selectedRowIds: ((data.length == 1) ? { 0: true } : {}) // Si une seule ligne de données, la sélectionner automatiquement (la première : 0)
    }
    
    // Filter = fonction obligatoire d'affichage du champ de filtrage de chaque entête de colonne : n'affiche rien (rendu pas fait dans l'entête !)
    const defaultColumn = React.useMemo(
        () => ({
          Filter: () => <></>,
        }),
        []
      )

    // Table instance :
    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        rows,
        prepareRow,
        state,
        page,
        nextPage,
        previousPage,
        canPreviousPage,
        canNextPage,
        pageOptions,
        gotoPage,
        pageCount,
        setPageSize,
        setAllFilters,
        selectedFlatRows
    } = useTable({
        columns,
        data,
        defaultColumn,
        initialState: initialState
    },
    useFilters,
    useSortBy,
    usePagination,
    useRowSelect,
    hooks => {
        hooks.visibleColumns.push(columns => [
            // Let's make a column for selection (checkboxes)
            {
                id: 'selection',
                // The header can use the table's getToggleAllRowsSelectedProps method
                // to render a checkbox
                Header: ({ getToggleAllPageRowsSelectedProps }) => (<></>
                ),
                // The cell can use the individual row's getToggleRowSelectedProps method
                // to the render a checkbox
                Cell: ({ row }) => (
                    <div>
                        <IndeterminateCheckbox {...row.getToggleRowSelectedProps()} />
                    </div>
                ),
            },
            ...columns,
        ])
    })

    // Lecture de l'état React-Table :
    const { pageSize, pageIndex, sortBy, filters, hiddenColumns, globalFilter, selectedRowIds } = state;

    // Informer le composant parent de la sélection de routes :
    useEffect(() => {
        const arrayOfRouteData=selectedFlatRows.map(d => d.original)    // Tableau d'objets routeData (lignes de données)
        setSelectedRoutes(arrayOfRouteData) // mémoriser la sélection dans le composant parent
    }, [selectedFlatRows.length]); // Effet exécuté initialement + ré-exécuté seulement si les routes sélectionnées changent
    

    // Affichage :
    return (<>
        {/* Etat, et boutons de commande */}
        <div className={"py-2"}>
            <span>{t("formSearchRoutes.nb_route_found", "{{count}} route found", {count: rows.length})}</span>&nbsp;&nbsp;
            <Button 
                variant={"outline-secondary"}
                className={""} 
                onClick={handleClickBtnDetails}
            >
                {(allColumns ? t("formSearchRoutes.show_less_details", "Less details") : t("formSearchRoutes.show_more_details", "More details") )}
            </Button>&nbsp;
            { ((rows.length > 1) || ((rows.length <= 1) && isFiltering)) && <Button 
                variant={"outline-secondary"}
                className={""} 
                onClick={handleClickBtnFilter}
            >
                {(isFiltering ? t("formSearchRoutes.filter_filtering", "Show all") : t("formSearchRoutes.filter_not_filtering", "Filter") )}
            </Button> }
        </div>

        {/* Contrôles de filtrage */}
        { isFiltering && <div className={"py-2"} >
            <div className={"mb-2"}>
                <Trans i18nKey="formSearchRoutes.filter_principle">Filter results by : </Trans>
            </div>
            <Form className={""} >
                <Form.Group as={Row} className={"pb-1"} controlId={"formFilterByComplName"} >
                    <Form.Label column sm={3} >
                        {byCaptain ? t("formSearchRoutes.ship", "Ship") : t("formSearchRoutes.captain", "Captain")}
                    </Form.Label>
                    <Col sm={9}>
                        <Select
                            onChange={onFilterComplSelected}
                            options={dataList.filterComplNames} 
                            isClearable={true}
                        />
                    </Col>
                </Form.Group> 
                { allColumns && (dataList.filterId.length > 0) && <Form.Group as={Row} className={"pb-1"} controlId={"formFilterById"} >
                    <Form.Label column sm={3} >
                        {(lang == "fr" ? "Id de " : "")}{byCaptain ? t("formSearchRoutes.captain", "Captain") : t("formSearchRoutes.ship", "Ship")}{(lang == "fr" ? "" : " Id")}
                    </Form.Label>
                    <Col sm={9}>
                        <Select
                            onChange={onFilterIdSelected}
                            options={dataList.filterId} 
                            isClearable={true}
                        />
                    </Col>
                </Form.Group> }
                { byCaptain && allColumns && <>
                    { (dataList.filterCitizenship.length > 0) && <Form.Group as={Row} className={"pb-1"} controlId={"formFilterByCaptainCitizenship"} >
                        <Form.Label column sm={3} >
                            <Trans i18nKey="formSearchRoutes.citizenship">Citizenship</Trans>
                        </Form.Label>
                        <Col sm={9}>
                            <Select
                                onChange={onFilterCitizenshipSelected}
                                options={dataList.filterCitizenship} 
                                isClearable={true}
                            />
                        </Col>
                    </Form.Group> }
                    { (dataList.filterBirthplace.length > 0) && <Form.Group as={Row} className={"pb-1"} controlId={"formFilterByCaptainBirthplace"} >
                        <Form.Label column sm={3} >
                            <Trans i18nKey="formSearchRoutes.birthplace">Birthplace</Trans>
                        </Form.Label>
                        <Col sm={9}>
                            <Select
                                onChange={onFilterBirthplaceSelected}
                                options={dataList.filterBirthplace} 
                                isClearable={true}
                            />
                        </Col>
                    </Form.Group> }
                </> }
                { !byCaptain && allColumns && <>
                    { (dataList.filterFlag.length > 0) && <Form.Group as={Row} className={"pb-1"} controlId={"formFilterByShipFlag"} >
                        <Form.Label column sm={3} >
                            <Trans i18nKey="formSearchRoutes.flag">Flag</Trans>
                        </Form.Label>
                        <Col sm={9}>
                            <Select
                                onChange={onFilterFlagSelected}
                                options={dataList.filterFlag} 
                                isClearable={true}
                            />
                        </Col>
                    </Form.Group> }
                    { (dataList.filterHomeport.length > 0) && <Form.Group as={Row} className={"pb-1"} controlId={"formFilterByShipHomeport"} >
                        <Form.Label column sm={3} >
                            <Trans i18nKey="formSearchRoutes.homeport">Homeport</Trans>
                        </Form.Label>
                        <Col sm={9}>
                            <Select
                                onChange={onFilterHomeportSelected}
                                options={dataList.filterHomeport} 
                                isClearable={true}
                            />
                        </Col>
                    </Form.Group> }
                    { (dataList.filterTonnageClass.length > 0) && <Form.Group as={Row} className={"pb-1"} controlId={"formFilterByShipTonnageClass"} >
                        <Form.Label column sm={3} >
                            <Trans i18nKey="formSearchRoutes.tonnage_class">Tonnage class</Trans>
                        </Form.Label>
                        <Col sm={9}>
                            <Select
                                onChange={onFilterTonnageClassSelected}
                                options={dataList.filterTonnageClass} 
                                isClearable={true}
                            />
                        </Col>
                    </Form.Group> }
                    { (dataList.filterClass.length > 0) && <Form.Group as={Row} className={"pb-1"} controlId={"formFilterByShipClass"} >
                        <Form.Label column sm={3} >
                            <Trans i18nKey="formSearchRoutes.class">Class</Trans>
                        </Form.Label>
                        <Col sm={9}>
                            <Select
                                onChange={onFilterClassSelected}
                                options={dataList.filterClass} 
                                isClearable={true}
                            />
                        </Col>
                    </Form.Group> }
                </> }
                
                
            </Form>
        </div>}

        {/* Affichage des parcours (React-Table) */}
        <div className={"py-2"}>
            <span>{t("formSearchRoutes.check_to_select", "Check a route's checkbox to select it : ")}</span>
        </div>
        <div className={"table-responsive py-2"}>
            <table className={"table table-light table-striped table-sm"} {...getTableProps()}>
                <thead>
                    {headerGroups.map(headerGroup => (
                        <tr {...headerGroup.getHeaderGroupProps()}>
                            {headerGroup.headers.map(column => (
                                <th {...column.getHeaderProps(column.getSortByToggleProps())} 
                                    title={column.depth == 1 ? t("formSearchRoutes.help_sort_column", "Click on the header to toggle the sort order of the column (ascending, descending, none). Shift + click = multi-column sorting") : ""}>
                                    <div className="d-inline-flex">
                                        {column.render('Header')}
                                        {column.isSorted
                                             ? column.isSortedDesc
                                                 ? <>&nbsp;<FontAwesomeIcon icon={solid('caret-down')} size="lg" title={t("formSearchRoutes.sort_order_desc", "Descending sorting")} /></>
                                                 : <>&nbsp;<FontAwesomeIcon icon={solid('caret-up')} size="lg" title={t("formSearchRoutes.sort_order_asc", "Ascending sorting")} /></>
                                             : ''}
                                    </div>
                                    <div>{column.canFilter ? column.render('Filter') : null}</div>
                                </th>
                            ))}
                        </tr>
                    ))}
                </thead>
                <tbody {...getTableBodyProps()}>
                    {page.map(row => {
                        prepareRow(row)
                        return (
                            <tr {...row.getRowProps()}>
                                {row.cells.map(cell => {
                                    // console.log(cell.column);
                                    // Préparer le contenu de la cellule :
                                    var contenu="";
                                    switch(cell.column.id) {
                                        case 'captainName' :  // Rajouter les orthographes différentes au survol
                                            const captainId=row.original.captainId
                                            if(captainId == "") {   // Pas de capitaine
                                                contenu=cell.render('Cell')
                                            } else {    // Capitaine connu
                                                const captain=dataList.captainsList.find(captain => captain.id == captainId)
                                                const captainVariantes=captain.nameVariantes
                                                contenu=<span title={captainVariantes} >{cell.render('Cell')}</span>;
                                            }
                                            break;
                                        case 'shipName' :   // Rajouter les orthographes différentes au survol
                                            const shipId=row.original.shipId
                                            if(shipId == "") {   // Pas de navire
                                                contenu=cell.render('Cell')
                                            } else {    // Navire connu
                                                const ship=dataList.shipsList.find(ship => ship.id == shipId)
                                                const shipVariantes=ship.nameVariantes
                                                contenu=<span title={shipVariantes} >{cell.render('Cell')}</span>;
                                            }
                                            break;
                                        default:
                                            contenu=cell.render('Cell');
                                    }

                                    const result=<td {...cell.getCellProps()}>{contenu}</td>
                                    return result;
                                })}
                            </tr>
                        )
                    })}
                </tbody>
            </table>
        </div>

        {/* Contrôles pour la pagination */}
        <div className="d-flex align-items-center justify-content-around py-2">
            <div>
                <button className="btn btn-sm btn-light" onClick={() => gotoPage(0)} disabled={!canPreviousPage}>
                    <FontAwesomeIcon icon={solid('fast-backward')} size="lg" fixedWidth title={t("formSearchRoutes.page_first", "First page")} />
                </button>{' '}
            </div>
            <div>
                <button className="btn btn-sm btn-light" onClick={() => previousPage()} disabled={!canPreviousPage}>
                    <FontAwesomeIcon icon={solid('backward')} size="lg" fixedWidth title={t("formSearchRoutes.page_prec", "Former page")} />
                </button>{' '}
            </div>
            <div>
                <span>
                    <strong>{t("formSearchRoutes.page_current", "Page {{ pageIndex }} of {{ pagesLength }}", {
                        pageIndex: (pageOptions.length == 0) ? 0 : pageIndex + 1,
                        pagesLength: pageOptions.length
                    })}</strong>{' '}
                </span>
            </div>
            <div>
                <button className="btn btn-sm btn-light" onClick={() => nextPage()} disabled={!canNextPage}>
                    <FontAwesomeIcon icon={solid('forward')} size="lg" fixedWidth title={t("formSearchRoutes.page_next", "Next page")} />
                </button>{' '}
            </div>
            <div>
                <button className="btn btn-sm btn-light" onClick={() => gotoPage(pageCount - 1)} disabled={!canNextPage}>
                    <FontAwesomeIcon icon={solid('fast-forward')} size="lg" fixedWidth title={t("formSearchRoutes.page_last", "Last page")} />
                </button>{' '}
            </div>
            <div className="d-inline-flex align-items-center">
                <label className="form-label mb-0" htmlFor="pageNumberInput">{t("formSearchRoutes.page_goto", "Go to page")}&nbsp;</label>
                <input className="form-control" type="number" id="pageNumberInput" defaultValue={pageIndex+1} onChange={e => {
                    const pageNumber = e.target.value ? Number(e.target.value) - 1 : 0
                    gotoPage(pageNumber)
                }} style={{ width: '75px' }} />{' '}
            </div>
            <div>
                <select className="custom-select" value={pageSize} onChange={e => setPageSize(Number(e.target.value))}>
                    {[2, 5, 10, 20, 50, 100].map(pageSize => (
                        <option key={pageSize} value={pageSize}>{t("formSearchRoutes.page_lines", "{{ pageSize }} lines per page", {
                            pageSize: pageSize
                        })}</option>
                    ))}
                </select>
            </div>
        </div>

    </>)
}