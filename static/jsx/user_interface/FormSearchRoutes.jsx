// FormSearchRoutes.jsx
/**
 * Formulaire de recherche de parcours, par capitaine ou par navire
 * Affiché dans : RowControlsViz.jsx  (dans le corps de la boîte de dialogue modale)
 * 
 * Composant de liste de choix avec champ de recherche : react-select  https://www.npmjs.com/package/react-select  https://react-select.com/home 
 * 
 * translate prefix : formSearchRoutes
 */

import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Row, Col, Form, ButtonGroup, ToggleButton } from 'react-bootstrap';
import React, { useState, useEffect } from 'react';
import { Trans, useTranslation } from 'react-i18next';
import '../../css/shiproutes.css'
import Select from 'react-select'
import { TableOfRoutes } from './TableOfRoutes'

// Façons de choisir un parcours :
export const SEARCH_BY_NAME=0
export const SEARCH_BY_NAME_ALT=1
export const SEARCH_BY_ID=2
// valeurs aussi utilisées dans ListCaptainsShips.jsx (buildRoutesFound())

export const FormSearchRoutes = ({byCaptain, dataList, setSelectedRoutes}) => {
    const { t } = useTranslation();

    // * selectedCaptainOrShip : nom, ou id sélectionné (type selon : selectionType ci-dessous)
    const [selectedCaptainOrShip, setSelectedCaptainOrShip] = useState(null)
    // * selectionType : indique si on a sélectionné un nom (de capitaine ou de navire selon byCaptain), ou un id
    const [selectionType, setSelectionType] = useState(SEARCH_BY_NAME)  // Par défaut, recherche dans la liste déroulante des noms
    // * altSearchMode : recherche alternative détaillée ou par id
    const [altSearchMode, setAltSearchMode] = useState(SEARCH_BY_NAME)  // Par défaut, recherches alternatives masquées
    // * altSearchInputValue : contenu du champ de saisie pour la recherche alternative par nom
    const [altSearchInputValue, setAltSearchInputValue] = useState("")
    // * altSearchResults : tableau des résultats de recherche par nom ou variante
    const [altSearchResults, setAltSearchResults] = useState([])

    // Référence au champ de saisie pour la recherche alternative par nom :
    const altSearchInput = React.createRef()

    // à chaque frappe clavier dans le champ de saisie de recherche : MAJ état champ de saisie + recherche
    const handleAltSearchInputChange = event => {
        const newValue=event.currentTarget.value    // lecture du contenu du champ de recherche
        // console.log(newValue)
        var searchResults=[]

        // faire la recherche des mots saisis dans le champ :
        if(newValue != "") {    // champ pas vide
            searchResults=dataList.searchAllWordsFromString(newValue, byCaptain)
        }
        setAltSearchResults(searchResults)  // mémo. résultats dans le state pour affichage
        if(searchResults.length == 0) {   // Si aucun résultat de recherche
            setSelectedCaptainOrShip(null)  // annuler aussi la sélection de route
        }
        // mémo. valeur du champ de saisie dans l'état, pour MAJ affichage :
        setAltSearchInputValue(newValue)    
    }

    // quand on clique sur un résultat de recherche (SearchByNameAlt)
    const onChangeAltSearchResultSelected = event => {
        // const newValue=event.currentTarget.value    // lecture de l'option sélectionnée
        const selectedOptions=event.currentTarget.selectedOptions   // lecture du tableau des options sélectionnées
        var tabOfSelectedIds=null   // si la sélection est vide, on mémorise null par défaut, ce qui masque le tableau des parcours trouvés
        
        // Extraire le tableau des id des noms sélectionnés : 
        // console.log(selectedOptions)
        if((selectedOptions !== null) && (selectedOptions.length > 0)) {  // la sélection n'est pas vide
            tabOfSelectedIds=dataList.getIdTableFromSelectedOptions(selectedOptions)  // renvoie un tableau des id uniques sélectionnés
        }
        
        // mémoriser le(s) id(s) du (des) nom(s) choisis :
        // setSelectedCaptainOrShip(newValue)  // l'id du nom choisi dans la liste (ou null)
        setSelectedCaptainOrShip(tabOfSelectedIds)  // les ids des noms sélectionnés dans la liste (ou null)
        setSelectionType(SEARCH_BY_ID)    // on fait une recherche par id (de capitaine ou de navire)   
    }

    // On a choisi un capitaine (ou navire) dans la liste déroulante des noms :
    const onNameSelected = (option) => {
        const newValue=option ? option.value : null // Lecture du choix dans la liste (ou null si pas de sélection : croix cliquée)

        // mémoriser le choix du nom :
        setSelectedCaptainOrShip(newValue)  // le nom choisi dans la liste (ou null)
        setSelectionType(SEARCH_BY_NAME)    // on fait une recherche par nom (de capitaine ou de navire)

        // Si croix cliquée, annuler la sélection précédente :
        if(!newValue) { // L'utilisateur a cliqué sur la croix de vidage de la liste de sélection de capitaine / navire
            setSelectedRoutes([])   // Aucune routé sélectionnée !
        }
    }

    // Appui sur un des boutons de changement de mode de recherche alternative (détaillée ou par id)
    const onBtnGrpAltSearchModeChange = (e) => {
        const newMode=Number.parseInt(e.currentTarget.value) // Lecture du nom du bouton cliqué

        // Annuler la sélection précédente :
        setSelectedCaptainOrShip(null)  // Annule la valeur précédemment recherchée
        setAltSearchInputValue("")  // Aucune recherche détaillée en cours
        setAltSearchResults([])
        setSelectedRoutes([])   // Aucune route cochée
        // Appliquer le nouveau mode de recherche
        setAltSearchMode(newMode)    
    }

    // On a choisi un capitaine (ou navire) en sélectionnant son id dans la liste déroulante :
    const onIdSelected = (option) => {
        const newValue=option ? option.value : null // Lecture du choix dans la liste (ou null si pas de sélection : croix cliquée)

        // mémoriser le choix du nom :
        setSelectedCaptainOrShip([newValue])  // le nom choisi dans la liste (ou null)
        setSelectionType(SEARCH_BY_ID)    // on fait une recherche par id (de capitaine ou de navire)

        // Si croix cliquée, annuler la sélection précédente :
        if(!newValue) { // L'utilisateur a cliqué sur la croix de vidage de la liste de sélection de capitaine / navire
            setSelectedRoutes([])   // Aucune routé sélectionnée !
        }
    }

    
    return (<>
        <Form>
            <Row className={"mb-3 align-items-center"}>
                <Col xs={"auto"}>
                    {t('formSearchRoutes.invite_search_captain_ship', "Search a {{ searchElement }} in the list :", {
                        searchElement: (byCaptain ? t("formSearchRoutes.search_element_captain", "captain") : t("formSearchRoutes.search_element_ship", "ship") ),
                    })}
                </Col>
                <Col xs={"auto"}>
                    <ButtonGroup>
                        <ToggleButton id={"btnGrpAltSearchModeByName"} type={"radio"} name={"radio"} variant={"outline-secondary"}
                            value={SEARCH_BY_NAME} 
                            checked={altSearchMode == SEARCH_BY_NAME}
                            onChange={onBtnGrpAltSearchModeChange} 
                            title={t("formSearchRoutes.search_by_name_expl", "Start typing the name you are looking for")}
                        >
                            {t("formSearchRoutes.search_by_name", "Search by name")}
                        </ToggleButton>
                        <ToggleButton id={"btnGrpAltSearchModeDetailed"} type={"radio"} name={"radio"} variant={"outline-secondary"}
                            value={SEARCH_BY_NAME_ALT} 
                            checked={altSearchMode == SEARCH_BY_NAME_ALT}
                            onChange={onBtnGrpAltSearchModeChange} 
                            title={t("formSearchRoutes.alt_search_detailed_expl", "Find text fragments in the name")}
                        >
                            {t("formSearchRoutes.alt_search_detailed", "Detailed search")}
                        </ToggleButton>
                        <ToggleButton id={"btnGrpAltSearchModeById"} type={"radio"} name={"radio"} variant={"outline-secondary"}
                            value={SEARCH_BY_ID} 
                            checked={altSearchMode == SEARCH_BY_ID}
                            onChange={onBtnGrpAltSearchModeChange} 
                            title={t("formSearchRoutes.alt_search_by_id_expl", "Type the id directly if you know it")}
                        >
                            {t("formSearchRoutes.alt_search_by_id", "Search by id")}
                        </ToggleButton>
                    </ButtonGroup>
                </Col>
            </Row>
            { (altSearchMode == SEARCH_BY_NAME) && <Form.Group className="mb-3" controlId="formSearchRoutes.SearchFieldForCaptainOrShipByName">
                {/*<Form.Label>
                    {t('formSearchRoutes.invite_search_captain_ship', "Search a {{ searchElement }} in the list, by typing a name", {
                        searchElement: (byCaptain ? t("formSearchRoutes.search_element_captain", "captain") : t("formSearchRoutes.search_element_ship", "ship") ),
                    })}
                </Form.Label>*/}
                <Select
                    onChange={onNameSelected}
                    options={byCaptain ? dataList.captainNames : dataList.shipNames} 
                    isClearable={true}
                />
                { (byCaptain == true) && <Form.Text className="text-muted">
                    {t('formSearchRoutes.help_search_captain', "in the search field, type the name of the captain, a comma, then his first name, to filter the list.")}
                </Form.Text>}
            </Form.Group>}
            { (altSearchMode == SEARCH_BY_ID) && <Form.Group className="mb-3" controlId="formSearchRoutes.SearchFieldForCaptainOrShip">
                <Select
                    onChange={onIdSelected}
                    options={byCaptain ? dataList.captainIds : dataList.shipIds} 
                    isClearable={true}
                />
            </Form.Group>}
            { (altSearchMode == SEARCH_BY_NAME_ALT) && <Form.Group as={Row} className="mb-2" controlId="formSearchRoutes.SearchFieldByNameAlt">
                <Form.Label column sm="2">
                    {t('formSearchRoutes.invite_alt_search_words', "Words to search")}
                </Form.Label>
                <Col sm="10">
                    <Form.Control 
                        type="text" 
                        placeholder="" 
                        value={altSearchInputValue}
                        onChange={handleAltSearchInputChange}
                    />
                </Col>
            </Form.Group>}
            { (altSearchMode == SEARCH_BY_NAME_ALT) &&  <Form.Group className="mb-3" controlId="formSearchRoutes.SearchFieldForCaptainOrShip">
                <Form.Label>
                    {t('formSearchRoutes.nb_alt_search_results', "{{count}} results found", { count: dataList.countSearchResults(altSearchResults) })}
                </Form.Label>
                <select 
                    className={"form-select"}
                    // id={"formSearchRoutes.SearchResultsByNameAlt"} 
                    name={"formSearchRoutes.SearchResultsByNameAlt"} 
                    size={"5"} 
                    onChange={onChangeAltSearchResultSelected} 
                    multiple={true} 
                >
                    {altSearchResults.map((searchResult, indexMajoritaire) => {
                        var result=null

                        if(searchResult.results.length == 1) {    // un seul résultat
                            result=<option value={searchResult.results[0].value} key={"altSearchResult-"+indexMajoritaire}>{searchResult.results[0].label}</option>
                        } else {    // plusieurs résultats pour un même nom : faire un groupe
                            result=<optgroup key={"altSearchResult-group-"+indexMajoritaire} label={searchResult.optGroupName}>
                                {searchResult.results.map((aResult, indexMinoritaire) => (
                                    <option value={aResult.value} key={"altSearchResult-"+indexMajoritaire+"-"+indexMinoritaire}>{aResult.label}</option>
                                ))}
                            </optgroup>
                        }
                        return result
                    })}
                </select>
                { (altSearchResults.length > 0) && <Form.Text className="text-muted">
                    {t('formSearchRoutes.help_alt_search_results', "Click on a result to see the routes found")}
                </Form.Text>}
            </Form.Group>}
        </Form>
        { selectedCaptainOrShip && 
            <TableOfRoutes 
                selection={selectedCaptainOrShip} 
                selectionType={selectionType}
                byCaptain={byCaptain} 
                dataList={dataList} 
                setSelectedRoutes={setSelectedRoutes} 
            />
        }
    </>)
}
