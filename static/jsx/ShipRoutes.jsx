// ShipRoutes.jsx
/**
 * Application React de visualisation des trajets des navires :
 * - chargement des données de l'API porticapi (à l'URL : apiurl)
 * - affichage des composants d'interface utilisateur, et d'affichage de carte
 * 
 * Utilise : Bootstrap 5, React-Bootstrap 2, Axios
 * 
 * translate prefix : mainComponent (nom de la section où se trouvent les textes affichés ici, à modifier à l'intérieur des fichiers du dossier : translate/locales)
 */

import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Row, Col } from 'react-bootstrap';
import React, { useState, useEffect, useMemo } from 'react';
import { useTranslation } from 'react-i18next';
import axios from 'axios';
import { UrlParamsParser } from './UrlParamsParser'
import { MsgLoad } from './user_interface/MsgLoad'
import { MapOfFrenchPorts } from './map_of_ports/MapOfFrenchPorts'
import { Chronogram, MODE_ZOOM, MODE_PLAYER } from './chronogram/Chronogram'
import { RowNavbarViz } from './user_interface/RowNavbarViz'
import { RowControlsViz } from './user_interface/RowControlsViz'
import { ListChronoSteps } from './datamodel/ListChronoSteps'
import '../css/shiproutes.css'
import { UrlButton } from './user_interface/UrlButton';


export const ShipRoutes = ({apiurl}) => {   // déstructure les props pour avoir les variables directement dans le composant fonctionnel
    const { t, i18n } = useTranslation();
    const lang=i18n.resolvedLanguage;   // langue actuelle (détectée ou sélectionnée)

    // Valeurs initiales pour le state : Parser les paramètres contenus dans l'URL de l'application
    // (ex.: http://127.0.0.1:5000/index.html?by_captain=false&captain_id_1=00014926&... )
    // les valeurs initiales par défaut des paramètres (et le parseur) sont dans : UrlParamsParser.js
    const urlParamsParser=useMemo(() => {   // Optimisation : mise en cache, pour éviter de refaire à chaque MAJ d'une valeur du state
        const result=new UrlParamsParser(window.location.search)    // ex.: window.location.search = ?by_captain=false&captain_id_1=00014926&...
        // console.log(result.listParams) // debug : afficher les paramètres, reçus (décodés de l'URL) ou par défaut (dans le code de UrlParamsParser.js)
        return result
    }, [])  // Les valeurs de l'URL ne changent pas après le démarrage : [] = pas de mise à jour prévue !   
        
    // Dans le state global de l'application :
    // - variables pour le fonctionnement de ShipRoutes.jsx
    // - variables communes au chronogramme et à la carte, ...
    // * apiData : les données au format JSON, ou null si pas chargées
    const [apiData, setApiData] = useState(null);
    // * loadStatus : pour connaître l'état du chargement des données / le message d'erreur
    const [loadStatus, setLoadStatus] = useState({
        apiMessage: "", // message reçu pendant le chargement des données si erreur
        apiInfo: null,    // information complémentaire (debug)
        apiInfoWrap: false, // true : Ne pas afficher d'ascenseur horizontal, mais passer à la ligne (pour erreurs autres que 500)
        loading: true,  // true pendant le chargement, false quand c'est fini
        error: false // true si une erreur s'est produite pendant le chargement, false sinon
    });
    // * byCaptain : indique si on a sélectionné les parcours par capitaine (true), ou par navire (false)
    const [byCaptain, setByCaptain] = useState(urlParamsParser.listParams.by_captain)
    // Mémorisation de l'affichage, en cas du changement de langue :
    // * captainToDisplay1 : capitaine du parcours en cours d'affichage
    const [captainToDisplay1, setCaptainToDisplay1] = useState("")
    // * shipToDisplay1 : bateau du parcours en cours d'affichage
    const [shipToDisplay1, setShipToDisplay1] = useState("")
    // * captainToDisplay2 : capitaine du 2ème parcours sélectionné
    const [captainToDisplay2, setCaptainToDisplay2] = useState("")
    // * shipToDisplay2 : bateau du 2ème parcours sélectionné
    const [shipToDisplay2, setShipToDisplay2] = useState("")
    // * differentiateColors : afficher deux parcours dans des couleurs neutres si id de capitaine (ou navire) différents
    const [differentiateColors, setDifferentiateColors] = useState(false)
    // * highlightRouteNb : numéro de la route à afficher sur fond de couleur (0 : aucune, 1 ou 2 : route correspondante)
    const [highlightRouteNb, setHighlightRouteNb] = useState(0)
    // * routesSelected : tableau de routeData (cf. RouteInfo.js)
    const [routesSelected, setRoutesSelected] = useState([])    // initialement, aucun parcours à afficher n'a été choisi
    // * listSteps : liste des segments du parcours (segments de la 'route' sélectionnée), chargée dans Chronogram, et stockée ici (car partagée avec la carte)
    const [listSteps, setListSteps] = useState(null)
    // * highlightedStepIndex : N° d'objet step mis en évidence sur fond gris
    const [highlightedStepIndex, setHighlightedStepIndex] = useState(-1)
    // * graphMode : mode d'interraction avec le graphique (zoom ou player)
    const [graphMode, setGraphMode] = useState(MODE_ZOOM)
    // * filterRedSteps : ne pas afficher les étapes en rouge
    const [filterRedSteps, setFilterRedSteps] = useState(urlParamsParser.listParams.filter_red_steps)  // coché par défaut (case à cocher en-dessous du chronogramme)
    // * filterOrangeSteps : ne pas afficher les étapes en orange
    const [filterOrangeSteps, setFilterOrangeSteps] = useState(urlParamsParser.listParams.filter_orange_steps)  // décoché par défaut
    // * center : localisation du centre de la carte (lat, lng)
    const [center, setCenter] = useState({lat: 46.860191, lng: 2})
    // * zoom : niveau de zoom de la carte
    const [zoom, setZoom] = useState(5)
    // * stopPlayer : booléen qui passe à true quand on clique sur la carte, pour demander au player de s'arrêter (si en mode player, et défilement auto. en cours - play)
    const [stopPlayer, setStopPlayer] = useState(false)
    
    
    // Gérer une erreur renvoyée lors de la communication avec l'API
    const gererErreurApi = (error) => {
        // Erreur : dans le navigateur (error) ou sur le serveur (error.response)
        // error.response avec Flask/jsonify() : {data (JSON - simple chaine), status (404), statusText (NOT FOUND), headers, config, request}
        // console.log(error.response)
        var apiMsg = "";
        var apiInfo = null;
        var apiInfoWrap=false;
        if((typeof error.response) == 'undefined') {    // Pas de réponse du serveur
            apiMsg = error.toString();
            // apiInfo = error.response.data;
            apiInfo=""
            apiInfoWrap = false;
        } else {    // On a une réponse du serveur
            // Valeurs par défaut :
            apiMsg = <>{error.toString()} - {error.response.statusText}</>;
            apiInfo = error.response.data;
            apiInfoWrap=true;   // Passer à la ligne, pas d'ascenseur horizontal
            // Personnalisation des infos de déboguage :
            switch(error.response.status) {
                case 404 :  // not found
                    apiInfo = error.response.request.responseURL+'\n\n'+error.response.data;  // Ajouter l'URL demandée
                    break;
                case 405 :  // method not allowed
                    // Ajouter la méthode utilisée :
                    apiMsg = <>{error.toString()} - {error.response.config.method.toString().toUpperCase()} {error.response.statusText}</>;
                    break;
                case 500 :  // internal server error
                    // ascenseur horizontal (pas de retour à la ligne forcé pour préserver la lisibilité des messages de Pandas SQL) :
                    apiInfoWrap=false;  
                    break;
                default :
                            
            }
        }
        // Mémoriser le message d'erreur et le status du chargement :
        setLoadStatus({ apiMessage: apiMsg, apiInfo: apiInfo, apiInfoWrap: apiInfoWrap, loading: false, error: true });
    }

    // Charger les données depuis l'API porticapi
    const chargerDonnees = () => {
        var newApiData={}

        // Requêtes initiales à l'API, à enchaîner :
        const requeteCapitaines = `${apiurl}/captains/?format=json&lang=${lang}`
        const requeteNavires = `${apiurl}/ships/?format=json&lang=${lang}`
        const requeteProduits = `${apiurl}/cargo_categories/`

        // mettre le loadStatus à : chargement en cours
        setLoadStatus({ apiMessage: '', apiInfo: null, apiInfoWrap: false, loading: true, error: false })

        // Interroger d'abord l'endpoint de l'API : /captains
        axios.get(requeteCapitaines)
            .then(res => {  // Succès (1) : lecture données des capitaines
                /*
                var erreur=new Error("Erreur de test")
                erreur.response={data: "pas d'info !"}
                throw erreur
                */
                
                // stocker les données reçues :
                newApiData={ ...newApiData, captainsData: res.data }

                // Interroger ensuite l'endpoint de l'API : /ships
                return axios.get(requeteNavires)    // renvoie une nouvelle promesse (objet Promise)
            })
            .then(res => {  // Succès (2) : lecture données des navires
                // stocker les données reçues :
                newApiData={ ...newApiData, shipsData: res.data }
                
                // Interroger ensuite l'endpoint de l'API : /cargo_categories
                return axios.get(requeteProduits)    // renvoie une nouvelle promesse (objet Promise)
            })
            .then(res => {  // Succès (3) : lecture données des produits 
                // stocker les données reçues :
                newApiData={ ...newApiData, productsData: res.data }
                setApiData(newApiData)  // Stocker données finales (complètes) dans le state

                // mettre à jour le status du chargement :
                setLoadStatus({ ...loadStatus, loading: false, error: false })  // chargement terminé sans erreur
            })
            .catch(error => { 
                gererErreurApi(error) // Gestion d'erreur avec l'API
            });
    }

    // Charger les données de l'API
    useEffect(() => {
        // Requête à l'API + stockage dans le State
        chargerDonnees();   
    }, [lang]); // Effet exécuté initialement + ré-exécuté seulement si lang change (rechargement des traductions des noms des ports : toponym, ...)

    // Un choix de parcours à afficher vient d'être effectué
    const onRoutesSelected = (modalByCaptain, modalSelectedRoutes) => {
        // Mémoriser le mode de choix de la donnée :
        if(modalByCaptain != byCaptain) {
            setByCaptain(modalByCaptain)
        }
        // Mémoriser les routes sélectionnées :
        setRoutesSelected(modalSelectedRoutes)

        // Afficher la sélection :
        const nbParcoursSel=modalSelectedRoutes.length  // Nombre de parcours sélectionnés : 0, 1 ou 2
        if(nbParcoursSel > 0) { // Il y a des parcours à afficher : au moins 1
            const newDifferentiateColors=ListChronoSteps.differentiateColors(modalSelectedRoutes, modalByCaptain)   // Indique si on doit afficher les parcours avec des couleurs neutres
            setDifferentiateColors(newDifferentiateColors)  // mémo. mode d'affichage différencié si plusieurs parcours à afficher avec id différents
            // 1er parcours :
            const routeData1=modalSelectedRoutes[0]
            setCaptainToDisplay1((nbParcoursSel == 2 ? "1: " : "")+routeData1.captainName+" (id: "+routeData1.captainId+")")
            setShipToDisplay1((nbParcoursSel == 2 ? "1: " : "")+routeData1.shipName+" (id: "+routeData1.shipId+")")  // carré plein moyen \u23f9 (autres : petit=\u25a0, gros=\u2b1b) voir https://fr.wikipedia.org/wiki/Table_des_caract%C3%A8res_Unicode_(2000-2FFF)
            // 2ème parcours sélectionné :
            if(modalSelectedRoutes.length == 2) {   // On a sélectionné un 2ème parcours à afficher
                const routeData2=modalSelectedRoutes[1]
                setCaptainToDisplay2((nbParcoursSel == 2 ? "2: " : "")+routeData2.captainName+" (id: "+routeData2.captainId+")")
                setShipToDisplay2((nbParcoursSel == 2 ? "2: " : "")+routeData2.shipName+" (id: "+routeData2.shipId+")")
            } else {    // Sinon, effacer les champs correspondants
                setCaptainToDisplay2("")
                setShipToDisplay2("")
            }
        }
        
    }

    // Quand les données de l'API sont chargées, traiter les id reçus par l'URL de l'application, pour affichage (initial) immédiat
    useEffect(() => {
        if((!loadStatus.loading && !loadStatus.error) && (urlParamsParser.parametersAreValid)) {  // Chargement API terminé + Il y a au moins un parcours à afficher
            const newSelectedRoutes=urlParamsParser.prepareSelectedRoutes(apiData)  // Retrouver dans les données de l'API les infos nécessaires sur les parcours à afficher
            onRoutesSelected(urlParamsParser.listParams.by_captain, newSelectedRoutes)  // Mémoriser et afficher les parcours
        }
    }, [loadStatus]); // Effet exécuté initialement + ré-exécuté seulement si les [variables] changent

    // Changement de l'étape affichée par le player
    const changeHighlightedStep = (stepIndex) => {
        // mémo. valeur :
        setHighlightedStepIndex(stepIndex)

        // MAJ de highlightRouteNb (N° de route affichée par le player)
        if(stepIndex == -1) {   // aucune étape sélectionnée
            setHighlightRouteNb(0)
        } else {    // retrouver l'étape sélectionnée dans la liste des étapes
            if(routesSelected) {
                if(routesSelected.length <= 1) {    // Une seule route affichée
                    setHighlightRouteNb(0)
                } else {    // plusieurs routes affichées
                    if(listSteps) { // Liste des étapes chargée
                        const currentStep=listSteps.getStepByIndex(stepIndex)
                        if(currentStep) {   // étape sélectionnée trouvée
                            const routeIndex=currentStep.routeIndex
                            if(routeIndex != -1) {  // N° de parcours retrouvé (1 ou 2)
                                setHighlightRouteNb(routeIndex)
                            }
                        }
                    }
                }
            } 
        }
    }

    /**
     * Prépare le composant bouton pour copier l'URL contenant les paramètres d'affichage des parcours (s'il y en a d'affichés). 
     * C'est là que tous les paramètres passés dans l'URL sont collectés (car les variables se trouvent dans le state de ShipRoutes.jsx). 
     * Ce composant sera affiché par <RowControlsViz />
     * @returns le composant <UrlButton /> si des parcours sont affichés, un composant vide <></> sinon
     */
    const getUrlButton = () => {
        return ( (routesSelected && (routesSelected.length > 0)) ? <UrlButton
            byCaptain={byCaptain} 
            captainId1={( (routesSelected && (routesSelected.length > 0)) ? routesSelected[0].captainId : null )} 
            shipId1={( (routesSelected && (routesSelected.length > 0)) ? routesSelected[0].shipId : null )} 
            captainId2={( (routesSelected && (routesSelected.length > 1)) ? routesSelected[1].captainId : null )}
            shipId2={( (routesSelected && (routesSelected.length > 1)) ? routesSelected[1].shipId : null )} 
            filterRedSteps={filterRedSteps} 
            filterOrangeSteps={filterOrangeSteps}
        /> : <></> )
    }

    
    // Rendu du composant :

    // Chargement en cours
    if(loadStatus.loading) {
        return (<MsgLoad erreur={false} message={t('mainComponent.load_in_progress', 'Data loading ...')} />)
    }

    // Erreur de chargement
    if(loadStatus.error) {
        // Dev :
        return (<MsgLoad erreur={true} message={<>{t('mainComponent.load_error', 'Error while loading data : ')}<br />{loadStatus.apiMessage}</>} info={loadStatus.apiInfo} infoWrap={loadStatus.apiInfoWrap} />)
        // Prod :
        // return (<MsgLoad erreur={true} message={<>{t('mainComponent.load_error', 'Error while loading data : ')}<br />{loadStatus.apiMessage}</>} />)
    }

    // Données chargées sans erreur
    if(!loadStatus.loading && !loadStatus.error) {
        // console.log(apiData.productsData)    // voir ce qui a été chargé

        // Composants : barre de navigation (menu), contrôles d'interface utilisateur, et carte + chronogramme.
        // Taille : taille de la carte fixée sur le conteneur de la carte (XXvh), dans MapOfFrenchPorts.jsx
        return (
            <Container>
                <RowNavbarViz bgColorClass={"bg-orange-100"} txtClass={"txtNavBar"} />
                <RowControlsViz apiData={apiData} 
                    byCaptain={byCaptain} 
                    captainToDisplay1={captainToDisplay1} 
                    shipToDisplay1={shipToDisplay1} 
                    captainToDisplay2={captainToDisplay2} 
                    shipToDisplay2={shipToDisplay2} 
                    setRoutesSelected={onRoutesSelected} 
                    differentiateColors={differentiateColors} 
                    highlightRouteNb={highlightRouteNb} 
                    urlButton={getUrlButton()}
                />
                { (routesSelected.length > 0) && <Row className={"px-2"} >
                    <Col sm={6}>
                        <Chronogram 
                            routes={routesSelected} 
                            apiurl={apiurl} 
                            byCaptain={byCaptain} 
                            differentiateColors={differentiateColors} 
                            listSteps={listSteps} 
                            setListSteps={setListSteps} 
                            highlightedStepIndex={highlightedStepIndex} 
                            setHighlightedStepIndex={changeHighlightedStep} 
                            filterRedSteps={filterRedSteps} 
                            setFilterRedSteps={setFilterRedSteps} 
                            filterOrangeSteps={filterOrangeSteps} 
                            setFilterOrangeSteps={setFilterOrangeSteps} 
                            graphMode={graphMode} 
                            setGraphMode={setGraphMode} 
                            stopPlayer={stopPlayer} 
                            setStopPlayer={setStopPlayer}
                        />
                    </Col>

                    <Col sm={6} className={"my-2 ps-0"} >
                        <MapOfFrenchPorts 
                            productsData={apiData.productsData} 
                            nbParcours={routesSelected.length} 
                            differentiateColors={differentiateColors} 
                            tabSteps={listSteps ? listSteps.steps : null} 
                            highlightedStepIndex={highlightedStepIndex} 
                            filterRedSteps={filterRedSteps} 
                            filterOrangeSteps={filterOrangeSteps} 
                            graphMode={graphMode} 
                            center={center} changeCenter={setCenter} 
                            zoom={zoom} changeZoom={setZoom} 
                            setStopPlayer={setStopPlayer}
                        />
                    </Col>

                </Row> }
            </Container>
        )
    }
}