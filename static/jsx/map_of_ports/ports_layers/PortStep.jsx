// PortStep.jsx
/**
 * Composant d'affichage d'un port sous forme de cercle
 * Le cercle est  un point noir
 * L'étiquette avec le nom du port est gérée ailleurs (dans PortNames.jsx)
 */

import React from 'react';
import 'leaflet/dist/leaflet.css';
import 'leaflet/dist/leaflet.js'
import { Tooltip, Circle } from 'react-leaflet'
import '/static/css/tooltips-custom.css'
import { PortViewUtils } from './PortViewUtils'

export const PortStep = ({portData}) => {
    // Mise en forme du CircleMarker :
    const color_zeroConge='black'   // noir si pas de congé
    const color_Conges='#0095ff'    // bleu clair sinon
    const color_Stroke='blue'   // contour bleu foncé
    const opacity_Stroke=0.9
    const opacity_empty_marker=0.05
    const opacity_full_marker=0.8
    const width_Stroke=1
    // Cercle vide (conges_cr)
    const emptyMarkerLook={ stroke: true, color: color_Stroke, opacity: opacity_Stroke, weight: width_Stroke, fill: true, fillColor: color_Conges, fillOpacity: opacity_empty_marker }    
    // Cercle plein (conges_inputdone)
    const fullMarkerLook={...emptyMarkerLook, fillOpacity: opacity_full_marker}
    // Port sans congé (petit point noir)
    const markerNoCongeLook={color: color_zeroConge, stroke: false, fill: true, fillOpacity: 1}
    // Cercle invisible pour tooltip d'infos
    const markerInvisibleLook={ stroke: true, opacity: 0, weight: width_Stroke, fill: true, fillOpacity: 0}

    // Formatage du marqueur Circle : vide ou plein selon les données disponibles
    const computePathOptions = () => {
        var result=markerNoCongeLook;

        return result;
    }
   
    
    // Affichage du Port :
    const markerRadius=2*PortViewUtils.magnifyingFactor;
    // console.log(data.name+": r="+markerRadius+" en "+data.point)

    return (<>
        <Circle
            center={portData.point}
            pathOptions={computePathOptions()}
            radius={markerRadius} 
        >
        </Circle>
        {/* pas encore d'affichage de données au survol du port :
        <Circle
            center={data.point}
            pathOptions={markerInvisibleLook}
            radius={markerRadius}
            pane="shadowPane"
        >
            <Tooltip sticky>
                <PortCongesDataView portData={data} source={source} annee={annee} />
            </Tooltip>
        </Circle>*/}
    </>)
}