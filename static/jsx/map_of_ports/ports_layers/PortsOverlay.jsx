// PortsOverlay.jsx
/**
 * Affichage des ports des étapes du parcours
 * 
 * translate prefix : portsOverlay
 */

import React, { useEffect, useState } from 'react';
import { LayersControl, LayerGroup } from 'react-leaflet'
import { useTranslation } from 'react-i18next';
import { PortStep } from './PortStep'
import { PortNames } from './PortNames'


export const PortsOverlay = ({portsList, classNameForPortNames, checked, name, whenReady }) => {
    const { t, i18n } = useTranslation();
    const lang=i18n.resolvedLanguage;   // langue actuelle (détectée ou sélectionnée)

    // State :
    // * layerIsReady : pour déclencher une mise à jour du composant parent par whenReady()
    const [layerIsReady, setLayerIsReady]=useState(false);  // Pour attendre la fin de la création des ports

    // Signaler à l'appelant que la couche de carte est prête
    useEffect(() => {
        if(layerIsReady && whenReady) { // Une fonction est fournie par l'appelant, on peut l'exécuter :
            whenReady()
        }
    }, [layerIsReady]); // Fait initialement, et quand layerIsReady change

    // Rendu du composant :

    // Si l'overlay est visible (coché) initialement :
    const isChecked=(checked == undefined) ? false : checked;

    // Nom de l'overlay : si 'name' pas spécifié, mettre une valeur par défaut :
    const overlayName=(name == undefined) ? t('portsOverlay.overlay_name_default', "Ports") : name;

    return (<LayersControl.Overlay  checked={isChecked} name={overlayName} >
                <LayerGroup>
                    { portsList && portsList.ports.map(objPortData => (
                        <PortStep 
                            portData={objPortData} 
                            key={"portStep-"+objPortData.uhgsId} />
                    ))}
                    { portsList && 
                        <PortNames 
                            portsList={portsList} 
                            classNameForPortNames={classNameForPortNames} 
                        />
                    }
                </LayerGroup>
                {portsList && !layerIsReady && setLayerIsReady(true)}
    </LayersControl.Overlay>)
}