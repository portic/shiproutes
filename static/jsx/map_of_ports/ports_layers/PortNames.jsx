// PortNames.jsx
/**
 * Affichage de tous les noms des ports, toujours classés par ordre de CONGES décroissants (source = "all", annee = "all")
 * Ajoute un nom de classe à chaque tooltip (étiquette de nom de port), pour permettre le fonctionnement de DetectOverlappingTooltips (détection de collision)
 */

 import React, { useEffect, useState } from 'react';
import 'leaflet/dist/leaflet.css';
import 'leaflet/dist/leaflet.js'
import { Marker, Tooltip } from 'react-leaflet'
import '/static/css/tooltips-custom.css'

export const PortNames = ({portsList, classNameForPortNames}) => {

    // Rendu du composant :
    return (<>
        { portsList && portsList.ports.map(objPortData => {
            // console.log(objPortData.portName)
            return (
                <Marker 
                    position={objPortData.point} 
                    icon={L.divIcon({className: "hidden-div-icon"})} 
                    key={"portNameMarker-"+objPortData.uhgsId} 
                >
                    <Tooltip 
                        permanent 
                        key={"portNameTooltip-"+objPortData.uhgsId}
                        className={"port-conges-tooltip "+classNameForPortNames} 
                        opacity={1} 
                    >{objPortData.portName}</Tooltip>
                </Marker>
            )}
        )}
    </>);
}