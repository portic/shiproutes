// PortViewUtils.js
/**
 * Constantes et fonctions utilitaires factorisées ici
 * Pour dessiner des marqueurs cercle d'aire proportionnellee à une grandeur
 */

import { destination } from 'leaflet-geometryutil'

export class PortViewUtils {

    // Facteur d'échelle, permettant la superposition des marqueurs
    static magnifyingFactor=90;

    // Transpose linéairement une valeur d'une plage [in_min, in_max] à une autre [out_min, out_max]
    static map = (x, in_min, in_max, out_min, out_max) => {
        return (x - in_min) * (out_max - out_min) / (in_max - in_min) + out_min;
    }

    // Pour avoir un cercle d'aire proportionnelle à une valeur x :
    // Transpose x d'une plage [nbCongesMin, nbCongesMax] vers un rayon de marqueur cercle [r_min, r_max]
    // formule : rayon = (diametreMin / 2) * racine_carrée_de(valeur / valeurMin)
    //                 = rayonMin * racine_carrée_de(valeur / valeurMin)
    // voir : https://blog.adrienvh.fr/2014/04/09/calculer-le-rayon-de-cercles-proportionnels/ 
    static radiusForCircleMarker= (x, xMin, rMin) => {
        const result=rMin * Math.sqrt(x / xMin);
        return result;
    }

    /** 
     * Déterminer les coordonnées d'un carré (de côté : squareSide), centré sur un point (centerLatLng), à une distance en mètres (des côtés) de : squareSide/2
     * Idée : déterminer des points au Nord-Est et Sud-Ouest du centre, formant chacun un triangle avec le centre, de côtés en angle droit, égaux, de taille : squareSide/2
     * => les bounds du carré sont exprimées avec ces deux points (opposés, sur la diagonale).
     * Dans le package Leaflet.GeometryUtil, il y a la fonction : destination(départ, angle, distance) qui renvoie les coordonnées d'un point :
     * - à une distance donnée du départ, 
     * - avec un angle donné (compté en degrés, dans le sens des aiguilles d'une montre, à partir du zéro degrés au nord)
     * => ici la distance est l'hypothénuse d'un triangle, et les angles sont à calculer pour les deux points au Nord-Est (neAngle) et Sud-Ouest (swAngle)
     * Voir : https://ibootweb.com/questions/2618796/comment-dessiner-un-marqueur-rectangle-dans-un-depliant-donne-seulement-1-paire-lat-lon-et-tailles-a-et-b-en-metres
     * Fonction utilitaire géométrique de Leaflet : http://makinacorpus.github.io/Leaflet.GeometryUtil/global.html#destination
     */
     static squareBounds = (centerLatLng, squareSide) => {
        // Hypothénuse :
        // Pythagore : Triangle avec 2 côtés perpendiculaires de taille (squareSide / 2) => hypothénuse = sqrt(A² + B²), avec A=B=squareSide/2
        const hypotenuse = Math.sqrt(2*(squareSide/2)**2);

        // Angles :
        const cstAngle=Math.atan(1)*180/Math.PI;
        // swAngle = 90 + Math.atan(A/B)*180/Math.PI, avec A=B=squareSide/2 => A/B=1
        const swAngle = 90 + cstAngle;
        // neAngle = 0 - Math.atan(A/B)*180/Math.PI
        const neAngle = 0 - cstAngle;

        // Les deux points opposés définissant le carré (bounds) :
        const southWest = destination(centerLatLng, swAngle, hypotenuse); //returns point L.latlng
        const northEast = destination(centerLatLng, neAngle, hypotenuse);

        // Le carré souhaité :
        const bounds = L.latLngBounds(southWest, northEast);
        return bounds;
    }

}
