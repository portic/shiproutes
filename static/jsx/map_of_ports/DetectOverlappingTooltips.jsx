// DetectOverlappingTooltips.jsx
/**
 * Composant qui masque dans le DOM les Tooltips en recouvrant d'autres (Tooltip collision detection and avoidance)
 * => évite les gros paquets d'étiquettes inesthétiques
 * => seuls les premiers tooltips ajoutés subsistent : il faut ajouter à la carte les plus importants en premier.
 * Les groupes de tooltips sont repérés par leur style CSS (classe). 
 * Voir l'algorithme : https://gis.stackexchange.com/questions/382087/how-to-remove-leaflet-tooltip-label-collision
 * Attention : ce composant doit être enfant de <MapContainer> (composant React-Leaflet) pour fonctionner !
 * 
 * La detection de collision s'effectue au sein de chaque classe de tooltip (pas entre les différentes classes).
 * Elle se déclenche à chaque fois que le niveau de zoom change (événement zoomend), car les étiquettes (tooltips) ont une taille de police fixe.
 * Elle a lieu aussi : initialement, et lorsqu'une couche est cochée.
 * Utilise le hook "useMapEvents()" de React-Leaflet, qui ne marche que dans un composant enfant de MapContainer (pour l'accès à l'objet map)
 * Voir : https://stackoverflow.com/questions/65337803/react-leaflet-v3-zoom-listener
 */

import {useEffect} from "react";
import {useMapEvents} from "react-leaflet";


export const DetectOverlappingTooltips=({tooltipClassesToDeClutter}) => {
    // console.log("tooltipClassesToDeClutter: "+tooltipClassesToDeClutter);

    // Dans le State :
    // * listClassNames : liste des classes de tooltips à traiter (classe par classe)
    const listClassNames=tooltipClassesToDeClutter.split(' ');  // Séparateur de la chaîne : espace

    
    // Renvoie true si les bounding boxes se recouvrent
    const overlap = (rect1, rect2) => {
        return(!(rect1.right < rect2.left || 
                rect1.left > rect2.right || 
                rect1.bottom < rect2.top || 
                rect1.top > rect2.bottom));
      }
      
    // Masque les tooltips en recouvrant d'autres de la classe demandée
    const hideOverlappingTooltipsByClass = (tooltipClassName) => {
        var rects = []; // Tableau des "bounding boxes" (rectangles englobants) des tooltips
        var tooltips = document.getElementsByClassName(tooltipClassName);   // Tableau des tooltips de la classe demandée
        // console.log(tooltipClassName+": "+tooltips.length);

        // Initialisation : marquer les tooltips comme non-traités, et lire leur zone occupée
        for (var i = 0; i < tooltips.length; i++) {
            tooltips[i].style.visibility = '';
            rects[i] = tooltips[i].getBoundingClientRect();
        }
        // Masque les tooltips de la liste qui en recouvrent d'autres (de la liste aussi)
        for (var i = 0; i < tooltips.length; i++) {
            if (tooltips[i].style.visibility != 'hidden') {
                for (var j = i + 1; j < tooltips.length; j++) {
                    if (overlap(rects[i], rects[j])) {
                        tooltips[j].style.visibility = 'hidden';
                    } 
                }
            }
        }
    }

    // Traitement à faire dans cet objet :
    // Détection / évitement des collisions de tooltips, classe par classe
    const hideAllClassesOfOverlappingTooltips = () => {
        listClassNames.forEach((className) => { // parcours de la liste des classes de tooltips à traiter
            hideOverlappingTooltipsByClass(className);  // traitement
        });
    }

    // Debug : afficher la visibilité des tooltips des classes à traiter
    const affAllTooltipsViz = (classes) => {
        classes.split(' ').forEach((className) => { // parcours de la liste des classes de tooltips à traiter
            const tooltips = document.getElementsByClassName(className);   // Tableau des tooltips de la classe demandée
            for (var i = 0; i < tooltips.length; i++) {
                console.log(tooltips[i].innerText+" : visibility = '"+tooltips[i].style.visibility+"'");
            }
            console.log(className+" total : "+tooltips.length);
        });
    }

    // Pour faire le traitement, enregistrer des gestionnaires d'événements : 
    // ( liste des événements : https://leafletjs.com/reference.html#map-event )
    const map = useMapEvents({
        zoomend: () => {
            // console.log("zoomend");
            hideAllClassesOfOverlappingTooltips();
        },     // Zoom avant ou arrière terminé
        overlayadd: () => {
            // console.log("overlayadd");
            hideAllClassesOfOverlappingTooltips();
        },  // On vient de (re-)cocher une couche dans le LayersControl
    });
    
    // Faire le traitement une fois au départ, et sur changement des propriétés :
    useEffect(() => {
        // console.log("initialement");
        // affAllTooltipsViz("port-name-tooltip");
        hideAllClassesOfOverlappingTooltips();  // évitement collision d'étiquettes
    }, [])

    
    // Ce composant n'affiche rien (il manipule directement le DOM) :
    return null
}