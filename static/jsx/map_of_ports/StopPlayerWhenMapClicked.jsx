// StopPlayerWhenMapClicked.jsx
/**
 * Composant qui positionne un flag de demande d'arrêt du player, quand on clique sur la carte
 * Ce composant non-visuel doit être enfant de MapContainer (situé dans MapOfFrenchPorts), pour s'abonner à l'événement Leaflet : click
 * Fonctionnement :
 * - dans ShipRoutes.jsx, une variable d'état booléenne stopPlayer (et sa fonction de modification : setStopPlayer) stocke la demande d'arrêt du player
 *      => la fonction setStopPlayer(true) (passée ici en paramètre) permet de faire la demande d'arrêt
 * - dans Chronogram.jsx, stopPlayer et setStopPlayer sont passés au player : ChronoPlayer.jsx
 * - dans ChronoPlayer.jsx, on détecte le passage à 'true' de stopPlayer, et on change l'état interne (playing = false) pour satisfaire la demande
 *      => quand une demande a été traitée, setStopPlayer(false) efface la demande.
 */

import {useMapEvent} from "react-leaflet"
import { Chronogram, MODE_ZOOM, MODE_PLAYER } from '../chronogram/Chronogram'

export const StopPlayerWhenMapClicked=({graphMode, setStopPlayer}) => {

    // Pour réagir au clic sur la carte, enregistrer le gestionnaire d'événement : 
    // ( liste des événements : https://leafletjs.com/reference.html#map-event )
    const map = useMapEvent('click', () => {    // sur clic, exécuter cette fonction
        if(graphMode == MODE_PLAYER) {  // Si le chronogramme est en mode Player (et non pas Zoom)
            setStopPlayer(true) // Positionner le flag de demande d'arrêt du player
        }
    })

    // Ce composant n'affiche rien :
    return null
}

