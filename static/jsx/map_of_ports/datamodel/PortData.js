// PortData.js
/**
 * Classe de gestion des données d'un port
 * Peut être mise en liste dans un tableau
 * Contient une méthode statique de tri des ports, par ordre de congés décroissants, pour gérer correctement les collisions des étiquettes de nom
 * 
 * Les champs de données viennent de l'API /travels : port de départ ou d'arrivée, nb. de congés, ...
 */

import 'leaflet/dist/leaflet.css';
import 'leaflet/dist/leaflet.js';
import React from 'react';

export class PortData {
    // Membres de l'instance :
    uhgsId=""   // Id
    portName="" // Nom du port, dans la langue en cours
    latitude=0  // Latitude
    longitude=0 // Longitude
    nbConges=0  // nb. max. de congés entre 1787 et 1789, pour pouvoir trier les ports
    isPortOfCurrentStep=false   // true si c'est le port de départ ou d'arrivée de l'étape en cours

    // Membres de la classe :
    

    // Constructeur
    constructor(id, portName, latitude, longitude, nbConges) {
        // mémo. données passées :
        this.uhgsId=id
        this.portName=portName
        this.latitude=latitude
        this.longitude=longitude
        this.nbConges=nbConges
    }

    // Fonction de comparaison à passer à Array.sort() pour trier les ports par ordre décroissant du de nb. de congés (les plus gros ports en premier)
    static compareConges(a, b) {
        const nbCongesA=a.nbConges+(a.isPortOfCurrentStep ? 1000000 : 0)    // Avec priorité aux ports de l'étape en cours (ajout de 1 million au nb. de congés du port)
        const nbCongesB=b.nbConges+(b.isPortOfCurrentStep ? 1000000 : 0)
        return (nbCongesB - nbCongesA); // si b > a (en nombre de congés), résultat positif : b classé avant a
    }

    // Propriété point : Point Leaflet de coordonnées du port (ex.: L.latLng(50.5, 30.5))
    get point() {
        var point=null
        try {
            point=L.latLng(this.latitude, this.longitude) // création objet L.latLng de Leaflet
        }
        catch(error) {
            /*
            console.log(error)
            console.log("latitude: "+this.latitude)
            console.log("longitude: "+this.longitude)
            console.log(this)
            */
        }
        return point
    }

    
};
  