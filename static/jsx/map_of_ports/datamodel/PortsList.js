// PortsList.js
/**
 * Liste des ports du parcours, pour affichage dans PortsOverlay
 */

import { PortData } from './PortData'
import { ListChronoSteps } from '../../datamodel/ListChronoSteps'
import { ChronoStep } from '../../datamodel/ChronoStep'

export class PortsList {
    // Membres de l'instance :
    ports=new Array()   // Liste des données PortData
    
    // Membres de la classe :
    
    
    // Constructeur
    constructor(tabSteps) {
        this.construireListPorts(tabSteps)
    }

    /**
     * Construit la liste des ports du parcours, étape par étape
     * @param {Array} tabSteps Tableau des étapes du parcours
     */
    construireListPorts = (tabSteps) => {
        // Création des objets PortData pour les ports de départ / arrivée de chaque étape :
        for(const etape of tabSteps) {   // Pour chaque étape du parcours (etape de type : ChronoStep)
            var port=null
            var id=""
            var portName=""
            var latitude=0.0
            var longitude=0.0
            var nbConges=0

            // Port de départ :
            id=etape.departureUhgsId    // Lire l'Id
            port=this.ports.find(unPort => unPort.uhgsId == id) // Recherche si un port avec cet Id existe déjà
            if (port == undefined) {    // Le port n'existe pas déjà => rassembler les autres données, et le créer :
                portName=etape.departureName
                latitude=etape.departureLat
                longitude=etape.departureLng
                nbConges=etape.departureMaxConges
                if(!isNaN(latitude) && !isNaN(longitude)) { // Le port a des coordonnées non-nulles, il peut être placé sur la carte
                    port=new PortData(id, portName, latitude, longitude, nbConges)  // Création objet PortData
                    this.ports.push(port)   // Ajout à la liste des ports
                }
            }
            // Port d'arrivée :
            id=etape.destinationUhgsId    // Lire l'Id
            port=this.ports.find(unPort => unPort.uhgsId == id) // Recherche si un port avec cet Id existe déjà
            if (port == undefined) {    // Le port n'existe pas déjà => rassembler les autres données, et le créer :
                portName=etape.destinationName
                latitude=etape.destinationLat
                longitude=etape.destinationLng
                nbConges=etape.destinationMaxConges
                if(!isNaN(latitude) && !isNaN(longitude)) { // Le port a des coordonnées non-nulles, il peut être placé sur la carte
                    port=new PortData(id, portName, latitude, longitude, nbConges)  // Création objet PortData
                    this.ports.push(port)   // Ajout à la liste des ports
                }
            }
        }
        // Tri initial de la liste des ports par ordre d'importance (congés) :
        this.ports.sort(PortData.compareConges)
        // console.log(this.ports)
    }

    /**
     * Marque les ports de départ et d'arrivée de l'étape, comme étant (ou pas) ceux de l'étape en cours
     * (permet d'assurer en priorité la visibilité - ou pas - de ces deux ports sur la carte, lors de la gestion des collisions d'étiquettes de nom des ports)
     * @param {ChronoStep} etape L'étape dont on doit rechercher les ports de départ et d'arrivée
     * @param {boolean} estPortDeLetape True si on veut marquer les ports comme étant ceux de l'étape en cours, false sinon
     */
    marquerPortsDeLetape = (etape, estPortDeLetape) => {
        // Retrouver les ports de départ et d'arrivée de l'étape :
        const idPortDepart=etape.departureUhgsId
        const portDepart=this.ports.find(port => port.uhgsId == idPortDepart)
        const idPortDarrivee=etape.destinationUhgsId
        const portDarrivee=this.ports.find(port => port.uhgsId == idPortDarrivee)
        // Marquer les ports :
        if(portDepart != undefined) {
            portDepart.isPortOfCurrentStep=estPortDeLetape
        }
        if(portDarrivee != undefined) {
            portDarrivee.isPortOfCurrentStep=estPortDeLetape
        }
    }

    // Annuler la priorisation du port précédent : met le drapeau 'isPortOfCurrentStep' à false pour tous les ports de la liste
    clearFlagCurrentStepOnAllPorts = () => {
        this.ports.forEach(port => {port.isPortOfCurrentStep=false})
    }

    // Marquer les ports de l'étape comme prioritaires pour l'affichage
    updateForSelectedStep = (step) => {
        // Effacer l'attribut prioritaire de tous les ports :
        this.clearFlagCurrentStepOnAllPorts()
        // Marquer les ports de l'étape comme prioritaires :
        this.marquerPortsDeLetape(step, true)
        // Re-trier la liste des ports :
        this.ports.sort(PortData.compareConges) // Met en tête les ports de l'étape en cours
        // console.log(this.ports)
    }

}