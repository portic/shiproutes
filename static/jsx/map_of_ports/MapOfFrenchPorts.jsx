// MapOfFrenchPorts.jsx
/**
 * Carte des ports de France au XVIIIème siècle (1787-1789) :
 * - Fond de carte vectoriel (World1789Overlay) : régions du monde, avec étiquettes de noms
 * - Couche des congés (PortsCongesOverlay) : étiquette=nom, Cercle bleu d'aire proportionnelle au nombre de congés, point noir si pas de donnée
 *   + symbole indiquant la Présence d'un greffier dans un port (PortHasAClerk) : carré violet
 * - Couche des variables de complétion (PortsVariableFilledOverlay) : étiquette=nom, cercle d'aire proportionnelle au total des pointcalls du port, couleur selon pourcentage de complétion
 * Rq.: le composant PortNames d'affichage des noms des ports, est intégré aux couches : congés, et variables (une seule affichée à la fois)
 * - Couche des limites d'amirautés, affichée avec les autres couches, décochable
 * - Légende : regroupe les éléments de légende de chaque couche. Refermable pour faciliter l'utilisation sur smartphone.
 * - Gestion collision des étiquettes par groupes : ajout étiquettes dans l'ordre décroissant du nb. de congés (ports), ou de la taille (régions)
 * 
 * React-Leaflet : https://react-leaflet.js.org/
 * Leaflet : https://leafletjs.com/
 * 
 * translate prefix : portsMap
 */

import React, { useState, useEffect } from 'react';
import 'leaflet/dist/leaflet.css';
import 'leaflet/dist/leaflet.js'
import { MapContainer, LayersControl } from 'react-leaflet'
import { useTranslation } from 'react-i18next';
import { World1789Overlay } from './background_layer/World1789Overlay'
import { AdmiraltyLimitsOverlay } from './background_layer/AdmiraltyLimitsOverlay'
import '../../css/world-background.css'
import { DetectOverlappingTooltips } from './DetectOverlappingTooltips'
import { Legend } from './legend/Legend'
import { UpdateZoomCenter } from './UpdateZoomCenter'
import { StopPlayerWhenMapClicked } from './StopPlayerWhenMapClicked'
import { PortsList } from './datamodel/PortsList'
import { ListProducts } from '../datamodel/ListProducts'
import { PortsOverlay } from './ports_layers/PortsOverlay'
import { StepsOverlay } from './steps_layer/StepsOverlay'


export const MapOfFrenchPorts = ({productsData, nbParcours, differentiateColors, tabSteps, highlightedStepIndex, graphMode, center, changeCenter, zoom, changeZoom, setStopPlayer}) => {
    const { t, i18n } = useTranslation();
    const lang=i18n.resolvedLanguage;   // langue actuelle (détectée ou sélectionnée)

    // Dans le State :
    // * portsList : liste des ports du parcours
    const [portsList, setPortsList] = useState(null)
    // * productsList : liste des produits (dictionnaire, pour affichage de la cargaison au survol du parcours)
    const [productsList, setProductsList] = useState(new ListProducts(productsData, lang))
    // * allLayersReady : quand toutes les couches sont affichées
    const [allLayersReady, setAllLayersReady] = useState(false);
    // * beginDetectOverlappingTooltips : pour (commencer à) faire la détection de collision (initiale) des tooltips
    const [beginDetectOverlappingTooltips, setBeginDetectOverlappingTooltips] = useState(false);

    // Parcours sélectionné : préparer les données pour les différentes couches
    useEffect(() => {
        if(tabSteps) {    // Ne pas faire initialement, seulement quand un parcours est réellement sélectionné
            const builtPortsList=new PortsList(tabSteps)   // Créer la liste de données des ports
            setPortsList(builtPortsList)    // Mémoriser la liste dans le state
        }
    }, [tabSteps])

    // Nouvelle étape sélectionnée : prioriser l'affichage des ports de l'étape
    /*
    useEffect(() => {
        if(portsList && (highlightedStepIndex != -1)) {    // Ne pas faire initialement, seulement quand une nouvelle étape est sélectionnée
            const highlightedStep=listSteps.getStepByIndex(highlightedStepIndex)    // retrouve la nouvelle étape sélectionnée
            portsList.updateForSelectedStep(highlightedStep)   // marquer les ports de l'étape comme prioritaires pour l'affichage
            setPortsUpdates(portsUpdates+1) // Déclencher le ré-affichage des ports sur la carte (dans le nouvel ordre)
            
            setAllLayersReady(false)    // Pour refaire la détection de collision des noms de ports
            setBeginDetectOverlappingTooltips(false)
            
        }
    }, [highlightedStepIndex])
    */

    // Faire quand tout est chargé : détection collision des étiquettes
    useEffect(() => {
        if(allLayersReady) {    // Ne pas faire initialement, seulement quand la variable passe à true
            setBeginDetectOverlappingTooltips(true);
        }
    }, [allLayersReady])

    
    // Rendu du composant :
    // console.log("allLayersReady: "+allLayersReady)
    // console.log("beginDetectOverlappingTooltips: "+beginDetectOverlappingTooltips)
    
    // Afficher les données sur une carte avec React-Leaflet :
    return (
        <MapContainer 
            style={{ height : "76vh" }} 
            center={center}
            zoom={zoom}
            scrollWheelZoom={true} 
            whenCreated={(map) => {}}
        >
            <LayersControl position="topright">
                
                <World1789Overlay checked 
                    // whenReady={() => {!portsList && !allLayersReady && setAllLayersReady(true)}}
                />

                { tabSteps && <StepsOverlay checked 
                    productsList={productsList} 
                    nbParcours={nbParcours} 
                    differentiateColors={differentiateColors} 
                    tabSteps={tabSteps} 
                    highlightedStepIndex={highlightedStepIndex} 
                    graphMode={graphMode} 
                    // whenReady={() => {!allLayersReady && setAllLayersReady(true)}}
                />}
                
                { portsList && <PortsOverlay checked 
                    portsList={portsList} 
                    classNameForPortNames={"port-name-tooltip"}
                    whenReady={() => {!allLayersReady && setAllLayersReady(true)}}
                />}


            </LayersControl>

            

            { beginDetectOverlappingTooltips && 
                <DetectOverlappingTooltips tooltipClassesToDeClutter={"region-name-tooltip port-name-tooltip"} />
            }

            {/*<Legend layers={listLayers} print={print} variable={displayVariable} source={source} annee={annee} />*/}

            <UpdateZoomCenter changeCenter={changeCenter} changeZoom={changeZoom} />

            <StopPlayerWhenMapClicked graphMode={graphMode} setStopPlayer={setStopPlayer} />

        </MapContainer>
    )

}