// StepSpline.jsx
/**
 * Affichage d'une Spline avec l'extension leaflet-spline, pour afficher une étape du parcours.
 * Affichage de la direction par des flèches avec l'extension leaflet-arrowheads, et aussi https://github.com/slutske22/leaflet-arrowheads/issues/9 (lire jusqu'au bout - moveend)
 * Ce composant ajoute / retire de la carte ses éléments (effet avec nettoyage), 
 * pour un affichage rapide directement avec Leaflet (sans React-Leaflet) 
 * => génère beaucoup de warnings de React-Leaflet ! (il faut désactiver l'affichage des warnings dans la console des outils Web)
 */

import React, { useEffect, useState } from 'react';
import ReactDOM from "react-dom"
import 'leaflet/dist/leaflet.css';
import 'leaflet/dist/leaflet.js'
import { useMap } from 'react-leaflet'
import { spline } from "leaflet-spline"
import { PortViewUtils } from '../ports_layers/PortViewUtils'
import 'leaflet-arrowheads'
import { StepDataView } from './StepDataView'
import { Chronogram, MODE_ZOOM, MODE_PLAYER } from '../../chronogram/Chronogram'
import '/static/css/tooltips-custom.css'


// Affichage sur la carte d'une étape de parcours (étape sélectionnée ou non), au sein d'un LayerGroup
export const StepSpline = ({productsList, chronoStep, nbParcours, differentiateColors, layerGroupRef, isSelected, graphMode}) => {
    // console.log(chronoStep.latLngs)

    // Look de la spline :
    const opacity_selected=0.7
    const opacity_zoom_mode=0.3
    const opacity_shadow=0.05
    const width_Stroke=2.5
    // Chemin en ombre :
    const shadowLook={ 
        stroke: true, 
        // color: color_Stroke, 
        opacity: opacity_shadow, 
        weight: width_Stroke, 
        fill: false
        // fill: true, 
        // fillColor: color_Conges, 
        // fillOpacity: opacity_empty_marker 
    }    
    // Chemin sélectionné :
    const selectedLook={...shadowLook, opacity: opacity_selected}
    // En mode zoom, tous chemins visibles mais en plus pâle :
    const zoomLook={...shadowLook, opacity: opacity_zoom_mode}

    // Mise en forme du chemin, selon qu'il est sélectionné ou non
    const computePathOptions=() => {
        // Look global selon s'il est sélectionné ou pas :
        var result=(graphMode == MODE_ZOOM ? zoomLook : (isSelected ? selectedLook : shadowLook))   // Mode zoom, sinon selon sélection

        // Ajouter les caractéristiques de l'étape :
        if(isSelected || (graphMode == MODE_ZOOM)) {    // si pas sélectionné, laisser en bleu foncé continu (sur le fond de carte bleu clair)
            // couleur :
            result={...result, color: chronoStep.color }    // couleur de l'étape, reçue de l'API    
            // pointillés :
            if(chronoStep.isDashed) {   
                result={...result, dashArray: "4,6" }   // Pattern des pointillés, voir : https://developer.mozilla.org/fr/docs/Web/SVG/Attribute/stroke-dasharray 
            }
        }
        
        return result
    }

    // Look du rectangle invisible pour tooltip d'infos
    const markerInvisibleLook={ stroke: true, opacity: 0, weight: width_Stroke, fill: true, fillOpacity: 0}


    // Dans le state :
    // * splineBounds : rectangle englobant la trajectoire (pour zoom / pan)
    const [splineBounds, setSplineBounds]=useState(null)

    
    const map = useMap()    // Récupérer l'objet Map, pour appel des fonctions de leaflet-spline 

    
    // Ajout / suppression de la spline à la carte (au LayerGroup)
    useEffect(() => {
        // Config / debug :
        const debug=true    // Pour visualiser avec des marqueurs les emplacements des points de geom4326 (reçus par l'API)
        const useSpline=true    // true -> spline, false -> polyline

        const tabMarkers=new Array()    // Tableau des marqueurs ajoutés (pour pouvoir les retirer)
        var stepSpline=null // La courbe matérialisant l'étape du parcours (spline ou polyline)
        var arrowHeads=null // flèches sur la spline uniquement
        const layerGroup=layerGroupRef.current
        var boundingRectForTooltip=null // rectangle pour affichage tooltip d'infos au survol
        var infoTooltip=null

        // Look des flèches
        const arrowheadsOptions={
            frequency: '60px',  // à intervalles réguliers, quelque soit le niveau de zoom
            // offsets: { start: '15px', end: '15px'},  // Fait planter !!!
            size: '10px', // petites flèches à tous les niveaux de zoom
            opacity: opacity_selected+0.1,
            color: chronoStep.color, // reprendre la couleur de la courbe
            dashArray: ""   // pas de pointillés sur la flèche !
        }

        // Dessine les flèches de direction sur la spline uniquement
        // Voir : https://github.com/slutske22/leaflet-arrowheads/issues/9
        const drawArrowHeads = () => {
            try {
                // retirer les flèches précédentes (en cas de zoom)
                if(arrowHeads) {
                    layerGroup.removeLayer(arrowHeads)
                }

                const arrowsLatlngs = stepSpline.trace([0,0.1,0.2,0.3,0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1])  
                    // Extraire des coordonnées de points de la spline, à intervalles prédéfinis (par une suite de nombres entre 0 et 1)
                    // prendre le plus de points possibles, pour que le polyline ghost colle au mieux à la spline => les flèches seront le plus possible dessus !
                const ghost = L.polyline(arrowsLatlngs)
                    .arrowheads(arrowheadsOptions);
                layerGroup.addLayer(ghost)
                arrowHeads = ghost.getArrowheads()
                layerGroup.removeLayer(ghost)
                layerGroup.addLayer(arrowHeads)
            }
            catch(error) {
                arrowHeads=null
            }
        }

        // Construire la courbe :
        var pathOptions=computePathOptions()  // Mise en forme du chemin
        const tabLatLngs=chronoStep.latLngs
        // const tabLatLngs=chronoStep.supprimerPointsSuccessifsIdentiques()   // debug : supprimer deux éléments identiques
        if(isSelected) {
            // console.log(chronoStep.latLngs)
            // console.log(tabLatLngs)
        }
        if(useSpline) {
            // Avec Spline :
            pathOptions={...pathOptions, smoothing: 0.075 }    // placement des points de contrôle pour mettre en forme la courbe de Bezier (par défaut : 0.15)
            stepSpline=spline(tabLatLngs, pathOptions)    // Construction de la spline, à partir du tableau des points de passage, et des options de mise en forme
        } else {
            // Avec Polyline :
            pathOptions={...pathOptions, smoothFactor: 5.0, noClip: false}  // le smoothFactor élevé permet se simplifier le tracé (affichage plus rapide) quand on dézoome
            stepSpline=L.polyline(tabLatLngs, pathOptions)    // Construction de la courbe
        }
        
        // Ajouter des flèches de sens de parcours :
        if(isSelected) {    // flèches seulement sur le segment sélectionné
            if(useSpline) { // spline : utiliser méthode alternative
                drawArrowHeads()    // fonction locale de (re)dessin des flèches
                map.on("movestart", () => { if(arrowHeads) {layerGroup.removeLayer(arrowHeads)} }) // efface les flèches à l'emplacement précédent en cas de zoom
                map.on("moveend", () => {drawArrowHeads()}) // redessine les flèches en cas de zoom
            } else {    // polyline : arrowheads fonctionne directement
                stepSpline=stepSpline.arrowheads(arrowheadsOptions) // Ajout des flèches à la spline
            }
        }

        // Ajout de la spline au LayerGroup :
        layerGroup.addLayer(stepSpline)

        // Si c'est l'étape en cours
        if(isSelected) {
            // mémo. bounds pour pouvoir zoomer dessus :
            const newSplineBounds=stepSpline.getBounds()   // Lire la zone occupée par la spline
            const splineCenter=newSplineBounds.getCenter()
            setSplineBounds(newSplineBounds)    // Mémo. nouveau rectangle englobant

            // Ajouter un tooltip d'information :
            /* Pb. avec cette méthode : il faut passer la souris exactement sur le tracé (parfois délicat !)
            stepSpline.bindTooltip("Example", {sticky: true})
            infoTooltip=stepSpline.getTooltip()
            console.log(infoTooltip)
            */
            // Rectangle pour affichage du tooltip au survol de la souris :
            boundingRectForTooltip=L.rectangle(newSplineBounds, {   
                ...markerInvisibleLook, // rectangle invisible
                pane: "shadowPane",  // au 1er plan pour meilleur suivi de la souris
                className: "hidden-pointer-tooltip" // Masquer le curseur au-dessus du tooltip (pour lisibilité), car centré dessus !
            })    
            // Rendre un composant React dans un HTMLElement (<div>) :
            const container = L.DomUtil.create('div')   
                // Voir (réponse 2, solution 1) https://stackoverflow.com/questions/67460092/need-proper-way-to-render-jsx-component-inside-leaflet-popup-when-using-geojson
            ReactDOM.render(<StepDataView 
                productsList={productsList} 
                chronoStep={chronoStep} 
                nbParcours={nbParcours} 
                differentiateColors={differentiateColors} 
            />, container)
            // Créer le tooltip d'infos pour afficher le HTMLElement, puis l'associer au rectangle :
            boundingRectForTooltip.bindTooltip(container, {sticky: true, direction: "center"})   
            infoTooltip=boundingRectForTooltip.getTooltip()
            // Ajout au layerGroup (+ retrait associé dans le nettoyage de l'effet) :
            layerGroup.addLayer(boundingRectForTooltip) 
            
            // Debug : afficher un marqueur cercle à chaque point de passage de la spline
            if(debug) {
                chronoStep.latLngs.forEach((tabLatLng) =>{
                    const marker=L.circle(L.latLng(tabLatLng), // centre
                        4*PortViewUtils.magnifyingFactor, // rayon
                        {color: 'blue', stroke: false, fill: true, fillOpacity: 1} // look
                        )
                    layerGroup.addLayer(marker) // ajout du marker au LayerGroup
                    tabMarkers.push(marker) // ajout à la liste des marqueurs ajoutés (pour suppression lors du nettoyage de l'effet)                
                })
            }
        }
        
        // Nettoyage de l'effet : enlever les éléments ajoutés précédemment à la carte
        return (() => {
            layerGroup.removeLayer(stepSpline)  // retirer la spline du LayerGroup
            if(arrowHeads) {    // retirer ses flèches
                layerGroup.removeLayer(arrowHeads)
            }
            if(boundingRectForTooltip) {    // retirer le rectangle invisible pour affichage du tooltip d'infos
                layerGroup.removeLayer(boundingRectForTooltip)
            }
            if(debug) {
                tabMarkers.forEach((marker) => {
                    layerGroup.removeLayer(marker)  // retirer chaque marker du LayerGroup
                })
            }
        })    
    }, [chronoStep, isSelected, graphMode])    // Changement de look selon l'état de sélection de l'étape (et mode zoom ou pas)

    // Zoomer sur la spline
    useEffect(() => {
        if(splineBounds) {
            map.fitBounds(splineBounds, {maxZoom: 10}) // Faire un zoom dessus, sans dépasser une certaine valeur
        }
    }, [splineBounds])
    

    // Composant non-visuel (utilisation directe des fonctions Leaflet, pas de composants React à afficher)
    return null
}
