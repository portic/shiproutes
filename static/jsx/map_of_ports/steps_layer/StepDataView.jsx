// StepDataView.jsx
/**
 * Visualisation des données : affichage des infos d'une étape du parcours
 * (affiché au survol d'une étape à la souris, sur la couche des étapes)
 * 
 * Attention : ce composant est rendu par ReactDOM.render() dans StepSpline pour Leaflet !
 * 
 * translate prefix : stepDataView
 */

import 'bootstrap/dist/css/bootstrap.min.css';
import { Container, Row, Col, Table } from 'react-bootstrap';
import React from 'react';
import { useTranslation } from 'react-i18next';
import { ChronoStep } from '../../datamodel/ChronoStep'
import { ListChronoSteps } from '../../datamodel/ListChronoSteps'
import { CargoLists } from '../../datamodel/CargoLists'
import '/static/css/shiproutes.css'

export const StepDataView = ({productsList, chronoStep, nbParcours, differentiateColors}) => {
    const { t, i18n } = useTranslation();
    const lang=i18n.resolvedLanguage;   // langue actuelle (détectée ou sélectionnée)
    
    // Mise en forme du texte
    const colNoHorizMargin="px-0";
    const contentRow="pb-1 pt-2";

    // Renvoie la classe de mise en forme (couleur de texte) Bootstrap la plus proche de la couleur du segment de l'étape
    const getColorClassName = () => {
        var result="text-"

        switch(chronoStep.color) {
            // Couleurs renvoyées par l'API (apiColor et color) :
            case "red":
                result+="danger"
                break
            case "green":
                result+="success"
                break
            case "grey":
                result+="secondary"
                break
            case "orange":
                result+="warning"
                break
            
            // Couleurs pour différencier les parcours (color seulement) :
            case "lightskyblue" :
                result+="info"
                break
            case "blue" :
                result+="primary"
                break
            case "lightgreen" :
                result+="lightgreen"
                break
        }
        return result
    }

    // Pour l'affichage des dates de début et de fin de l'étape
    const getDateDebFinStr = () => {
        var result=""
        /*
        const dateDeb=chronoStep.departureDate
        const dateFin=chronoStep.destinationDate

        if((dateDeb - dateFin) == 0) {    // Dates de début et de fin de l'étape identiques
            result=chronoStep.departureDateSource
        } else {    // dates différentes
        */
            // Formattage : dateDeb - dateFin
            result=chronoStep.departureDateSource+" - "+chronoStep.destinationDateSource
            // ajouter le nb. de jours du parcours entre parenthèses :
            // const nbJours=ChronoStep.diffDatesJours(dateDeb, dateFin)
            const nbJours=chronoStep.duration
            result+=" (<="+t("stepDataView.nb_days", "{{count}} days", {count: nbJours})+")"
        // }
        return result
    }

    
    // Rendu du composant :
    // const distance=((lang == 'fr') ? Math.round(chronoStep.distance*1.852) : chronoStep.distance) // en km si fr, en milles nautiques sinon
    const distance=chronoStep.distance
    const tonnage=chronoStep.tonnage
    const captainName=chronoStep.captainName
    const shipName=chronoStep.shipName
    const shipFlag=chronoStep.shipFlag
    const shipHomeport=chronoStep.shipHomeport
    const cargoLists=chronoStep.getCargoLists(productsList)

    return (
        <Container>
            <Row className={contentRow} >
                <Col className={colNoHorizMargin} >
                    <div className={"text-center d-flex align-items-center"} >
                        { (nbParcours > 1) && <>
                            <span className={(differentiateColors ? (chronoStep.routeIndex == 1 ? "text-primary" : "text-success") : "text-secondary")+" "+(chronoStep.routeIndex == 1 ? "bg-primary" : "bg-success")+" bg-opacity-10"}>
                                &nbsp;{chronoStep.routeIndex}:&nbsp;
                            </span>&nbsp;
                        </>} 
                        <strong className="">{chronoStep.departureName}&nbsp;</strong>
                        <span className={getColorClassName()}>
                            {chronoStep.isDashed ? <span className="fs-3">&#x21E2;</span> : <span className="fs-5">&rarr;</span>}
                        </span>
                        <strong className="">&nbsp;{chronoStep.destinationName}</strong>
                    </div>
                </Col>
            </Row>
            <Row className={contentRow} >
                <Col className={colNoHorizMargin} >
                    { getDateDebFinStr() }<br />
                    {t('stepDataView.distance', "milage (nautical miles)")} :  {distance === null ? "-" : distance}
                </Col>
            </Row>
            <Row className={contentRow} >
                <Col className={colNoHorizMargin} >
                    {t('stepDataView.captain', "captain")} :  {captainName === null ? "-" : captainName}
                </Col>
            </Row>
            <Row className={contentRow} >
                <Col className={colNoHorizMargin} >
                    {t('stepDataView.ship', "ship")} :  {shipName === null ? "-" : shipName}<br />
                    {t('stepDataView.flag', "flag")} :  {shipFlag === null ? "-" : shipFlag}<br />
                    {t('stepDataView.homeport', "homeport")} :  {shipHomeport === null ? "-" : shipHomeport}<br />
                    {t('stepDataView.tonnage', "tonnage (barrels)")} :  {tonnage === null ? "-" : tonnage}<br />
                    {t('stepDataView.cargo', "cargo")} :  { cargoLists.emptyAll ? <>&nbsp;-</> : 
                        <Table striped bordered size="sm" className={"mt-2"} >
                            <thead>
                                <tr>
                                    <th className={"text-center"} >{t('stepDataView.departure', "Departure")}</th>
                                    <th className={"text-center"} >{t('stepDataView.destination', "Destination")}</th>
                                </tr>
                            </thead>
                            <tbody>
                                { cargoLists.linesOfBothLists.map((tabLineContent, lineIndex) => (
                                    <tr key={"table-line-"+lineIndex}>
                                        {tabLineContent.map((cellContent, cellIndex) => (
                                            <td key={"table-line-"+lineIndex+"-cell-"+cellIndex}>{cellContent}</td>
                                        ))}
                                    </tr>
                                ))}
                            </tbody>
                        </Table>
                    }
                </Col>
            </Row>
        </Container>
    )

}
