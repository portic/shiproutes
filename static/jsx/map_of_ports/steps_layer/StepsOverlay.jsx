// StepsOverlay.jsx
/**
 * Affichage des étapes de parcours sur la carte du monde en 1789
 * 
 * translate prefix : stepsOverlay
 */

import React, { useEffect, useState, useRef } from 'react';
import { LayersControl, LayerGroup } from 'react-leaflet'
import { useMap } from 'react-leaflet'
import { useTranslation } from 'react-i18next'
import { StepSpline } from './StepSpline'
import { Chronogram, MODE_ZOOM, MODE_PLAYER } from '../../chronogram/Chronogram'


export const StepsOverlay = ({productsList, nbParcours, differentiateColors, tabSteps, highlightedStepIndex, graphMode, checked, name, whenReady }) => {
    const { t } = useTranslation();

    const layerGroupRef = useRef(null)

    const map = useMap()    // Récupérer l'objet Map

    // State :
    // * layerIsReady : pour déclencher une mise à jour du composant parent par whenReady()
    const [layerIsReady, setLayerIsReady]=useState(false);  // Pour attendre la fin de la création des ports
    // * stepsSplines : les étapes affichables du parcours
    const [stepsSplines, setStepsSplines]=useState([]);
    // * zoomBounds : zone englobante de l'ensemble des étapes, pour le mode zoom
    const [zoomBounds, setZoomBounds]=useState(null);

    /**
     * Renvoie les étapes (chronoStep) affichables du parcours, dans un tableau []
     * (étapes pour lesquelles les ports de départ et d'arrivée ont des coordonnées (longitude, latitude) non-nulles,
     * pour lesquelles il y a des points non-identiques, ...)
     */ 
    const getDisplayableSteps = () => {
        const result=new Array()

        // Parcours de la liste des étapes du parcours :
        tabSteps.forEach((chronoStep) => {
            // console.log("hasCoordinates: "+chronoStep.hasCoordinates()+" lat: "+chronoStep.departureLat+" "+chronoStep.destinationLat+" lng: "+chronoStep.departureLng+" "+chronoStep.destinationLng)
            if(chronoStep.hasCoordinates()) {   // l'étape est affichable (on a des coordonnées des ports de départ et d'arrivée)
                if(chronoStep.isDisplayableOnMap()) {   // Pas d'autre anomalie détectée
                    const tabLatLngs=chronoStep.supprimerPointsSuccessifsIdentiques()   // debug : supprimer deux éléments identiques
                    if(tabLatLngs.length > 1) { // Au moins deux points différents à afficher
                        if((graphMode == MODE_PLAYER) || ((graphMode == MODE_ZOOM) && (chronoStep.apiColor != "red"))) {   // En mode zoom, ne pas afficher les étapes en rouge
                            result.push(chronoStep)
                        }
                    }
                }
            }
        })
        return result
    }

    // Renvoie la liste des composants <StepSpline /> correspondant aux seules étapes affichables du parcours 
    const getStepsSplines = (tabDisplayableSteps, layerGroupRef) => {
        const tabResult=new Array()

        tabSteps.forEach((chronoStep, stepIndex) => {    // Parcours du tableau de TOUTES les étapes, pour bonne valeur de stepIndex
            if(tabDisplayableSteps.includes(chronoStep)) {  // si l'étape est dans la liste des étapes à retourner
                tabResult.push(<StepSpline 
                    productsList={productsList} 
                    chronoStep={chronoStep} 
                    nbParcours={nbParcours} 
                    differentiateColors={differentiateColors} 
                    layerGroupRef={layerGroupRef} 
                    isSelected={stepIndex == highlightedStepIndex} 
                    graphMode={graphMode} 
                    key={"step-"+chronoStep.routeIndex+"-"+chronoStep.rank} 
                />)
            }
        })
        return tabResult
    }

    // Renvoie le rectangle englobant des splines du parcours
    const getSplinesBounds = (tabSteps) => {
        var tabLatLngsParcours=[]
        
        // Faire un tableau de tous les points des splines du parcours :
        tabSteps.forEach((chronoStep) => {   // pour chaque étape du parcours
            const tabLatLngsEtape=chronoStep.latLngs // Lire le tableau des coordonnées servant à tracer la spline de l'étape
            // console.log(tabLatLngsEtape)
            if(tabLatLngsEtape.length > 0) { // Le tableau n'est pas vide (la spline est fournie pour cette étape, geom4326 n'est pas nul)
                tabLatLngsParcours=tabLatLngsParcours.concat(tabLatLngsEtape)    // Ajoute les points de la spline de l'étape, au tableau du parcours
            } else {    // le tableau est vide : pas de spline pour cette étape
                const tabLatLngsDepartArrivee=chronoStep.getDepartureDestinationCoordinates()   // coordonnées des villes de départ et d'arrivée
                // console.log(chronoStep.rank)
                // console.log(tabLatLngsDepartArrivee)
                tabLatLngsParcours=tabLatLngsParcours.concat(tabLatLngsDepartArrivee)   // ajout des coordonnées des villes de départ / arrivée aux coordonnées du parcours
            }
        })
        // Extraire le rectangle englobant :
        const newBounds=L.latLngBounds(tabLatLngsParcours)
        return newBounds
    }

    // MAJ de la liste des étapes affichables
    useEffect(() => {
        if(tabSteps && layerGroupRef.current) { // MAJ possible
            // Etapes affichables -> splines à afficher
            const tabDisplayableSteps=getDisplayableSteps() // Refaire la liste des étapes affichables
            const newStepsSplines=getStepsSplines(tabDisplayableSteps, layerGroupRef)   // Splines correspondant aux étapes
            setStepsSplines(newStepsSplines) // MAJ liste des splines à afficher
            // En mode zoom, zoomer sur l'ensemble du parcours
            if(graphMode == MODE_ZOOM) {
                const newBounds=getSplinesBounds(tabSteps)   // extraire le rect. englobant des points des splines des étapes du parcours
                setZoomBounds(newBounds)    // stocke le rect. englobant dans l'état, ce qui va déclencher le zoom
            }
        }
    }, [tabSteps, highlightedStepIndex, graphMode]); // Fait initialement, et quand une de ces variables change

    // En mode zoom, zoomer sur l'ensemble du parcours
    useEffect(() => {
        if(zoomBounds) {
            map.fitBounds(zoomBounds, {maxZoom: 10}) // Faire un zoom sur le parcours, sans dépasser une certaine valeur
        }
    }, [zoomBounds])

    // Signaler à l'appelant que la couche de carte est prête
    useEffect(() => {
        // console.log("useEffect (layerIsReady : "+layerIsReady+")");
        if(whenReady) { // Une fonction est fournie par l'appelant, on peut l'exécuter :
            whenReady();
            // console.log("whenReady()")
        }
    }, [layerIsReady]); // Fait initialement, et quand layerIsReady change

    
    // Rendu du composant :

    // Si l'overlay est visible (coché) initialement :
    const isChecked=(checked == undefined) ? false : checked;

    // Nom de l'overlay : si 'name' pas spécifié, mettre une valeur par défaut :
    const overlayName=(name == undefined) ? t('stepsOverlay.overlay_name_default', "Steps") : name;
    
    return (<>
        { tabSteps && 
            <LayersControl.Overlay  checked={isChecked} name={overlayName} >
                <LayerGroup ref={layerGroupRef}>
                    { layerGroupRef.current && stepsSplines}
                </LayerGroup>
                { !layerIsReady && setLayerIsReady(true)}
            </LayersControl.Overlay>
        }
    </>)
}