// UpdateZoomCenter.jsx
/**
 * Composant qui met à jour, dans l'état de VizSources, les valeurs de 'center' et 'zoom'
 * Ce composant non-visuel doit être enfant de MapContainer (situé dans MapOfFrenchPorts), pour s'abonner aux événements Leaflet : zoomend et moveend
 */

import {useMapEvents} from "react-leaflet"

export const UpdateZoomCenter=({changeCenter, changeZoom}) => {

    // Pour mémoriser les valeurs de zoom et de center, enregistrer des gestionnaires d'événements : 
    // ( liste des événements : https://leafletjs.com/reference.html#map-event )
    const map = useMapEvents({
        zoomend: (event) => {    // Niveau de zoom
            // console.log("zoomend");
            const zoomLevel=map.getZoom()
            // console.log(zoomLevel)
            changeZoom(zoomLevel)
        },     // Zoom avant ou arrière terminé
        moveend: () => {    // Centre de la carte
            // console.log("moveend");
            const centerCoords=map.getCenter()
            changeCenter(centerCoords)
        },  // modification du Center terminée
    })

    // Ce composant n'affiche rien :
    return null
}





