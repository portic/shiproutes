// LegendHasAClerk.jsx
/**
 * Affiche la légende de la couche : HasAClerk (Ports avec présence d'un greffier)
 * 
 * translate prefix : legend
 */
import 'bootstrap/dist/css/bootstrap.min.css';
import { Row, Col } from 'react-bootstrap';
import React from 'react';
import { Trans, useTranslation } from 'react-i18next';
import '/static/css/legend-symbols.css'

export const LegendHasAClerk = ({legendClassnames: cNames }) => {
    const { t } = useTranslation();

    // Rendu du composant
    return (<div>
        <Row className={cNames.legendItemRow} >
            <Col xs={cNames.symbolColWidthInGrid} className={cNames.symbolCol} >
                <div className={"legendSquareHasAClerk"} >&nbsp;</div>
            </Col>
            <Col xs={cNames.textColWidthInGrid} className={cNames.textNoHorizontalMargin}>
                <Trans i18nKey="legend.hasAClerk_clerk">admiralty clerk on site</Trans>
            </Col>
        </Row>
    </div>)
}