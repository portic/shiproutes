// LegendAdmiraltyLim.jsx
/**
 * Affiche la légende de la couche : Limites d'amirautés (Pointillé séparant les amirautés sur les côtes / fleuves)
 */
import 'bootstrap/dist/css/bootstrap.min.css';
import { Row, Col } from 'react-bootstrap';
import React from 'react';
import { Trans, useTranslation } from 'react-i18next';
import '/static/css/legend-symbols.css'

export const LegendAdmiraltyLim = ({layer, legendClassnames: cNames }) => {
    const { t } = useTranslation();

    // Rendu du composant
    return (<div>
        <Row className={cNames.layerNameRow} >
                <Col className={cNames.textNoHorizontalMargin} >
                    <u>{layer.name} : </u>
                </Col>
        </Row>
        <Row className={cNames.legendItemRow} >
            <Col xs={cNames.symbolColWidthInGrid} className={cNames.symbolCol} >
                <div className={"legendLineAdmiraltyLim"} >&nbsp;</div>
            </Col>
            <Col xs={cNames.textColWidthInGrid} className={cNames.textNoHorizontalMargin}>
                <Trans i18nKey="legend.admiralty_limits">admiralty limits</Trans>
            </Col>
        </Row>
    </div>)
}
