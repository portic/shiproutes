// LegendVar.jsx
/**
 * Affiche la légende des couches : Variables (complétude) - autres que congés
 * 
 * translate prefix : legend
 */
import 'bootstrap/dist/css/bootstrap.min.css';
import { Row, Col } from 'react-bootstrap';
import React from 'react';
import { Trans, useTranslation } from 'react-i18next';
import '/static/css/legend-symbols.css'
 
export const LegendVar = ({layer, legendClassnames: cNames }) => {
    const { t } = useTranslation();

    // Rendu du composant
    return (<div>
        <Row className={cNames.layerNameRow} >
            <Col className={cNames.textNoHorizontalMargin} >
                <u>{layer.name} : </u>
            </Col>
        </Row>
        <Row className={cNames.legendItemRow} >
            <Col xs={cNames.symbolColWidthInGrid} className={cNames.symbolCol} >
                <div className={"legendCircleNoConge"} >&nbsp;</div>
            </Col>
            <Col xs={cNames.textColWidthInGrid} className={cNames.textNoHorizontalMargin}>
                <Trans i18nKey="legend.no_data">no data</Trans>
            </Col>
        </Row>
        <Row className={cNames.legendItemRow} >
            <Col xs={cNames.symbolColWidthInGrid} className={cNames.symbolCol} >
                <div className={"legendCircleVar20"} >&nbsp;</div>
            </Col>
            <Col xs={cNames.textColWidthInGrid} className={cNames.textNoHorizontalMargin}>
                {t('legend.var_20', "0 to 20% ({{ count }})", {count: layer.effectif[0] })}
            </Col>
        </Row>
        <Row className={cNames.legendItemRow} >
            <Col xs={cNames.symbolColWidthInGrid} className={cNames.symbolCol} >
                <div className={"legendCircleVar40"} >&nbsp;</div>
            </Col>
            <Col xs={cNames.textColWidthInGrid} className={cNames.textNoHorizontalMargin}>
                {t('legend.var_40', "21 to 40% ({{ count }})", {count: layer.effectif[1] })}
            </Col>
        </Row>
        <Row className={cNames.legendItemRow} >
            <Col xs={cNames.symbolColWidthInGrid} className={cNames.symbolCol} >
                <div className={"legendCircleVar60"} >&nbsp;</div>
            </Col>
            <Col xs={cNames.textColWidthInGrid} className={cNames.textNoHorizontalMargin}>
                {t('legend.var_60', "41 to 60% ({{ count }})", {count: layer.effectif[2] })}
            </Col>
        </Row>
        <Row className={cNames.legendItemRow} >
            <Col xs={cNames.symbolColWidthInGrid} className={cNames.symbolCol} >
                <div className={"legendCircleVar80"} >&nbsp;</div>
            </Col>
            <Col xs={cNames.textColWidthInGrid} className={cNames.textNoHorizontalMargin}>
                {t('legend.var_80', "61 to 80% ({{ count }})", {count: layer.effectif[3] })}
            </Col>
        </Row>
        <Row className={cNames.legendItemRow} >
            <Col xs={cNames.symbolColWidthInGrid} className={cNames.symbolCol} >
                <div className={"legendCircleVar100"} >&nbsp;</div>
            </Col>
            <Col xs={cNames.textColWidthInGrid} className={cNames.textNoHorizontalMargin}>
                {t('legend.var_100', "81 to 100% ({{ count }})", {count: layer.effectif[4] })}
            </Col>
        </Row>
    </div>)
}