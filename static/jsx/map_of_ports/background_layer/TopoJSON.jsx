// TopoJSON.jsx
/**
 * <TopoJSON> est un composant pour React-Leaflet qui charge un fichier TopoJson et le convertit en GeoJSON, 
 * puis renvoie un objet <GeoJSON> de React-Leaflet.
 * Code pris ici : https://pretagteam.com/question/reactleaflet-adding-a-topojson-layer
 * Nécessite le package yarn : topojson-client, voir : https://www.npmjs.com/package/topojson-client
 */
import React, { useRef, useEffect } from "react";
import { GeoJSON } from "react-leaflet";
import * as topojson from "topojson-client";
 
export const TopoJSON = ({ data, ...defProps }) => {
    const layerRef = useRef(null);
 
    const addData = (layer, jsonData) => {
        if (jsonData.type === "Topology") {
            for (let key in jsonData.objects) {
                let geojson = topojson.feature(jsonData, jsonData.objects[key]);
                layer.addData(geojson);
            }
        } else {
            layer.addData(jsonData);
        }
    }
 
    useEffect(() => {
        const layer = layerRef.current;
        addData(layer, data);
    }, []);
 
    return <GeoJSON ref={layerRef} {...defProps} />;
}