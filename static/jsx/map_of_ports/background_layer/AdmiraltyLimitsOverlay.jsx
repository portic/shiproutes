// AdmiraltyLimitsOverlay.jsx
/**
 * Couche de carte : Limites des amirautés sur les côtes / fleuves, à afficher en traits pointillés rouges
 * A partir d'un fichier GeoJSON : static/maps/limites_amirautes_4326_utf8.json
 * 
 * Propriétés à passer :
 * - layerData : visibilité, et nom de la couche
 * - lookOptions (un style par défaut est fourni - style appliqué aux régions GeoJSON, voir Path options : https://leafletjs.com/reference.html#path-option )
 */

import React from 'react';
import 'leaflet/dist/leaflet.css';
import 'leaflet/dist/leaflet.js'
import { LayersControl, GeoJSON, LayerGroup } from 'react-leaflet'
import admiraltyLimits from '../../../maps/limites_amirautes_4326_utf8.json'

export const AdmiraltyLimitsOverlay = ({layerData, lookOptions}) => {
    // Rendu du composant :

    // Style pour les limites d'amirautés :
    // Styling of GeoJSON : style, with Path options : https://leafletjs.com/reference.html#path-option
    const admiraltyLimitsLookDefault={
        // borders on polygons :
        stroke: true,
        color: "#B2886C",   // Marron foncé, de la charte graphique Portic
        weight: 2.5,
        opacity: 0.9,
        dashArray: "4,6",  // Pattern des pointillés, voir : https://developer.mozilla.org/fr/docs/Web/SVG/Attribute/stroke-dasharray 
        // filling of polygons :
        fill: false,
        // Custom CSS class name set on an element. Only for SVG renderer.
        // className: xxx;
    }
    
    // Si pas de mise en forme spécifiée, reprendre le look par défaut 
    const admiraltyLimitsLook=(lookOptions == undefined) ? admiraltyLimitsLookDefault : lookOptions;
    // console.log(layerData.visible);
    
    return (
        <LayersControl.Overlay checked={layerData.visible} name={layerData.name} > 
            <LayerGroup>

                <GeoJSON
                    data={admiraltyLimits} 
                    style={admiraltyLimitsLook} 
                />
                
            </LayerGroup>
        </LayersControl.Overlay>
    )
}
