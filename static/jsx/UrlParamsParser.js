// UrlParamsParser.js
/**
 * Classe parseur des paramètres passés dans l'URL de l'application ShipRoutes.
 * 
 * Permet d'obtenir la liste des valeurs des paramètres de l'application : paramsList
 * - paramètres reconnus : by_captain, captain_id_1, ship_id_1, captain_id_2, ship_id_2, filter_red_steps, filter_orange_steps
 * - liste des valeurs autorisées pour chaque paramètre
 * - valeurs par défaut à utiliser (si paramètre manquant ou valeur erronée)
 * - signale les valeurs erronées dans la console
 * 
 * Documentation avec JSDoc : https://en.wikipedia.org/wiki/JSDoc
 * 
 * translate prefix : mainComponent
 */

import { t } from "i18next";
import { ListCaptainsShips } from "./datamodel/ListCaptainsShips";

export class UrlParamsParser {
    // Résultat : Liste des paramètres décodés (ou sinon par défaut) :
    listParams={}
    parametersAreValid=false    // indique si on peut afficher quelque chose avec les paramètres qui sont valides
    // (stocké dans l'instance de la classe)

    // Valeurs autorisées :
    static booleanOk=["true", "false"]

    // Valeurs par défaut :
    static defaultListParams={
        "by_captain": false, 
        // "captain_id_1": "00002918", 
        // "ship_id_1": "0002931N", // Fidèle Marianne
        "captain_id_1": null, 
        "ship_id_1": null, 
        "captain_id_2": null, 
        "ship_id_2": null, 
        "filter_red_steps": true, 
        "filter_orange_steps": false
    }

    /**
     * Constructeur qui parse (isole / valide / stocke) les paramètres reçus par l'URL de l'application
     * ex. d'URL : http://127.0.0.1:5000/index.html?by_captain=false&filter_red_steps=false&filter_orange_steps=false
     * partie contenant les paramètres : ?by_captain=false&filter_red_steps=false&filter_orange_steps=false
     * pour obtenir les paramètres de l'URL courante, utiliser : window.location.search
     * @see https://developer.mozilla.org/en-US/docs/Web/API/URLSearchParams
     * @param {string} parametersPortionOfUrl La portion de l'URL de l'application qui contient les paramètres (?param=value&otherParam=otherValue) : window.location.search
     */
    constructor(parametersPortionOfUrl) {
        this.parseParameters(parametersPortionOfUrl)    // isole / valide / stocke les paramètres dans la chaîne passée au constructeur
        this.validateParameters()   // Vérifie si les paramètres sont exploitables (suffisamment OK ou pas)
    }

    /**
     * Emet un message d'erreur. 
     * Signale une erreur dans l'URL. 
     * Si on veut modifier la façon de signaler une erreur dans l'URL de l'application, c'est ici qu'il faut le faire. 
     * @param {string} errorMsg message d'erreur
     */
    signalUrlError(errorMsg) {
        // Signaler l'erreur dans la console :
        console.error(errorMsg)
    }

    /**
     * Signaler un paramètre incorrect passé dans l'URL
     * @param {string} paramName le paramètre pour lequel il y a un problème
     * @param {boolean} isUnknownParam true si paramètre inconnu. false si problème avec la valeur du paramètre
     * @param {string} value la valeur reçue pour ce paramètre
     * @param {Array} tabKnownValues liste (tableau) des valeurs autorisées (attendues) pour ce paramètre
     */
    signalIncorrectUrlParam(paramName, isUnknownParam, value, tabKnownValues) {
        var errorMsg=""

        if(isUnknownParam) {    // Nom de paramètre inconnu
            errorMsg=t('mainComponent.unknown_param_error', "Error in URL : '{{ paramName }}' unknown parameter", {paramName: paramName})
        } else {    // Paramètre connu, mais valeur incorrecte
            errorMsg=t('mainComponent.incorrect_value_error', "Error in URL : bad value '{{ value }}' for parameter '{{ paramName }}'. Correct values are : {{ listOfValues }}", {
                paramName: paramName,
                value: value,
                listOfValues: tabKnownValues.toString()
            })
        }
        this.signalUrlError(errorMsg)
    }

    // Gère la valeur reçue pour un paramètre booléen
    parseBooleanParam(paramName, valueReceived) {
        const valueLowercase=valueReceived.trim().toLowerCase() // True -> true

        if (UrlParamsParser.booleanOk.includes(valueLowercase)) {    // valeur OK
            const paramValue=(valueLowercase === 'true')
            Object.defineProperty(this.listParams, paramName, {
                writable: true,
                value: paramValue   // remplacement de la valeur dans la liste des paramètres
              })
        } else {    // valeur incorrecte
            this.signalIncorrectUrlParam(paramName, false, valueReceived, UrlParamsParser.booleanOk)
        }
    }

    // Gère la valeur reçue pour un paramètre Id (chaine)
    parseIdStringParam(paramName, valueReceived) {
        const valueUppercase=valueReceived.trim().toUpperCase() // 0001234n -> 0001234N
        var errorMsg=""

        // Valider la longueur : 8 car.
        if(valueUppercase.length != 8) {    // longueur incorrecte
            errorMsg=t('mainComponent.incorrect_id_format_error', "Error in URL : bad value '{{ value }}' for parameter '{{ paramName }}' that must be an id. Id examples (8 chars) : 00002918, 0002931N", {
                value: valueReceived,
                paramName: paramName
            })
            this.signalUrlError(errorMsg)
        } else {
            /* Début test désactivé pour permettre les futurs doc_id
            // Valider la syntaxe :
            const regexValidId = /^0+\d+N?$/  // 0 initial (1+ fois), entier (sans virgule/point), N optionnel en fin
            const idCorrect=regexValidId.test(valueUppercase)
            if (!idCorrect) { // format incorrect
                errorMsg=t('mainComponent.incorrect_id_format_error', "Error in URL : bad value '{{ value }}' for parameter '{{ paramName }}' that must be an id. Id examples (8 chars) : 00002918, 0002931N", {
                    value: valueReceived,
                    paramName: paramName
                })
                this.signalUrlError(errorMsg)
            } else {    // Format correct
            */
                // Stocker la valeur :
                Object.defineProperty(this.listParams, paramName, {
                    writable: true,
                    value: valueUppercase   // remplacement de la valeur dans la liste des paramètres
                  })
            // }    // Fin test désactivé
        }
    }

    /**
     * Lecture des paramètres passés dans l'URL (pour remplir this.paramsList). 
     * Appelée par le constructeur. 
     * @param {string} parametersPortionOfUrl la portion de l'URL de l'application qui contient les paramètres (?param=value&param2=value2&...)
     */
    parseParameters(parametersPortionOfUrl) {
        this.listParams={}  // ré-initialisation
        var errorMsg=""
        
        // Copier d'abord les valeurs par défaut des paramètres de l'URL :
        this.listParams=Object.assign(this.listParams, UrlParamsParser.defaultListParams)

        // Lire les paramètres de l'URL de l'application :
        const queryParams = new URLSearchParams(parametersPortionOfUrl)
        // Parcourir la liste pour valider les paramètres attendus :
        for (const [param, value] of queryParams) {
            switch(param) {
                // Booléens :
                case "by_captain" : // true : recherche par capitaine, false : recherche par navire
                case "filter_red_steps" : // true : masquer les étapes réfutées, false : les afficher
                case "filter_orange_steps" : // true : masquer les étapes controversées, false : les afficher
                    this.parseBooleanParam(param, value)
                    break;
                // Chaines de car. au format "id" (un zéro, puis des chiffres, et un N optionnel en fin)
                case "captain_id_1" : 
                case "ship_id_1" : 
                case "captain_id_2" : 
                case "ship_id_2" :
                    this.parseIdStringParam(param, value)
                    break;
                default:    // paramètre inconnu
                    this.signalIncorrectUrlParam(param, true, '', [])
                    break;
            }
        }
    }

    /**
     * Vérifie si les paramètres reçus sont exploitables et permettent d'afficher quelque chose
     * Si oui, appeler plus tard (après chargement des données) la fonction exploitant les données de l'API pour construire la sélection de routes correspondante
     */
    validateParameters() {
        if((this.listParams.captain_id_1 !== null) && (this.listParams.ship_id_1 !== null)) {   // Si on a (au moins) un id de capitaine ET de navire,
            this.parametersAreValid=true    // On peut au moins afficher un parcours
        }
    }

    // Fabrique le tableau d'objets routeData, en retrouvant les données nécessaires dans les données de l'API
    prepareSelectedRoutes(apiData) {
        const result=new Array()    // tableau de routeData
        const dataList=new ListCaptainsShips(apiData)   // Construit les listes de capitaines et de navires à partir de l'API
        const tabIdCouples=new Array()
        var idCouples=null

        // Remplir le tableau des couples d'id (captain_id, ship_id) à rechercher :
        // Couple 1 :
        if((this.listParams.captain_id_1 !== null) && (this.listParams.ship_id_1 !== null)) {   // Si on a (au moins) un id de capitaine ET de navire,
            idCouples=[this.listParams.captain_id_1, this.listParams.ship_id_1] // créer le couple
            tabIdCouples.push(idCouples)    // ajouter à la liste des parcours à rechercher
        }
        // Couple 2 :
        if((this.listParams.captain_id_2 !== null) && (this.listParams.ship_id_2 !== null)) {   // Si on a (au moins) un id de capitaine ET de navire,
            idCouples=[this.listParams.captain_id_2, this.listParams.ship_id_2] // créer le couple
            tabIdCouples.push(idCouples)    // ajouter à la liste des parcours à rechercher
        }

        // Chercher les données (routeData) des parcours correspondants :
        dataList.buildRoutesFoundFromSelectedIds(this.listParams.by_captain, tabIdCouples)

        // Préparer le tableau de routeData :
        // Extraire les objets routeData de chaque RouteInfo du tableau routesFound[] :
        dataList.routesFound.forEach(route => {
            result.push(route.routeData)
        })

        return result
    }


};