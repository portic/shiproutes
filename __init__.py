#!/usr/bin/python3
# -*-coding:UTF-8 -*

from flask import Flask, render_template, send_from_directory, redirect

app = Flask(__name__)

# racine : application Shiproutes (React)
@app.route("/")
@app.route("/index.html")
def index():
    return render_template("index.html")

# Aide de Shiproutes (racine)
# note : the default generated version of the doc site must not be displayed, 
# because the language switcher doesn't work in this version 
# => redirect to 'en' version
@app.route("/static/manual/")
@app.route("/static/manual/index.html")
def help_redirect():
    # Permanent Redirect
    return redirect('en/', 301)

@app.route("/static/manual/en/")
def help_en():
    return send_from_directory('static/manual/en/', 'index.html')

@app.route("/static/manual/fr/")
def help_fr():
    return send_from_directory('static/manual/fr/', 'index.html')

if __name__ == "__main__":
    app.run(debug=True)