---
language: fr
title: Manuel utilisateur de ShipRoutes
summary: Notice d'aide à l'utilisation du logiciel ShipRoutes, pour la visualisation des parcours des navires et des capitaines (projet ANR Pornic)
authors:
    - Silvia Gallimard
    - Bernard Radines
date: 2023-03-20
copyright: © the ANR Portic Project 2019-2023. All rights reserved.
license: GNU Affero General Public License Version 3 (AGPL v3)
document_url: http://shiproutes.portic.fr/static/manual/fr/usermanual.html
shiproutes_url: http://shiproutes.portic.fr/
portic_url: https://anr.portic.fr
---

# Introduction. A quoi sert cette visualisation ?

L'application Web décrite ici, fonctionnant dans le navigateur, est accessible à l'URL : [http://shiproutes.portic.fr/](http://shiproutes.portic.fr/)

Elle permet de visualiser dans le temps et dans l'espace les parcours des capitaines et des navires qui figurent dans la base de données de Portic. Pour explorer le contenu de la base de données, vous pouvez consulter l'application [VizSources](http://vizsources.portic.fr) ainsi que le [site du programme Portic](https://anr.portic.fr/).

Les données collectées ont été pourvues d'un identifiant de capitaine et d'un identifiant de navire. Si nous avons jugé qu'il s'agit du même capitaine (ou du même navire) déjà rencontré ailleurs dans les sources, l'utilisation du même identifiant permet dès lors de suivre son itinéraire dans le temps, à travers les différentes sources présentes dans la base.

Le travail d'attribution d'identifiants et de correction des erreurs est en cours. N'hésitez pas à revenir sur ce site, et à nous écrire pour nous signaler des erreurs à l'adresse suivante : *interfaces[at]portic.fr*

### Principales fonctions

- [**recherche** du parcours](#1-comment-effectuer-une-recherche) d'un navire, ou d'un capitaine, dans la base de données de Portic
- [**visualisation** des étapes du parcours](#2-la-visualisation-dun-seul-parcours), avec leur degré d'incertitude :
    - dans le temps sous forme d'un chronogramme (une étape = une flèche d'un port vers un autre),
    - dans l'espace, par l'affichage des déplacements en mer sur une carte (itinéraire, ou route maritime).
- possibilité de [visualiser **deux parcours à la fois**](#3-visualiser-deux-parcours-en-meme-temps)
- possibilité d'[**obtenir un lien** (URL)](#4-exporter-les-parcours-en-tant-que-lien) pour partager un parcours d'intérêt

Les mentions obligatoires à citer en cas d'utilisation d'éléments produits avec notre interface figurent en fin de ce manuel.

### Choix de la langue

- par défaut : français si navigateur Web en français, sinon anglais
- possibilité de changer la langue par la liste déroulante à droite du bandeau de titre ![sélecteur de langue de l'application](img/sel_lang_app.jpg)
- dans l'aide du logiciel, possibilité également de choisir la langue d'affichage de la même façon ![sélecteur de langue de l'aide](img/sel_lang_help.jpg)

### Cheminement global : comment utiliser ShipRoutes

L'utilisation de ShipRoutes passe par trois étapes :

1. La recherche et la sélection d'un capitaine ou d'un navire dont on souhaite visualiser le parcours, à l'aide de l'**outil de recherche** ; 
2. La lecture de la visualisation de l'ensemble du parcours choisi (chronogramme et carte) ; 
3. La possibilité de faire défiler (manuellement ou automatiquement) les étapes du parcours, simultanément sur le chronogramme, et sur la carte.

# 1. Comment effectuer une recherche

## 1.1 Choisir de chercher un capitaine ou un navire

L'application permet de rechercher et sélectionner un capitaine ou un navire dont on souhaite visualiser le parcours. Pour commencer, choisir d'abord :

- recherche par nom du capitaine (cliquer sur l'icône 'personne' ![icône 'personne'](img/icone_personne.jpg) ou la zone de texte correspondante),
- ou recherche par nom du navire (cliquer sur l'icône 'navire' ![icône 'navire'](img/icone_navire.jpg) ou la zone de texte correspondante).

La coche sur la droite d'un des deux pictogrammes indique que la dernière recherche s'est effectuée dans ce mode (par capitaine, ou par navire).

## 1.2 Comment effectuer la recherche d'un nom de capitaine ou de navire

Ce premier choix effectué, il est possible de rechercher le nom du capitaine ou du navire par **trois modalités** de recherche différentes : 

- par nom, 
- par nom avec variantes, 
- par identifiant (id). 
 
Pour en choisir une, cliquer sur le bouton correspondant dans cette barre :

![bouton de choix du mode de recherche](img/btn_choix_mode_recherche.jpg)

!!! note "Nota bene"
 
    Quelle que soit la modalité choisie, une fois sélectionné le (ou les) nom(s) qui vous intéressent, dans le tableau des résultats il faudra : 

    - cocher un ou deux parcours à visualiser,

        ![sélection d'un résultat de recherche](img/ex_select_result.jpg)

    - puis cliquer sur le bouton "Voir" pour passer à la visualisation.

        ![cliquer sur le bouton 'Voir'](img/btn_voir.jpg)


### 1.2.1 Modalité de recherche : **Par nom**

Il suffit de taper dans la zone de recherche la suite des caractères composant le nom souhaité : 

- pour rechercher un capitaine, taper d'abord son nom, une virgule, un espace, puis son prénom
- dans la liste déroulante, les résultats présentés sont limités aux termes demandés (filtrage)
    
    ![exemple de recherche par nom](img/ex_rech_par_nom.jpg)

- enfin, cliquer sur le nom de son choix dans la liste déroulante pour le sélectionner
 
!!! warning "Attention"

    Cette modalité de recherche ne donne des résultats que si la **chaîne exacte** des caractères recherchés est respectée. Dans l'exemple précédent, une recherche << Abaud Raymond >> ne donnera pas de résultat car la virgule a été omise. Il faut saisir << Abaud, Raymond >>. La recherche n'est pas sensible aux majuscules.

!!! warning "Attention"

    Cette modalité de recherche ne donne de résultat **que sur la variante du nom la plus fréquente** (ou, en cas d'égalité des nombres d'occurrences, sur la première variante présente dans la base). Pour rechercher parmi toutes les variantes, il faut utiliser la modalité de recherche "Par nom (ou variante du nom)". 

!!! note "Nota bene"

    Dans le tableau des résultats de recherche, pour l'affichage des différentes formes du nom (du capitaine ou du navire) :  
        
    - c'est la forme la plus fréquente qui s'affiche
    - au survol du nom avec la souris, on peut voir les autres formes : "variante (nombre d’occurrences)"
    ![variantes du nom](img/ex_variantes.jpg)

### 1.2.2 Modalité de recherche : **Par nom (ou variante du nom)**

Cette fonctionnalité permet de rechercher parmi les orthographes des noms plus ou moins fréquemment rencontrées (majoritaires ou minoritaires) : 

- orthographe **majoritaire** : celle qui est la plus fréquemment rencontrée dans les documents de l'époque. C'est celle qui sera affichée dans les étapes suivantes de la recherche de parcours, et qui est associée à l'identifiant affiché
- orthographe **minoritaire** : une variation du nom, mentionnée moins fréquemment dans les documents. Elle est toujours suivie de la forme majoritaire entre parenthèses dans nos résultats de recherche, et elle est associée à l'identifiant de la forme majoritaire.
  
Pour effectuer la recherche :
    
- dans le champ de recherche ("Mots à rechercher"), saisir des mots ou fragments de mots dans n'importe quel ordre (pas forcément dans l'ordre : nom, prénom)
- dans la zone des résultats de recherche située juste en-dessous, les orthographes du nom comprenant tous les mots tapés (correspondances trouvées) s'affichent
- format d'affichage : 
  
    id:000xxxxx (000xxxxN pour les navires) orthographe minoritaire (orthographe majoritaire)

- si plusieurs formes d'un même nom (même identifiant) correspondent à la recherche, la forme majoritaire est affichée en italique gras avec son identifiant, et toutes les formes de ce nom qui correspondent à la recherche sont affichées légèrement en décalé.

    ![exemple de recherche par nom (ou variante du nom)](img/ex_rech_par_nom_alt.jpg)
 
- enfin, **cliquer sur la forme de son choix** dans la liste des résultats
  
    !!! note "Nota bene"
            
        Dans le tableau des résultats de recherche, c'est l'orthographe majoritaire correspondante qui s'affichera. On pourra le vérifier en passant la souris au-dessus des noms dans la liste des parcours trouvés. Toutes les orthographes rencontrées dans les documents de l'époque s'afficheront dans une bulle d'aide, suivies de leurs fréquences respectives entre parenthèses.

        ![variantes du nom](img/ex_variantes_2.jpg)

- vous pouvez **sélectionner plusieurs formes** dans la liste des noms trouvés, afin de comparer ou visualiser les parcours qui leurs sont associés
 
    !!! note "Pour réaliser une sélection multiple, vous pouvez :"

        - sélectionner / désélectionner **plusieurs noms de la liste**, en maintenant enfoncée la touche "Shift" (touche permettant de taper un caractère en majuscule) pendant le clic de la souris ;
        - sélectionner **une plage de noms** successifs dans la liste, en cliquant d'abord sur le premier nom de la plage, puis en maintenant enfoncée la touche "Ctrl" pendant le clic de la souris sur le dernier nom de la plage.


### 1.2.3 Modalité de recherche : **Par id**

Permet de n'afficher que les parcours d'un capitaine ou navire précis (pas d'homonymie listée).

Cette modalité est utile surtout pour des internautes qui ont utilisé au préalable la base de données ou l'API pour identifier un ou plusieurs capitaines ou navires en fonction, par exemple, du trajet ou de la cargaison.

- il faut avoir noté auparavant l'identifiant précis du capitaine ou du navire dont on veut afficher le(s) parcour(s)
- la liste déroulante présente les identifiants figurant dans la base au format : 

    000xxxxx (pour les capitaines) ou 000xxxxN (pour les navires) suivi par l'orthographe majoritaire
    
- il faut saisir l'identifiant précis en prenant soin de taper au moins un zéro ("0") initial, suivi des chiffres de l'identifiant, et éventuellement du "N" final si on effectue une recherche de navire
- la liste est filtrée jusqu'à ne retenir que l'identifiant souhaité 

    ![exemple de recherche par id](img/ex_rech_par_id.jpg)

- il faut alors cliquer sur la ligne de l'identifiant souhaité pour le sélectionner 

## 1.3 Résultat de la recherche : la notion de 'parcours'

Quelle que soit la modalité de recherche choisie, le résultat s'affiche sous forme d'une **liste (tableau) des parcours trouvés** correspondant au nom recherché.

Un parcours correspond à une **combinaison (un capitaine ET un navire)**, sauf si un des identifiants est manquant (pas encore attribué). 

!!! note "Nota bene"

    Dans le cas où l'identifiant du capitaine ou du navire n'a pas encore été attribué, c'est l'identifiant du document (doc_id) de l'époque, dans lequel il est mentionné, qui est utilisé à la place. 

    !!! warning "Attention"

        Ce *doc_id* est "transitoire", il a vocation a être remplacé ultérieurement par l'id qui sera attribué. Il peut donc "disparaître" à tout moment, en fonction de l'évolution de nos investigations.

La liste des résultats comprend donc l'ensemble des parcours trouvés correspondant au nom saisi (autant de lignes qu'il y a de paires navire / capitaine répondant au critère choisi) :

- si la recherche donne un seul résultat, celui-ci est automatiquement coché (sélectionné) pour être visualisé en cliquant directement sur le bouton "Voir" (en bas à droite de la boîte de dialogue) : 
  
    ![cliquer sur le bouton 'Voir'](img/btn_voir.jpg)

- En cas de résultats multiples, l'utilisateur peut en cocher deux (paires d'identifiants différents), puis cliquer sur le bouton "Voir" pour visualiser les deux parcours (voir plus bas). 

La présence de plusieurs résultats peut s'expliquer par :

- une homonymie (cas très fréquent pour les navires)
- la présence dans la base de plusieurs parcours effectués par un même capitaine sur des navires différents (ou d'un même navire commandé par des capitaines différents).
 
![Exemple de capitaine présent sur deux navires différents](img/figure_2.jpg)  
***Capitaine ayant piloté deux navires différents** - Sélection de la variante du nom, et visualisation des deux parcours.*
 
## 1.4 Ajuster l'affichage - Affiner une recherche "très" fructueuse

Si la recherche produit beaucoup de résultats (exemple : navire "Marianne"), on peut : 

- **régler l'affichage** à l'écran (pagination des résultats), 
- **trier** les résultats pour mettre en évidence l'information pertinente, 
- **filtrer** les résultats plus avant pour en réduire le nombre.

### 1.4.1 Choisir le nombre de résultats affichés par page

Il est possible de choisir le nombre des résultats qui s'affichent sur la page, puis utiliser le système de **défilement** des pages de résultats.

![sélectionner le nombre de lignes par page](img/figure_1.jpg)  
***Pagination** - Ajuster le nombre de résultats par page, et boutons pour changer de page.*
 
### 1.4.2 Trier les résultats

Il est possible de **trier** les résultats selon les valeurs des colonnes. Le tri en cours est matérialisé par des flèches noires orientées dans le sens du tri (ascendant ou descendant) à côté des noms des colonnes concernées (tri multi-colonne possible).

![exemple de tri multi-colonne](img/ex_tri_multi-colonne.jpg)  
***Exemple de tri multi-colonne** - Tris ascendants sur le nom du navire, puis sur le nom du capitaine.*

Le principe d'un tri multi-colonne est le suivant : considérons deux lignes de données, si égalité sur la valeur de la colonne 1, alors on compare sur la colonne 2, si à nouveau égalité alors on trie sur la colonne 3, ...

Il faut cliquer sur le nom de la variable / de la colonne désirée, pour activer / désactiver le tri des lignes du tableau selon les valeurs de cette colonne. Plusieurs clics successifs sur un même entête de colonne permettent d'alterner entre : tri croissant, tri décroissant, pas de tri sur cette colonne.

Pour un tri multi-critères, après de simples clics sur une première colonne, maintenir la touche "Shift" enfoncée et cliquer sur un deuxième entête de colonne (plusieurs fois si nécessaire selon le type de tri désiré), pour effectuer le tri aussi sur une deuxième colonne (puis une troisième etc.). 

### 1.4.3 Filtrer les résultats

Il est possible de **filtrer** les résultats pour restreindre progressivement, champ par champ, la liste des résultats, ou pour retrouver un résultat en particulier. Pour filtrer :

- cliquer d'abord sur le bouton bascule "Filtrer" pour activer le formulaire de filtrage (Par défaut, aucun filtrage : pour revenir à l'option par défaut, cliquer sur le bouton bascule "Tout afficher")
- Le tableau des résultats s'affiche par défaut sur un nombre restreint de variables. Il est possible d'afficher plus de variables.

    filtres ***simples***: nom du navire, nom du capitaine, dates de début et fin d'observations ;

    filtres ***multiples***, en cliquant sur le bouton bascule "Plus de détails". 
    
- Pour revenir aux variables par défaut, cliquer sur le bouton bascule "Moins de détails".

![activer le filtrage des résultats](img/figure_3.jpg)  
***Activation du filtrage multiple** - Exemple de recherche par nom de navire : << Marianne >>.*
 
Champs affichés (colonnes) pour une recherche par **nom de navire** :

- filtres ***simples*** :
    - nom du navire (tri croissant),
    - nom du capitaine (tri croissant), 
    - dates de début et de fin d'observations ;
- filtres ***multiples*** - possibilité de sélectionner en plus :
    - l'identifiant du navire, 
    - le pavillon, 
    - le port d'attache, 
    - la classe de tonnage exprimée en tonneaux, 
    - le type de navire, 
    - l'identifiant du capitaine.

Champs affichés (colonnes) pour une recherche par **nom de capitaine** :

- filtres ***simples*** :
    - nom du capitaine (tri croissant), 
    - navire sur lequel il a effectué le parcours (tri croissant), 
    - dates de début et de fin (il s'agit de la date la plus ancienne et la plus récente à laquelle le capitaine est présent dans la base de données) ;
- filtres ***multiples*** - possibilité de sélectionner en plus :
    - l'identifiant du capitaine, 
    - son appartenance "nationale" (catalan, corse, suédois, etc.),
    - son appartenance locale (ex.: "d'Antibes"), 
    - l'identifiant du navire.
 
Chaque champ (critère) de filtrage permet une recherche par saisie directe dans le champ, puis sélection finale. Le filtrage est immédiat et la liste des résultats se met à jour :

- pour remettre à zéro le contenu d'un critère de filtrage, cliquer sur la croix (x) en bout de ligne
- pour arrêter le filtrage (retirer tous les critères), cliquer sur le bouton bascule "Tout afficher"

!!! note "Nota bene"

    Les filtres prennent en compte uniquement les champs renseignés dans la base. Si la variable est vide, le navire ou le capitaine correspondant ne s'affiche pas (alors qu'il pourrait *aussi* répondre au critère, s'il était connu). 

# 2. La visualisation d'un seul parcours

Après avoir coché la ligne du navire ou du capitaine qui vous intéresse, vous pouvez en visualiser le parcours en cliquant sur le bouton "Voir".

![cliquer sur le bouton 'Voir'](img/btn_voir.jpg)

## 2.1 Présentation de l'écran

Voici une vue globale de l'écran :

![comprendre l'écran d'affichage](img/figure_4.jpg)
 
 En **haut à gauche** de l'écran, les icônes "capitaine" et "navire" sont suivies par leurs noms et identifiants respectifs, pour vous rappeler la combinaison choisie :

![Affichage rappelant le parcours sélectionné](img/ex_1_parcours_selectionne.jpg)

!!! warning "Nota bene"

    Un clic sur les icônes 'capitaine' ou 'navire' (ou sur leur nom) permet de revenir à la fonction de recherche, pour sélectionner un autre parcours à visualiser.

En **dessous, à gauche**, s'affiche un **graphique (chronogramme)** illustrant le parcours de la combinaison capitaine/navire choisie :

![Chronogramme](img/ex_chronogramme.jpg)

- l'axe horizontal indique le temps
- l'axe vertical indique la latitude des localités touchées par le capitaine ou le navire 
- les segments du parcours affiché qualifient le degré d'incertitude liée à la nature des sources, tel que nous l'avons reconstitué. La légende en précise la sémiologie (voir plus bas en détail) : 

![Légende](img/ex_legende.jpg)

Il est possible de masquer ou afficher les étapes réfutées et / ou controversées en cochant les cases correspondantes dans la ligne au-dessous du graphique :

![Cases à cocher pour le masquage des étapes réfutées ou controversées](img/cases_masquage_etapes.jpg)

**A droite de l'écran** s'affiche la **carte** qui visualise par défaut l'intégralité du parcours de la combinaison capitaine/navire choisie. La sémiologie des étapes est identique à celle de la légende, mais les étapes réfutées sont omises :

![Carte](img/ex_carte.jpg)

## 2.2 Sémiologie des étapes

Grâce à l'identification des capitaines [(voir ici)](https://anr.portic.fr/incertitude/#Lidentification_des_capitaines) et des navires [(voir ici)](https://anr.portic.fr/incertitude/#Lidentification_des_navires), il nous est possible dans certains cas de confirmer ou infirmer la réalité d'un trajet déclaré. La sémiologie du chronogramme et de la carte indique le degré d'incertitude des trajets déclarés :

![Légende](img/ex_legende.jpg)

Type de ligne :

- ligne **pointillée** : trajet non attesté par les sources (non-déclaré), que nous avons déduit, entre deux escales successives déclarées (ex.: Un navire qu'on trouve au départ des Sables-d'Olonne pour Terre-Neuve en mars et qu'on retrouve ensuite au sortir de La Rochelle en juillet sera supposé avoir effectué le voyage retour de Terre-Neuve à La Rochelle) ;
- ligne **continue** : trajet déclaré par la source (dans l'exemple précédent : Sables-d'Olonne -> Terre-Neuve)

Couleur de ligne :

- si la ligne est **grise**, la base ne contient pas de source permettant de confirmer que le trajet déclaré au départ a été réellement effectué ;
- si la ligne est **verte**, la base de données permet de confirmer que le trajet déclaré au départ a été réellement effectué.
 
    ![Sémiologie de la légende - Exemple le plus fréquent](img/figure_5.jpg) 

- si la ligne est **jaune**, le trajet est controversé : il est théoriquement possible mais pour l'instant l'historien ne le considère pas vraisemblable, en raison par exemple d'une durée trop courte entre deux départs depuis deux localités très distantes entre elles ;

    ![Sémiologie de la légende - Exemple de trajet controversé](img/figure_6a.jpg)
 
- si la ligne est **rouge**, la base de données permet d'infirmer que le trajet déclaré au départ a été réellement effectué. Par exemple, nous infirmons le trajet Nantes -> Bordeaux et considérons que le navire a effectué le trajet Nantes -> La Rochelle -> Bordeaux, car nous disposons du registre des sorties de Bordeaux et que nous n'avons par trouvé deux sorties de ce navire. 

    ![Sémiologie de la légende - Exemple de trajet réfuté](img/figure_6b.jpg)

## 2.3 Interagir avec les visualisations

Les **boutons "Frise" et "Carte"** en bas à gauche permettent respectivement d’interagir avec le chronogramme ou la carte.

![Bouton Frise / Carte](img/bouton_frise_carte.jpg)

- Frise = interagir avec le chronogramme (par défaut)
- Carte = interagir avec la carte (pour faire défiler les étapes)

Ce qui est **affiché sur la carte** selon le mode :

- **En mode "Frise"**, la carte affiche l'intégralité des segments (étapes du parcours). Les étapes réfutées (en rouge) ne sont pas affichées sur la carte. Seuls les ports qui sont des étapes du parcours figurent sur la carte (les autres ports de France et d'ailleurs ne sont pas affichés) ;
- **En mode "Carte"**, un zoom automatique à chaque étape permet de voir l'intégralité de l'étape en cours (initialement : la première).

### Interaction avec le graphique/chronogramme

Dans certains cas, le **chronogramme** affiche un très grand nombre d'étapes d'un parcours d'un capitaine ou d'un navire, ce qui en compromet la lisibilité. Pour contourner ce problème, **en mode "Frise"**, il est possible de zoomer pour voir les détails (noms des ports, dates) :

- **zoom avec la mollette de la souris**, positionnée au-dessus du chronogramme : permet d'agrandir / faire glisser une zone du graphique, pour voir les détails (effet loupe 'temporelle' - axe des temps uniquement) ;
- **échelle des temps** : hors zoom, l'affichage des dates fait figurer des dates distantes d'au moins 15 jours (pour limiter les superpositions de dates trop proches).
 
    ![Réglage de l'affichage des étapes proches dans le temps](img/figure_7.jpg)
 
**En mode "Carte"**, l'étape en cours est **mise en évidence sur le chronogramme** par un changement de la couleur de fond, et un affichage en gras de la flèche concernée :

![Mise en évidence sur le chronogramme de l'étape affichée sur la carte](img/figure_8.jpg)
 
### Interaction avec la carte

En mode "Carte", le nombre des étapes s'affiche sur la dernière ligne, dans la partie gauche de l'écran, au centre (Ex: 1/7 = étape 1 sur un total de 7 étapes). **Le mode "Carte" permet de faire défiler le parcours** étape par étape :

![Faire défiler les étapes](img/figure_9.jpg)

- **la carte affiche l'étape en cours**, reliant deux ports (de départ, de destination), par un **segment passant par la mer**. Le zoom de la carte se règle automatiquement sur l'étape
- par défaut, le mode "Carte" est réglé sur pause, sur la première étape
- en *pause*, on peut **faire défiler les étapes** *manuellement* en cliquant sur le bouton à gauche (en arrière) ou à droite (en avant) de l'affichage de l'étape
- le chronogramme et la carte sont synchronisés : ils montrent la même étape
- on peut faire défiler les étapes *automatiquement* en cliquant sur *la flèche noire (play)*, au rythme sélectionné dans la liste déroulante juste à côté
- en lecture automatique, un clic à la souris sur la carte fait passer le player en mode pause (défilement manuel)
 
Le survol à la souris d'une étape **sur la carte** fait apparaître une **bulle d'information sur l'étape** :
 
![lecture de la bulle d'information d'une étape](img/figure_10.jpg)

Cette bulle d'information comporte :

- les **localités** de départ et d'arrivée : Départ -> Arrivée
- les **dates** de départ et d'arrivée : Date de départ - Date d'arrivée (durée de l'étape en nombre de jours)
 
!!! warning "Le format des dates"
    
    Les dates se présentent sous la forme **AAAA-MM-JJ** (année-mois-jour) :

    - Si le séparateur est un signe **"égal" (=)**, la date ***précise*** est indiquée par la source.
    - Si le séparateur est un signe **"majeur" (>)**, la date est ***postérieure*** à celle indiquée. 
    - Si le séparateur est un signe **"mineur" (<)**, la date est ***antérieure*** à celle indiquée. 

!!! note "Exemples"

    - Dans le cas d'un navire qui part de Bordeaux le 1787=01=01 pour La Rochelle, la date d'arrivée à La Rochelle est inconnue, mais postérieure (1787>01>01) :
        - si aucune autre donnée postérieure relative à ce navire n'est présente dans la base, la date de l'étape s'affiche : **1787=01=01 - 1787>01>01**
        - si ce même navire est observé à la sortie de La Rochelle le 1787=03=15, l'étape Bordeaux -> La Rochelle s'affiche avec les deux dates suivantes : **1787=01=01 - 1787<03<15**
    - Dans le cas d'un navire arrivé à Marseille le 1787=01=01 depuis la Corse, et dont la date de **départ depuis la Corse** n'est pas connue, la date de départ de la Corse sera indiquée : **1787<01<01**.

!!! warning "Durée de l'étape"

    La durée de l'étape est donnée par la différence entre la date de départ et celle d'arrivée. Or, cette durée n'est juste que si elle se trouve entre deux dates précises. Elle est indicative dans tous les autres cas. 


- des informations sur **le capitaine** : 
    - Nom figurant dans le document relatif à l'étape indiquée (peut varier d'une étape à l'autre).
- des informations sur **le navire** : 
    - Nom du navire figurant dans le document relatif à l'étape indiquée (peut varier d'une étape à l'autre), 
    - pavillon, 
    - port d'attache, 
    - tonnage exprimé en tonneaux.
- Le détail de **la cargaison** (selon les informations disponibles dans le document de la source), sous la forme d'un tableau comportant deux colonnes.

Sur les différentes **lignes du tableau** de la **cargaison**, on voit les **mouvements de marchandises** au départ et à l'arrivée de l'étape sélectionnée, comportant :

- **l'action** effectuée par le produit (in, out, in-out ou transit) au point de départ (colonne 1) et/ou au point d'arrivée (colonne 2), telle qu'elle est décrite par les sources ;
- **le nom du produit**, qui a été standardisé en français contemporain ;
- **sa quantité** et **son unité**, si elles sont connues.

!!! note "Suivre les chargements / déchargements de marchandises de port en port"

    ![Déplacements du capitaine Cruziany](img/cruziany_parcours.jpg)

    Le capitaine Cruziany (id: 00015302) part de *Kherson* avec :
    
    - du blé, 
    - de la chanvre, 
    - du suif, 
    - de la toile,
    - de la viande salée ;
     
    Il s’arrête à *Constantinople* où :
    
    - il charge 2 caisses de mercerie ;
    
    Puis arrive à *Marseille* :

    - il décharge tout.
    
    L’information relative à la cargaison est indiquée uniquement si le produit est chargé ou déchargé dans un des deux ports de l’étape concernée. Elle se présente donc sur **les trois étapes de ce parcours** de la manière suivante:​

    ![indication de l'action effectuée sur les produits présents à bord](img/figure_11.jpg)

# 3. Visualiser deux parcours en même temps

L'application permet de **comparer deux parcours** (c'est-à-dire deux binômes capitaine/navire). Cela permet d'afficher en même temps le parcours de deux capitaines (ou deux navires) ayant deux identifiants différents. Cela permet entre autres de visualiser le parcours :

- d'un capitaine ayant servi sur deux navires différents, 
- ou d'un navire ayant eu deux capitaines différents. 

### Choisir deux parcours différents à visualiser

Exemple : le navire "Abdon (id: 0000066N)" a été commandé par deux capitaines : 

- "Beuzeboc, Denis François"
- "Buzebec, David"
 
Cela produit deux parcours différents. Pour les visualiser, lors d'une recherche de parcours, il faudra cocher deux capitaines (ou deux navires), puis cliquer sur le bouton "Voir".

Pour choisir deux parcours à visualiser ensemble, il faut d'abord repérer les deux candidats à comparer au début de la **recherche de parcours, dans la liste déroulante (Par nom)** : l'affichage de deux id au moins "(id: xxx, id:yyy, id:zzz)" assure de pouvoir le faire.

**Exemple :** navires "**Accord** (id: 0015563N, id: 0015562N)", deux navires différents pour le nom "Accord" donc deux parcours sélectionnables.

![sélection en même temps de deux navires différents](img/figure_12.jpg)
           
En cas de résultats multiples, il est possible dans le tableau des résultats de recherche, de cliquer sur le bouton "Plus de détails" pour affiner la recherche, comme cela a été expliqué plus haut (voir § [2. La visualisation d'un seul parcours](#2-la-visualisation-dun-seul-parcours)). 

### Codes couleurs et ordres de tri utilisés selon que les id sont identiques ou différents

Si on sélectionne **deux parcours d'un navire** (par exemple) avec un **id (de navire) identique** :

- les deux parcours s'afficheront avec les *mêmes couleurs que s'il s’agissait d'un seul parcours*
- les étapes défileront (avec le player) *dans l'ordre : des dates, puis des N° d'ordre* (rang de l'étape dans le parcours)

![Deux parcours avec le même identifiant de navire](img/figure_13.jpg)
***Recherche par navire :** Deux parcours avec le même identifiant de navire 0008878N.*

Si on sélectionne **deux parcours de capitaines** (par exemple) avec des **id (de capitaines) différents** :

- les deux parcours s'afficheront avec des *couleurs différentes* (voir légende). Sur le chronogramme et sur la carte, les **dominantes de couleurs** permettent de **différencier les parcours** :
  
    - dominante **bleue** (parcours N°1) et dominante **verte** (parcours N°2)
    - les étapes réfutées et controversées restent, elles, toujours en rouge et jaune 
    
- les étapes défileront (avec le player) *dans l'ordre du N° de parcours* (tout le parcours 1, puis tout le 2)

![Deux parcours avec le même identifiant de navire](img/figure_14.jpg)
***Recherche par capitaine :** Deux parcours avec des identifiants de capitaines différents 00009224 et 00009225.*

### Différencier les parcours pendant le défilement des étapes

**Pendant le défilement des étapes** du parcours (player), on peut voir si une étape appartient au parcours 1 (bleu) ou 2 (vert) :

- si les id des capitaines (ou navires) choisis sont différents, la couleur du segment renseigne directement :

    ![Parcours distingués par la couleur (navires avec id différents)](img/parcours_id_differents.jpg)  
    ***Navires avec id différents :** On distingue les parcours 1 Amis (id: 0011974N) et 2 Aventure (id: 0000258N) par la couleur des segments.*

- si les id des capitaines (ou navires) choisis sont identiques (ex.: navire 'Aimable Marianne' mêmes id 0008878N, id de capitaine différents 00009224 et 00009225), les jeux de couleurs employées sur les segments des deux parcours sont les mêmes, mais :

    - les noms de capitaine et de navire sont mis en évidence en haut de l'écran avec la couleur de fond du parcours (bleue ou verte) :

        ![Noms sur fond bleu pour un segment du parcours 1](img/noms_sur_segment_parcours_1.jpg)
        ***ex.: Sur un segment du parcours 1** - Les noms du capitaine et du navire concernés passent en fond bleu.*

    - sur le chronogramme, la couleur de fond de l'étape en cours correspond à la couleur du parcours (bleue ou verte)

        ![Couleur de fond du segment parcours 1 sur le chronogramme](img/chronogramme_segment_parcours_1.jpg)  
        ***ex.: Sur un segment du parcours 1** - La couleur de fond du segment sur le chronogramme est bleue.*

### Sur la carte, dans l'info-bulle

**Sur la carte,** au survol de l'étape en cours à la souris, le N° du parcours s'affiche dans la bulle d'aide : 

- ex.: Parcours 1: Rouen -> Bordeaux,

    ![info-bulle du parcours 1](img/info-bulle_parcours_1.jpg)

- ex.: Parcours 2: Bordeaux -> Rouen.

    ![info-bulle du parcours 2](img/info-bulle_parcours_2.jpg)

# 4. Exporter le(s) parcours en tant que lien

Vous avez la possibilité d'envoyer un parcours intéressant à un proche (dans un e-mail par exemple), ou l'inclure dans votre site Web pour y faire référence, en le partageant depuis ShipRoutes en tant que lien (URL). Ce lien ouvrira l'application ShipRoutes et affichera directement le(s) parcours souhaité(s).

Pour exporter le lien d'un parcours, contenant les id des capitaines et des navires, en conservant les paramètres de l'interface (recherche par capitaine ou par navire, segments rouges et oranges affichés ou non), il suffit de cliquer sur l'icône 'Lien'.

L'icône 'Lien' apparaît dès qu'un parcours a été sélectionné et visualisé. Elle se situe à droite des noms de capitaines et de navires, en haut de l'interface :

![Icône 'Lien'](img/figure_15.jpg)

Cliquez sur l'icône 'Lien'. Le texte clignotant "fait" s'affiche à côté du symbole pendant 4 secondes : l'URL a été copiée dans votre presse-papier, vous n'avez plus alors qu'à la coller dans votre texte.

![Comment exporter le parcours en tant que lien](img/figure_16.jpg)  
***Obtenir un lien vers un parcours** - Cliquez sur le bouton 'Lien' pour le copier.*

## 4.1 Conditions d'utilisation des éléments provenant de ShipRoutes

Les liens exportés, captures d'écrans des affichages de ShipRoutes, ... sont mis à disposition selon les termes de la [Licence Creative Commons Attribution 4.0 International (CC BY 4.0)](http://creativecommons.org/licenses/by/4.0/deed.fr). Merci de consulter les termes de la licence avant utilisation.

!!! note "Mentions obligatoires à citer en cas d'utilisation d'éléments produits avec notre interface :"

    [![Licence Creative Commons Attribution 4.0 International (CC BY 4.0)](https://i.creativecommons.org/l/by/4.0/80x15.png)](http://creativecommons.org/licenses/by/4.0/deed.fr) Élément issu de [*ShipRoutes*](http://shiproutes.portic.fr/), une visualisation du [*Projet ANR Portic (2019-2023)*](https://anr.portic.fr/), sans (ou avec) modification(s). Mise à disposition selon les termes de la [Licence Creative Commons Attribution 4.0 International (CC BY 4.0)](http://creativecommons.org/licenses/by/4.0/deed.fr).    


