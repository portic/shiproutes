# ShipRoutes : visualization of the routes of ships and captains

*Help Page for the shiproutes app*

<!-- Admonitions (below), see https://python-markdown.github.io/extensions/admonition/ et https://squidfunk.github.io/mkdocs-material/reference/admonitions/ -->

!!! note "This visualization interface allows you to:"

    1. **Search for routes** of a ship, or a captain, in the Portic database,
    2. **Visualize the steps** of a route, from port to port, with their uncertainty,
    3. Visualize **two routes at once** on the chronogram, and on the map.

For more info, see the [user manual](usermanual.md)

You can also [try the app online](http://shiproutes.portic.fr/)

Dynamic data provided by [the Portic API](http://data.portic.fr/)

Other visualizations from the ANR Portic project : [https://anr.portic.fr/acces-visualisations/](https://anr.portic.fr/acces-visualisations/)

ShipRoutes is an open-source Web application, distributed under the license : [![AGPLv3.png](http://www.gnu.org/graphics/agplv3-with-text-162x68.png)](agpl-3.0.md)

This is the **user documentation** for *ShipRoutes*, a visualization of the *ANR Portic* project : [![https://anr.portic.fr/](img/logo_portic_petit_100.jpg)](https://anr.portic.fr/)