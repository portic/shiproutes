---
language: en
title: ShipRoutes User Manual
summary: Help guide for using the ShipRoutes software, allowing the visualization of routes taken by ships and captains (ANR Portic Project)
authors:
    - Silvia Marzagalli
    - Bernard Pradines
date: 2023-03-23
copyright: © the ANR Portic Project 2019-2023. All rights reserved.
license: GNU Affero General Public License Version 3 (AGPL v3)
document_url: http://shiproutes.portic.fr/static/manual/en/usermanual.html
shiproutes_url: http://shiproutes.portic.fr/
portic_url: https://anr.portic.fr
---

# Introduction. What is this visualization for?

The web application described here, running in the browser, can be accessed at the URL: [http://shiproutes.portic.fr/](http://shiproutes.portic.fr/)

It allows you to visualize in time and space the routes of the captains and ships that appear in the Portic database. To explore the contents of the database, you can use the [VizSources](http://vizsources.portic.fr) application as well as the [Portic Project website](https://anr.portic.fr/).

The data collected was provided with a captain ID and a ship ID. If we have judged that it is the same captain (or the same ship) already encountered elsewhere in the sources, the use of the same identifier therefore makes it possible to follow its itinerary over time, through the different sources present in the database.

The work of assigning identifiers and correcting errors is in progress. Do not hesitate to come back to this site, and report errors to us at this address: *interfaces[at]portic.fr*

### Main features

- [**search** for the route](#1-how-to-search) of a ship or a captain, in the Portic database
- [**visualization** of the steps of the route](#2-viewing-a-single-route), with their degree of uncertainty:
    - in time as a chronogram (a step = an arrow from a port to another),
    - in space, by displaying movements at sea on a map (itinerary, or sea route).
- possibility of [visualizing **two routes at the same time**](#3-view-two-routes-at-the-same-time)
- it is possible to [**get a link** (URL)](#4-export-routes-as-a-link) to share an interesting route

The mandatory information to be quoted when using elements produced with our interface appears at the end of this manual.

### Choice of language

- by default: French if web browser in French, otherwise English
- possibility to change the language using the drop-down list to the right of the title bar ![app language switcher](img/sel_lang_app.jpg)
- in the software help, it is also possible to choose the display language in the same way ![help language switcher](img/sel_lang_help.jpg)

### Global pathways: how to use ShipRoutes

There are three steps to using ShipRoutes:

1. The search and selection of a captain or a ship whose route you want to view, using the **search tool**; 
2. The understanding of the visualization of the entire chosen route (chronogram and map) ; 
3. The possibility to scroll (manually or automatically) through the steps of the route, simultaneously on the chronogram, and on the map.

# 1. How to search

## 1.1 Choose to search for a captain or a ship

The application allows you to search and select a captain or a ship whose route you want to view. To get started, first choose:

- search by captain's name (click on the 'person' icon !['person' icon'](img/icone_personne.jpg) or on the corresponding text box),
- or search by ship name (click on the 'ship' icon !['ship' icon](img/icone_navire.jpg) or on the corresponding text box).

The tick on the right of one of the two pictograms indicates that the last search was carried out in this mode (by captain, or by ship).

## 1.2 How to Search for a Captain or Ship Name

Once this first choice has been made, it is possible to search for the name of the captain or the ship using **three different search methods**: 

- by name,
- by name with variations,
- by identifier (id). 
 
To choose one, click on the corresponding button in this bar:

![search mode selection buttons](img/btn_choix_mode_recherche.jpg)

!!! note "Nota bene"
 
    Whichever method you choose, once you have selected the name(s) you are interested in, in the results table you will need to: 

    - check one or two routes to view,

        ![selection of a search result](img/ex_select_result.jpg)

    - then click on the "Show" button to switch to visualization.

        ![click on the 'Show' button](img/btn_voir.jpg)


### 1.2.1 Search method: **By name**

Simply type in the search box the sequence of characters making up the desired name: 

- to search for a captain, first type his name, a comma, a space, then his first name
- in the drop-down list, the results displayed are limited to what you requested (filtering)
    
    ![example search by name](img/ex_rech_par_nom.jpg)

- finally, click on the name of your choice in the drop-down list to select it
 
!!! warning "Attention"

    This search method only gives results if you type in the **exact string** that is requested. In the previous example, a search for "Abaud Raymond" will not return a result because the comma was omitted. Enter "Abaud, Raymond". The search is not case sensitive.

!!! warning "Attention"

    This search method only gives results **on the most frequent variant of the name** (or, in the event of equal numbers of occurrences, on the first variant present in the database). To search among all the variants, use the search mode "By name (or variation of name)". 

!!! note "Nota bene"

    About the display of the several variations of the name (of the captain or ship) in the search results table:  
        
    - the most frequent variant is displayed
    - when hovering over the name with the mouse, you can see the other variants: "variant (number of occurrences)"  
    ![variants of the name](img/ex_variantes.jpg)

### 1.2.2 Search method: **By name (or variation of name)**

This feature allows you to search among the more or less frequently encountered spellings of names (majority or minority):

- **majority** spelling: the one that is most frequently encountered in the documents of the time. This is the one that will be displayed in the following steps of the route search, and which is associated to the displayed identifier
- **minority** spelling: a variant of the name, mentioned less frequently in the documents. It is always followed by the majority spelling in parentheses in our search results, and it is associated to the identifier of the majority spelling.
  
To search:
    
- in the search field ("Words to search"), enter words or fragments of words in any order (not necessarily in the order: last name, first name)
- in the search results area just below, the spellings of the name including all the words typed ("results found") are displayed
- display format: 
  
    id:000xxxxx (000xxxxN for ships) minority spelling (majority spelling)

- if several variants of the same name (same identifier) match the search, the majority spelling is displayed in bold italic with its identifier, and all the variants of this name which match the search are displayed slightly offset.

    ![example of search by name (or variation of name)](img/ex_rech_par_nom_alt.jpg)
 
- finally, **click on the result of your choice** in the list of results
  
    !!! note "Nota bene"
            
        In the search results table, the corresponding majority spelling will be displayed. This can be checked by hovering the mouse over the names in the list of routes found. All the spellings encountered in the documents of the time will be displayed in a tooltip, followed by their respective frequencies in parentheses.

        ![variants of the name](img/ex_variantes_2.jpg)

- you can **select several variants** in the list of the names found, to compare or visualize their associated routes.
 
    !!! note "To make a multiple selection, you can:"

        - select / deselect **several names from the list**, by holding down the "Shift" key (key allowing you to type a character in uppercase) during the mouse click;
        - select **a range of successive names** in the list, by first clicking on the first name of the range, then holding down the "Ctrl" key while clicking the mouse on the last name of the range.


### 1.2.3 Search method: **By id**

Allows you to display only the routes of a specific captain or ship (no homonym listed).

This method is useful especially for Internet users who have previously used the database or the API to identify one or more captains or ships depending, for instance, on the route or the cargo.

- you must have taken note beforehand of the precise identifier of the captain or the ship whose route(s) you want to display
- the drop-down list presents the identifiers appearing in the database in the format: 

    000xxxxx (for the captains) or 000xxxxN (for the ships) followed by the majority spelling
    
- you must enter the precise identifier, taking care to type at least an initial zero ("0"), followed by the digits of the identifier, and possibly the final "N" if you are performing a ship search
- the list gets filtered until only the desired identifier is retained 

    ![example of search by id](img/ex_rech_par_id.jpg)

- you must then click on the line of the desired identifier to select it 

## 1.3 Result of the search: the notion of 'route'

Regardless of the search mode chosen, the result is displayed in the form of a **list (table) of routes found** corresponding to the name you are searching for.

A route corresponds to a **combination (a captain AND a ship)**, unless one of the identifiers is missing (not yet assigned).

!!! note "Nota bene"

    If the captain or ship identifier has not yet been assigned, the identifier (doc_id) of the document of the time in which it is mentioned, is used instead. 

    !!! warning "Attention"

        This *doc_id* is "transitional", it is intended to be replaced later by the id that will be assigned. It can therefore "disappear" at any time, depending on the progress of our investigations.

The list of results therefore includes all the routes found corresponding to the name entered (as many lines as there are ship/captain pairs meeting the chosen criterion):

- if the search gives a single result, it is automatically checked (selected) to be viewed by clicking directly on the "Show" button (bottom right of the dialog box): 
  
    ![Click on the 'Show' button](img/btn_voir.jpg)

- In the event of multiple results, the user can check two (pairs of different identifiers), then click on the "Show" button to view the two routes (see below).

The presence of several results can be explained by:

- a homonym (very frequent case for ships)
- the presence in the database of several routes carried out by the same captain on different ships (or of the same ship commanded by different captains).
 
![Example of captain present on two different ships](img/figure_2.jpg)  
***Captain who piloted two different ships** - Selection of the variant of the name, and visualization of the two routes.*
 
## 1.4 Adjust what is displayed - Refine a "very" successful search

If the search produces a lot of results (for instance: the ship named "Marianne"), you can: 

- **adjust what is displayed** on screen (pagination of results), 
- **sort** the results to highlight relevant information, 
- **filter** the results furthermore to reduce their number.

### 1.4.1 Choose the number of results displayed per page

It is possible to choose the number of results displayed on the page. Then use the results page **scrolling** system.

![select the number of lines per page](img/figure_1.jpg)  
***Paging** - How to adjust the number of results per page, and buttons to move from page to page.*
 
### 1.4.2 Sorting the results

It is possible to **sort** the results according to the values in the columns. The current sort is materialized by black arrows oriented in the direction of the sort (ascending or descending) next to the names of the columns concerned (multi-column sorting possible).

![example of multi-column sorting](img/ex_tri_multi-colonne.jpg)  
***Example of multi-column sorting** - Ascending sorts on the name of the ship, then on the name of the captain.*

The principle of a multi-column sort is as follows: consider two rows of data, if equality on the value of column 1, then we compare on column 2, if again equality then we sort on column 3, ...

You must click on the name of the variable / the desired column, to activate / deactivate the sorting of the rows of the table according to the values in this column. Several successive clicks on the same column header allow you to alternate between: ascending sort, descending sort, no sort on this column.

For a multi-criteria sort, after simple clicks on a first column, hold down the "Shift" key and click on a second column header (several times if necessary depending on the type of sort desired), to perform the sort also on a second column (then a third etc.). 

### 1.4.3 Filter the results

It is possible to **filter** the results to progressively narrow, field by field, the list of results, or to find a particular result. To filter:

- first click on the "Filter" toggle button to activate the filtering form (By default, no filtering: to return to the default option, click on the "Show all" toggle button)
- The results table is displayed by default with a limited number of variables. It is possible to display more variables.

    ***simple*** filters: ship name, captain name, beginning and end dates of sightings;

    ***multiple*** filters, by clicking the "More details" toggle button. 
    
- To return to the default set of variables, click on the "Less details" toggle button.

![enable filtering of results](img/figure_3.jpg)  
***Enabling multiple filtering** - Example of search by ship name: << Marianne >>.*
 
Displayed fields (columns) for a search by **ship name**:

- ***simple*** filters:
    - ship name (ascending sort),
    - captain's name (ascending sort),
    - beginning and end dates of observations;
- ***multiple filters*** - possibility to select in addition:
    - the ship's identifier,
    - ship's flag,
    - ship's homeport,
    - the tonnage class expressed in tons,
    - the ship's class,
    - the captain's identifier.

Displayed fields (columns) for a search by **captain's name**:

- ***simple*** filters:
    - captain's name (ascending sort),
    - ship on which he traveled (ascending sort),
    - beginning and end dates (this is the earliest and latest date on which the captain is present in the database);
- ***multiple filters*** - possibility to select in addition:
    - the captain's identifier,
    - his citizenship (Catalan, Corsican, Swedish, etc.),
    - his birthplace (e.g.: "from Antibes"),
    - the ship's identifier.
 
Each filtering field (criteria) allows a search by direct entry in the field, then final selection. Filtering is immediate and the list of results is updated:

- to reset the content of a filter criterion, click on the cross (x) at the end of the line
- to stop filtering (remove all criteria), click on the "Show all" toggle button

!!! note "Nota bene"

    The filters take into account only the fields of the database which are filled in. If the variable is empty, the corresponding ship or captain is not displayed (although it could *also* meet the criteria, if value was known). 

# 2. Viewing a single route

After checking the line of the ship or captain that interests you, you can view the route by clicking on the "Show" button.

![click on the 'Show' button](img/btn_voir.jpg)

## 2.1 Presentation of the screen

Here is an overview of the screen:

![understanding the displayed screen](img/figure_4.jpg)
 
 At **top left** of the screen, the "captain" and "ship" icons are followed by their respective names and identifiers, to remind you of the chosen combination:

![Display recalling the selected course](img/ex_1_parcours_selectionne.jpg)

!!! warning "Nota bene"

    A click on the 'captain' or 'ship' icons (or on their names) allows you to return to the search function, to select another route to view.

**Below, on the left**, is displayed a **graph (chronogram)** illustrating the route of the chosen captain/ship combination:

![Chronogram](img/ex_chronogramme.jpg)

- the horizontal axis indicates time
- the vertical axis indicates the latitude of the localities visited by the captain or the ship 
- the segments of the route displayed qualify the degree of uncertainty linked to the nature of the sources, as we have reconstructed it. The legend specifies the semiology (see below in detail): 

![Caption](img/ex_legende.jpg)

You can hide or show invalidated and/or controversial steps by checking the corresponding boxes in the line below the graph:

![Checkboxes for hiding invalidated or controversial steps](img/cases_masquage_etapes.jpg)

**On the right of the screen** you find the **map** which displays by default the entire route of the chosen captain/ship combination. The semiology of the steps is identical to that of the caption, but the invalidated stages are omitted:

![Map](img/ex_carte.jpg)

## 2.2 Semiology of the steps

Thanks to the identification of the captains [(see here)](https://anr.portic.fr/incertitude/#Lidentification_des_capitaines) and the ships [(see here)](https://anr.portic.fr/incertitude/#Lidentification_des_navires), in some cases we can confirm or invalidate the reality of a declared route. The semiology of the chronogram and the map indicates the degree of uncertainty of the declared routes:

![Caption](img/ex_legende.jpg)

Line type:

- **dotted line**: route not attested by the sources (undeclared), which we have deduced, between two successive declared stopovers (e.g.: A ship that we find departing from Les Sables-d'Olonne for Terre-Neuve in March and then found on leaving La Rochelle in July will be assumed to have made the return trip from Terre-Neuve to La Rochelle);
- **continuous line**: route declared by the source (in the previous example: Sables-d'Olonne -> Terre-Neuve)

Line color:

- if the line is **grey**, the database does not contain any source confirming that the route declared at the departure port was real;
- if the line is **green**, the database confirms that the route declared at the departure port was real.
 
    ![Semiology of the caption - Most frequent example](img/figure_5.jpg) 

- if the line is **yellow**, the route is controversial: it is theoretically possible but for the moment the historian does not consider it probable, due for example to a too short duration between two departures from two very distant points;

    ![Semiology of the caption - Example of a controversial route](img/figure_6a.jpg)
 
- if the line is **red**, the database negates that the route declared at the departure port was real. For example, we invalidate the Nantes -> Bordeaux trip and consider that the ship has made the Nantes -> La Rochelle -> Bordeaux trip, because we have the Bordeaux departures register and we did not find two departures for this ship. 

    ![Semiology of the caption - Example of refuted route](img/figure_6b.jpg)

## 2.3 Interact with visualizations

The **"Timeline" and "Map" buttons** at the bottom left allow you to interact with the chronogram or the map respectively.

![Timeline / Map button](img/bouton_frise_carte.jpg)

- Timeline = interact with the chronogram (default)
- Map = interact with the map (to scroll through the steps)

What is **displayed on the map** depending on the mode:

- **In "Timeline" mode**, the map displays all the segments (steps of the route). Invalidated steps (in red) are not displayed on the map. Only the ports which are part of the route appear on the map (the other ports in France and elsewhere are not displayed);
- **In "Map" mode**, an automatic zoom at each step allows you to see the entire step in progress (initially: the first).

### Interaction with the graph/chronogram

In some cases, the **chronogram** displays a very large number of steps of a route of a captain or a ship, which compromises its readability. To work around this problem, **in "Timeline" mode**, it is possible to zoom in to see the details (port names, dates):

- **zoom with the mouse wheel**, positioned above the chronogram: allows you to enlarge / drag an area of the graph, to see the details ('temporal' magnifying glass effect - time axis only);
- **time scale**: out of zoom, the display of dates shows dates at least 15 days apart (to limit overlapping of dates that are too close).
 
    ![Adjusting the display of steps close in time](img/figure_7.jpg)
 
**In "Map" mode**, the current step is **highlighted on the chronogram** by a change in the background color, and a bold display of the arrow concerned:

![Highlighting on the chronogram of the step displayed on the map](img/figure_8.jpg)
 
### Map Interaction

In "Map" mode, the number of steps is displayed on the last line, in the left part of the screen, in the center (Ex: 1/7 = step 1 out of a total of 7 steps). **The "Map" mode allows you to scroll the route** step by step:

![Scroll through the steps](img/figure_9.jpg)

- **the map displays the current step**, connecting two ports (departure, destination), by a **segment passing by the sea**. The map zoom automatically adjusts to the step
- by default, the "Map" mode is set to pause, on the first step
- in *pause*, you can **scroll through the steps** *manually* by clicking on the button to the left (backward) or to the right (forward) of the step number
- the chronogram and the map are synchronized: they show the same step
- you can scroll through the steps *automatically* by clicking on *the black arrow (play)*, at the rate selected in the drop-down list next to it
- in automatic playback, a mouse click on the map switches the player to pause mode (manual scrolling)
 
Hovering with the mouse over a step **on the map** displays a **tooltip for the step**:
 
![read a step info tooltip](img/figure_10.jpg)

This tooltip includes:

- the **localities** of departure and arrival: Departure -> Arrival
- the **dates** of departure and arrival: Departure date - Arrival date (duration of the step in days)
 
!!! warning "The date format"
    
    Dates are in the form **YYYY-MM-DD** (year-month-day):

    - If the separator is an **"equals" sign (=)**, the ***precise*** date is given by the source.
    - If the separator is a **"greater" sign (>)**, the date is ***after*** the one indicated. 
    - If the separator is a **"lower" sign (<)**, the date is ***older*** than the one indicated. 

!!! note "Examples"

    - In the case of a ship leaving Bordeaux on 1787=01=01 for La Rochelle, the date of arrival at La Rochelle is unknown, but later (1787>01>01):
        - if no other subsequent data relating to this ship is present in the database, the date of the step is displayed: **1787=01=01 - 1787>01>01**
        - if this same ship is observed leaving La Rochelle on 1787=03=15, the Bordeaux -> La Rochelle step is displayed with the following two dates: **1787=01=01 - 1787<03<15**
    - In the case of a ship arriving in Marseille on 1787=01=01 from Corsica, and whose date of **departure from Corsica** is not known, the departure date from Corsica will be indicated as : **1787<01<01**.

!!! warning "Step duration"

    The duration of the step is given by the difference between the date of departure and that of arrival. However, this duration is only correct if it is between two precise dates. It is indicative in all other cases. 



- information on **the captain**: 
    - Name from the document, taken from the declaration of the step (may vary from step to step).
- information on **the ship**: 
    - Name of the ship appearing in the document relating to the step indicated (may vary from step to step), 
    - flag, 
    - homeport, 
    - tonnage expressed in barrels.
- Details of **the cargo** (according to the information available in the source document), in the form of a table with two columns.

On the different **lines of the table** of the **cargo**, we see the **movements of goods** at the start and at the arrival of the selected step, including:

- **the action** on the product (in, out, in-out or transit) at the point of departure (column 1) and/or at the point of arrival (column 2), as described by the sources;
- **the name of the product**, which has been standardized in contemporary French;
- **its quantity** and **its unit**, if known.

!!! note "Follow the loading / unloading of goods from port to port"

    ![Captain Cruziany's trips](img/cruziany_parcours.jpg)

    Captain Cruziany (id: 00015302) departs from *Kherson* with:
    
    - wheat,
    - hemp,
    - tallow,
    - canvas,
    - salted meat;
     
    He stops at *Constantinople* where:
    
    - he loads 2 boxes of haberdashery;
    
    Then arrives at *Marseille*:

    - he unloads everything.
    
    The information relating to the cargo is indicated only if the product is loaded or unloaded in one of the two ports of the step concerned. It is shown on **the three steps of this route** as follows:​

    ![indication of the action performed on the products on board](img/figure_11.jpg)

# 3. View two routes at the same time

With this program you can **compare two routes** (i.e. two captain/ship pairs). So you can display the routes of two captains (or two ships) with two different identifiers at the same time. This allows, among other things, to visualize the route of:

- a captain who served on two different ships, 
- or a ship that had two different captains. 

### Choose two different routes to view

Example: the ship "Abdon (id: 0000066N)" was commanded by two captains: 

- "Beuzeboc, Denis François"
- "Buzebec, David"
 
This produces two different routes. To view them, when searching for a route, you will have to check two captains (or two ships), then click on the "Show" button.

To choose two routes to view together, you must first identify the two candidates to be compared at the start of the **route search, in the drop-down list (By name)**: the display of at least two ids "(id: xxx, id:yyy, id:zzz)" ensures that you can do this.

**Example:** ships "**Accord** (id: 0015563N, id: 0015562N)", two different ships for the name "Accord" (homonymy) so two routes can be selected.

![selection of two different ships at the same time](img/figure_12.jpg)
           
In the event of multiple results, it is possible in the search results table to click on the "More details" button to refine the search, as explained previously (see § [2. Viewing a single route](#2-viewing-a-single-route)). 

### Color codes and sort orders used depending on whether the ids are the same or not

If we select **two routes of a ship** (for example) with an identical **id (ship_id)**:

- the two routes will be displayed with the *same colors as if it were a single route*
- the steps will scroll (with the player) *in order of: dates, then order numbers* (rank of the step in the route)

![Two routes with the same ship ID](img/figure_13.jpg)  
***Search by ship:** Two routes with the same ship ID 0008878N.*

If you select **two routes, by captain,** (for example) with **different captain IDs**:

- the two routes will be displayed with *different colors* (see legend). On the chronogram and on the map, the **dominant colors** make it possible to **differentiate the routes**:
  
    - dominant **blue** (course N°1) and dominant **green** (course N°2)
    - the invalidated and controversial steps remain, they, always in red and yellow
    
- the steps will scroll (with the player) *in the order of the route number* (the whole route 1, then the whole route 2)

![Two routes with the same ship ID](img/figure_14.jpg)  
***Search by captain:** Two routes with different captain IDs 00009224 and 00009225.*

### Differentiate routes while scrolling through steps

**While scrolling through steps** of the route (player), we can see if a step belongs to route 1 (blue) or 2 (green):

- if the ids of the chosen captains (or ships) are different, the color of the segment provides information directly:

    ![Routes distinguished by color (ships with different ids)](img/parcours_id_differents.jpg)  
    ***Ships with different ids:** The routes 1 Friends (id: 0011974N) and 2 Adventure (id: 0000258N) are distinguished by the color of the segments.*

- if the ids of the chosen captains (or ships) are identical (e.g. ship 'Aimable Marianne' same id 0008878N, different captain ids 00009224 and 00009225), the sets of colors used on the steps of the two routes are the same, but:

    - captain and ship names are highlighted at the top of the screen with the background color of the route (blue or green):

        ![Names on a blue background for a step of route 1](img/noms_sur_segment_parcours_1.jpg)
        ***e.g.: On a segment of route 1** - The names of the captain and the ship concerned change to a blue background.*

    - on the chronogram, the background color of the current step corresponds to the color of the route (blue or green)

        ![Background color of route segment 1 on the chronogram](img/chronogramme_segment_parcours_1.jpg)  
        ***e.g.: On a segment of route 1** - The background color of the segment on the chronogram is blue.*

### On the map, in the tooltip

**On the map,** when hovering over the current step with the mouse, the route number is displayed in the tooltip: 

- e.g.: Route 1: Rouen -> Bordeaux,

    ![route 1 tooltip](img/info-bulle_parcours_1.jpg)

- e.g.: Route 2: Bordeaux -> Rouen.

    ![route 2 tooltip](img/info-bulle_parcours_2.jpg)

# 4. Export route(s) as a link

You have the option of sending an interesting route to someone (in an e-mail for example), or including it in your website for reference, by sharing it from ShipRoutes as a link (URL). This link will open the ShipRoutes app and directly display the desired route(s).

To export the link of a route, containing the ids of the captains and the ships, while keeping the interface parameters (search by captain or by ship, red and orange segments displayed or not), simply click on the 'Link' icon.

The 'Link' icon appears as soon as a route has been selected and viewed. It is located to the right of the names of captains and ships, at the top of the interface:

!['Link' icon](img/figure_15.jpg)

Click on the 'Link' icon. The blinking text "done" is displayed next to the symbol for 4 seconds: the URL has been copied to your clipboard, you just have to paste it into your text.

![How to export the route as a link](img/figure_16.jpg)  
***Get a link to a route** - Click on the 'Link' button to copy it.*

## 4.1 Terms of Use for Items from ShipRoutes

Exported links, screenshots of ShipRoutes displays, ... are licensed under a [Creative Commons Attribution 4.0 International License (CC BY 4.0)](http://creativecommons.org/licenses/by/4.0/deed.en). Please read the license terms before use.

!!! note "Mandatory information to quote when using items produced with our interface:"

    [![Creative Commons Attribution 4.0 International License (CC BY 4.0)](https://i.creativecommons.org/l/by/4.0/80x15.png)](http://creativecommons.org/licenses/by/4.0/deed.en) Item from [*ShipRoutes*](http://shiproutes.portic.fr/), a visualization of  [*The ANR Portic Project (2019-2023)*](https://anr.portic.fr/), with (or without) change(s). Licensed under a [Creative Commons Attribution 4.0 International License (CC BY 4.0)](http://creativecommons.org/licenses/by/4.0/deed.en).    


