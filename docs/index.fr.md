# ShipRoutes : visualisation des parcours des navires et des capitaines

*Page d'Aide de l'application shiproutes*

<!-- Admonitions (ci-après), voir https://python-markdown.github.io/extensions/admonition/ et https://squidfunk.github.io/mkdocs-material/reference/admonitions/ -->

!!! note "Cette interface de visualisation vous permet de:"

    1. **Rechercher des parcours** d'un navire, ou d'un capitaine, dans la base de données de Portic,
    2. **Visualiser les étapes** d'un parcours, de port en port, avec leur incertitude,
    3. Visualiser **deux parcours à la fois** sur le chronogramme, et sur la carte.

Consultez le [manuel utilisateur](usermanual.md) pour plus d'informations

Vous pouvez aussi [voir l'application en ligne](http://shiproutes.portic.fr/)

Données dynamiques fournies par [l'API de Portic](http://data.portic.fr/)

Autres visualisations du projet ANR Portic : [https://anr.portic.fr/acces-visualisations/](https://anr.portic.fr/acces-visualisations/)

ShipRoutes est une application Web open-source, distribuée sous licence : [![AGPLv3.png](http://www.gnu.org/graphics/agplv3-with-text-162x68.png)](agpl-3.0.md)

Ceci est la **documentation utilisateur** de *ShipRoutes*, une visualisation du projet *ANR Portic* : [![https://anr.portic.fr/](img/logo_portic_petit_100.jpg)](https://anr.portic.fr/)