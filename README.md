**<h1>Application Shiproutes - Documentation technique.</h1>**

- [Objectif de l'application](#objectif-de-lapplication)
- [Guide de déploiement rapide](#guide-de-déploiement-rapide)
- [Paquets npm / yarn utilisés](#paquets-npm--yarn-utilisés)

L'application Web Shiproutes (le logiciel) est sous copyright : © the [ANR Portic Project](https://anr.portic.fr/) 2019-2023. All rights reserved.  
Le code source est publié en open-source sous licence [AGPL v3](docs/agpl-3.0.md) .

# Objectif de l'application

Mapping the uncertainty of past maritime routes : search a route by the captain or the ship name and visualize its route.

Application pour visualiser les trajets des navires :  

- sélection d'un navire par son nom, ou le nom de son capitaine
- affichage des trajets effectués sur une carte, et sur un chronogramme

L'application se connecte à l'API [porticapi](http://data.portic.fr) pour obtenir les données affichées.

L'application est accessible en ligne à l'adresse : [http://shiproutes.portic.fr](http://shiproutes.portic.fr)

# Guide de déploiement rapide

Pré-requis : pour mettre l'application Web en production, 

- un serveur Web compatible WSGI est nécessaire (ex.: Apache avec mod_wsgi), car l'application utilise le paquet / micro-framework *Flask* (écrit en Python)
- il faut Git, 
- un accès (HumanID + accès Gitlab) au serveur de dépôts de sources [gitlab.huma-num.fr](https://gitlab.huma-num.fr/),
- avoir déjà clôné le dépôt [Portic / shiproutes](https://gitlab.huma-num.fr/portic/shiproutes), 
- il faut une version stable récente de Node.js, 
- Yarn v1.x (npm install yarn -g)
- une version de Python (et un environnement virtuel activé).

Pour mettre à jour l'application en production sur le serveur Web, dans le dossier des sources de l'application, effectuer les opérations suivantes :

- récupérer la dernière version des sources de l'application dans la branche **main** :  
  `git pull`
- installer les modules Node nécessaires :  
  `yarn install`
- uniquement si demandé (pour résoudre des problèmes de compatibilité entre paquets) - mettre à jour les paquets npm, vers la version la plus récente :  
  `yarn upgrade`
- construire l'application React pour la production :  
  `yarn run build`
- installer les paquets Python nécessaires (dont Flask et mkdocs) :  
  `pip install -r requirements.txt`
  
  Actuellement, les paquets Python utilisés sont :

  ```
  flask
  mkdocs
  mkdocs[i18n]
  mkdocs-static-i18n
  ```
- construire le site du manuel utilisateur (lien 'Aide'), qui apparaît alors dans le dossier [static/manual](static/manual) :  
  `mkdocs build`

Pour lancer l'application (en local sur le poste de développement) :

- lancer le serveur Web Flask avec Python :  
  `python __init__.py`
- ouvrir l'application Web dans le navigateur - faire Ctrl+clic sur l'URL indiquée en ligne de commande, ou bien :  
  [http://127.0.0.1:5000](http://127.0.0.1:5000)

# Paquets npm / yarn utilisés

On retrouve la liste des paquets installés dans [package.json](package.json) :

- pour construire le bundle de code du projet **shiproutes.bundle.js** avec Webpack (dans le dossier 'static/dist'), il faut au minimum installer : React, Webpack et Babel
  ```
  yarn init -y
  yarn add react react-dom
  yarn add -D webpack webpack-cli webpack-dev-server clean-webpack-plugin
  yarn add -D @babel/core @babel/preset-env @babel/preset-react babel-loader
  yarn add -D babel-plugin-macros
  yarn add -D style-loader css-loader
  ```
  La configuration de Webpack est dans [webpack.config.js](webpack.config.js)

  La configuration de Babel est dans [babel.config.js](babel.config.js). Pour empaqueter seulement les icônes utilisées de Fontawesome, le plugin '*macros*' est utilisé, dont la configuration est dans [babel-plugin-macros.config.js](babel-plugin-macros.config.js).

  Rappel : le bundle de code de l'application est construit avec la commande :  
  `yarn run build`

- Pour ce projet spécifiquement, installer Leaflet et React Leaflet qui font l'affichage de cartes :

  `yarn add leaflet react-leaflet`

- Pour faire les accès à l'API porticapi qui fournit les données à afficher (http://data.portic.fr/api port 80 en production, http://127.0.0.1/api:80 en développement) :

  `yarn add axios`

- Pour la présentation, utilisation de Bootstrap 5.1 (qui dépend de popperjs), et de React-Bootstrap v2 :

  `yarn add bootstrap @popperjs/core react-bootstrap`

- Pour l'affichage d'une légende dans un coin de carte :

  `yarn add react-leaflet-custom-control`

- Pour importer les fichiers de cartes TopoJson et les convertir en GeoJson, une librairie est nécessaire :

  `yarn add topojson-client`

- Pour les calculs géométriques :

  `yarn add leaflet-geometryutil`

- Pour la localisation de l'application :

  `yarn add i18next react-i18next i18next-browser-languagedetector`

  Rappel : pour construire le site de documentation utilisateur de l'application, en Français et en Anglais, il faut taper la commande :  
  `mkdocs build`

- Pour les icônes FontAwesome Free en v6.1, avec le composant React officiel qui utilise SVG + JS :

  `yarn add @fortawesome/fontawesome-svg-core @fortawesome/free-solid-svg-icons @fortawesome/free-regular-svg-icons @fortawesome/free-brands-svg-icons @fortawesome/react-fontawesome@latest`

- Pour exporter la carte affichée dans un fichier image :

  `yarn add dom-to-image`

- composant de recherche / sélection d'un élément dans une liste prédéfinie (auto-complétion) :

  `yarn add react-select`

- composant d'affichage de données en tableau : React Table v7

  `yarn add react-table`

- composant d'affichage de graphiques pour le chronogramme : [Victory](https://formidable.com/open-source/victory/) charting

  `yarn add victory`

- composant de lissage (Spline - Cubic SVG Bezier curves) des courbes (polylines, polygons) : [leaflet-spline](https://github.com/slutske22/leaflet-spline)

  `yarn add leaflet-spline`

  Ce composant ajoute deux autres dépendances :

    - il s'appuie sur l'extension [Leaflet.curve](https://github.com/elfalem/Leaflet.curve) (@elfalem/leaflet-curve) pour fonctionner ;
    - il offre la possibilité d'animer les splines grâce à la librairie [Tween.js](https://github.com/tweenjs/tween.js/) (@tweenjs/tween.js) .

- composant pour tracer des flèches sur la carte, sur les segments d'un parcours (sens de parcours) : [leaflet-arrowheads](https://github.com/slutske22/leaflet-arrowheads)

  `yarn add leaflet-arrowheads`







  