const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const webpack = require('webpack'); // to access built-in plugins
const path = require('path');

module.exports = {
    entry: {
        shiproutes: "./static/jsx/index.js",
    },
    module: {
        rules: [
            {
                test: /\.(js|jsx)$/,
                use: "babel-loader",
            },
            {
                test: /\.(svg|png|jpg|jpeg|gif)$/i,
                type: 'asset/resource',
            },
            {
                test: /\.(woff|woff2|eot|ttf|otf)$/i,
                type: 'asset/resource',
            },
            {
                test: /\.css$/i,
                use: ["style-loader", "css-loader"],
            },
        ],
    },
    resolve: {
        extensions: ["", ".webpack.js", ".web.js", ".js", ".jsx", ".json"],
    },
    output: {
        path: __dirname + "/static/dist",
        filename: "[name].bundle.js",
    },
    plugins: [
        new CleanWebpackPlugin(), // supprime tous les fichiers du répertoire dist sans pour autant supprimer ce dossier
      ]
};
